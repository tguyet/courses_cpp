/**
* Thomas Guyet, AGROCAMPUS-OUEST, 2010
*/

#include "dessin.h"
#include <stdio.h>
#include <math.h>

void clear(int **matrix_r, int **matrix_g, int **matrix_b, int height, int width)
{
	int i,j;
	for(i=0; i< height; i++) {
		for(j=0; j<width; j++) {
			matrix_r[i][j]=255;
			matrix_g[i][j]=255;
			matrix_b[i][j]=255;
		}
	}
}

void plot(int **matrix_r, int **matrix_g, int **matrix_b, int height, int width, int x, int y, int r, int g, int b)
{
	if(x>=0 && x<height && y>=0 && y<width) {
		if(r>=0 && r<256 && g>=0 && g<256 && b>=0 && b<256) { 
			matrix_r[x][y]=r;
			matrix_g[x][y]=g;
			matrix_b[x][y]=b;
		} else {
			printf("Plot : Couleur non définie\n");
		}
	}
	//lorsqu'on est hors de l'image, on ne fait rien! Ca va permettre de définir des formes complexes étant en partie à l'extérieur et en assurant que ce qui recouvre l'image soit bien affiché!
}

void line(int **matrix_r, int **matrix_g, int **matrix_b, int height, int width, int x_begin, int y_begin, int x_end, int y_end, int r, int g, int b)
{
	int j=0,i=0;
	int x_step =1, y_step =1;
	if(r<0 || r>=256 || g<0 || g>=256 || b<0 && b>=256) { //On fait ce test ici pour éviter d'afficher 3000 messages identiques à partir de la fonction plot!
		printf("Line : Couleur non définie\n");
		return;
	}
	
	if(x_begin>x_end) {
		x_step=-1; //En fait, il faudra décrémenter x
	}
	if(y_begin>y_end) {
		y_step=-1; //En fait, il faudra décrémenter y
	}
	
	//On évacue les deux cas simples : horizontal et vertical
	if( x_begin == x_end  ) {
		for(i=y_begin;i!=y_end;i+=y_step){
			plot(matrix_r,matrix_g,matrix_b,height,width,x_begin,i,r,g,b); //Dans ma fonction line, je fais appel à la fonction plot qui gère les éventuelles sorties de l'image!!
		}
		return; //Je sors tout de suite de la fonction!
	}
	if( y_begin == y_end  ) {
		for(i=x_begin;i!=x_end;i+=x_step){
			plot(matrix_r,matrix_g,matrix_b,height,width,i,y_begin,r,g,b);
		}
		return; //Je sors tout de suite de la fonction!
	}
	
	// Si j'arrive ici, je sais que j'ai à faire à une ligne penchée.

	
	// En fonction de la pente, il faut soit itérer sur les x, soit itérer sur les y
	if( abs(x_end-x_begin) > abs(y_end-y_begin) ) {
		double pente = (double)abs(y_end-y_begin)/(double)abs(x_end-x_begin); //ici, je sais que x_end-x_begin!=0, car le cas a été traité avant
		double cumsum=0;// Somme cumulée de fraction pentes (on a toujours 0<cumsum<1), quand x avance cumsum comptabilise l'avancée virtuelle de y et lorsque cumsum>=1, alors y fait un saut de 1.
		
		j=y_begin;
		for(i=x_begin;i!=x_end;i+=x_step){ //En fonction du x_step, j'incremente ou je decremente!
			if(cumsum>=1 && cumsum<=1){ j+=y_step; cumsum=0;} //lorsque la somme cumulé des pentes a dépassé, on incrémente j (ou on décremente en fonction de j)
			cumsum+=pente;
			plot(matrix_r,matrix_g,matrix_b,height,width,i,j,r,g,b);
		}
		//il manque le dernier point:
		if(cumsum>=1  && cumsum<=1){ j+=y_step;}
		plot(matrix_r,matrix_g,matrix_b,height,width,x_end,j,r,g,b);
	} else {
		//C'est exactement pareil qu'avant mais en transposant x et y
		double pente = (double)abs(x_end-x_begin)/(double)abs(y_end-y_begin);
		double cumsum=0;
		i=x_begin; //NB: j'ai inversé l'utilisation de i et j de sorte à toujours avoir les mêmess indices ds la fonction plot !
		for(j=y_begin;j!=y_end;j+=y_step){					
			if(cumsum>=1){ i+=x_step; cumsum=0;}
			cumsum+=pente;
			plot(matrix_r,matrix_g,matrix_b,height,width,i,j,r,g,b);
		}
		if(cumsum>=1){ i+=x_step;}
		plot(matrix_r,matrix_g,matrix_b,height,width,i,y_end,r,g,b);
	}
}

void rectangle(int **matrix_r, int **matrix_g, int **matrix_b, int height, int width, int x, int y, int w, int h, int fg_r, int fg_g, int fg_b, int bg_r, int bg_g, int bg_b)
{
	int i=0,j=0;
	//Quelques tests préalables pour éviter plein d'erreur lors du tracé du fond par exemple!
	if(bg_r<0 || bg_r>255 || bg_g<0 || bg_g>255 || bg_b<0 || bg_b>255) {
		printf("Rect : Couleur BG non définie\n");
		return;
	}
	if(fg_r<0 || fg_r>255 || fg_g<0 || fg_g>255 || fg_b<0 || fg_b>255) {
		printf("Rect : Couleur FG non définie\n");
		return;
	}
	if (h < 0 || w < 0) {
		printf("Rect : Taille de rectangle impossible (<0)\n");
		return;
	}
	
	//trace le fond
	for(i=x+1;i<x+w;i++) {
		for(j=y+1;j<y+h;j++) {
			plot(matrix_r, matrix_g, matrix_b, height, width, i, j, bg_r, bg_g, bg_b);
		}
	}

	//trace le contour
	line(matrix_r, matrix_g, matrix_b, height, width, x,y, x+w, y, fg_r,fg_g,fg_b);
	line(matrix_r, matrix_g, matrix_b, height, width, x+w,y, x+w, y+h, fg_r,fg_g,fg_b);
	line(matrix_r, matrix_g, matrix_b, height, width, x+w,y+h, x, y+h, fg_r,fg_g,fg_b);
	line(matrix_r, matrix_g, matrix_b, height, width, x,y+h, x, y, fg_r,fg_g,fg_b);
}

void cercle(int **matrix_r, int **matrix_g, int **matrix_b, int height, int width, int c_x, int c_y, int rayon, int fg_r, int fg_g, int fg_b, int bg_r, int bg_g, int bg_b)
{
	int i=0,j=0;
	if(rayon == 0) return; //Pas de cercle!
	//Quelques tests préalables
	if(bg_r<0 || bg_r>255 || bg_g<0 || bg_g>255 || bg_b<0 || bg_b>255) {
		printf("Rect : Couleur BG non définie\n");
		return;
	}
	if(fg_r<0 || fg_r>255 || fg_g<0 || fg_g>255 || fg_b<0 || fg_b>255) {
		printf("Rect : Couleur FG non définie\n");
		return;
	}
	if (rayon < 0) {
		rayon=-rayon;
	}
	//Ici, on est sûr que rayon est positif !
	
	//Pour tracer l'intérieur du cercle, on parcourt le rectangle circonscrit et on change la couleur du pixel uniquement si la distance au centre est inférieure au rayon.
	for(i=c_x-rayon;i<=c_x+rayon;i++) {
		for(j=c_y-rayon;j<=c_y+rayon;j++) {
			int dy=c_y-j;
			int dx=c_x-i;
			if( (dx*dx + dy*dy) <= rayon*rayon ) { //Astuce : je compare les carrés plutot que de calculer les racines carrées car le calcul d'une racine carrée est très couteuse en temps (utilisation de deéveloppements limités)
				plot(matrix_r, matrix_g, matrix_b, height, width, i, j, bg_r, bg_g, bg_b);
				if( (dx*dx + dy*dy) > (rayon-1)*(rayon-1) ) {
					plot(matrix_r, matrix_g, matrix_b, height, width, i, j, fg_r, fg_g, fg_b);
				}
			}
		}
	}
}

