/**
* Thomas Guyet, AGROCAMPUS-OUEST, 2010
*/

void clear(int **matrix_r, int **matrix_g, int **matrix_b, int height, int width);

void plot(int **matrix_r, int **matrix_g, int **matrix_b, int height, int width, int x, int y, int r, int g, int b);

void line(int **matrix_r, int **matrix_g, int **matrix_b, int height, int width, int x_begin, int y_begin, int x_end, int y_end, int r, int g, int b);

void rectangle(int **matrix_r, int **matrix_g, int **matrix_b, int height, int width, int x, int y, int w, int h, int fg_r, int fg_g, int fg_b, int bg_r, int bg_g, int bg_b);

void cercle(int **matrix_r, int **matrix_g, int **matrix_b, int height, int width, int c_x, int c_y, int rayon, int fg_r, int fg_g, int fg_b, int bg_r, int bg_g, int bg_b);


