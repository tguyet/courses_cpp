/**
* Thomas Guyet, AGROCAMPUS-OUEST, 2010
*/
#include "images.h"
#include "dessin.h"
#include <stdio.h>

int main(int argc, char** argv)
{
	//L'image matricielle
	int **matrix_r =0, **matrix_g =0, **matrix_b =0;
	int height, width;
	
	FILE *fp; //Pointeur sur le fichier de description vectorielle
	int x_curseur =0, y_curseur =0; //Position du curseur
	int r_fg =255, g_fg =255, b_fg =255, r_bg =0, g_bg =0, b_bg =0; //Couleurs du foreground et du background
	int image_created =0; //Indique si une image existe bien!
	
	fp = fopen("image.des", "r");
	
	if(fp==0) {
		printf("Erreur d'ouverture d'un des fichiers\n");
		return -1;
	}
	
	while ( !feof(fp) ) //Tant qu'il reste des lignes, on lit le fichier
	{
		char str[10];
		int a=-1,b=-1,c=-1;
		fscanf(fp, "%s %d %d %d\n", str, &a, &b, &c);
		
		if( !strcmp(str, "IMAG") ) {
			if(image_created) {
				printf("ERREUR : Deux appels à la primitive IMAG!\n");
				break;
			}
			height=a;
			width=b;
			new_image(&matrix_r, &matrix_g, &matrix_b, height, width);
			clear(matrix_r, matrix_g, matrix_b, height, width);
			image_created=1;
		} else {
			if(!image_created) {
				printf("ERREUR : Pas de primitive IMAG initiale\n");
				break;
			}
			if( !strcmp(str, "MOVE") ) {
				x_curseur =a; y_curseur =b;
			} else if (!strcmp(str, "MOVA") ) {
				x_curseur +=a; y_curseur +=b;
			} else if (!strcmp(str, "PLOT") ) {
				plot(matrix_r, matrix_g, matrix_b, height, width, x_curseur, y_curseur, r_fg, g_fg, b_fg);				
			} else if (!strcmp(str, "LINE") ) {
				line(matrix_r, matrix_g, matrix_b, height, width, x_curseur, y_curseur, a, b, r_fg, g_fg, b_fg);
				x_curseur =a; y_curseur =b;
			} else if (!strcmp(str, "LINA") ) {
				line(matrix_r, matrix_g, matrix_b, height, width, x_curseur, y_curseur, x_curseur + a, y_curseur + b, r_fg, g_fg, b_fg);
				x_curseur +=a; y_curseur +=b;
			} else if (!strcmp(str, "RECT") ) {
				int x,y,w,h;
				if(x_curseur > a) {
					w=x_curseur-a;
					x=a;
				} else { 
					w=a-x_curseur;
					x=x_curseur;
				}
				if(y_curseur > b) {
					h=y_curseur-b;
					y=b;
				} else { 
					h=b-y_curseur;
					y=y_curseur;
				}
				rectangle(matrix_r, matrix_g, matrix_b, height, width, x, y, w, h, r_fg, g_fg, b_fg, r_bg, g_bg, b_bg);
				x_curseur=a; y_curseur=b;
			} else if (!strcmp(str, "RECA") ) {
				int x,y,w,h;
				if(a>0) {
					w=a;
					x=x_curseur;
				} else { 
					w=-a;
					x=x_curseur+a;
				}
				if(b>0) {
					h=b;
					y=y_curseur;
				} else { 
					h=-b;
					y=y_curseur+b;
				}
				rectangle(matrix_r, matrix_g, matrix_b, height, width, x, y, w, h, r_fg, g_fg, b_fg, r_bg, g_bg, b_bg);
				x_curseur+=a; y_curseur+=b;
			} else if (!strcmp(str, "CERC") ) {
				cercle(matrix_r, matrix_g, matrix_b, height, width, x_curseur, y_curseur, a, r_fg, g_fg, b_fg, r_bg, g_bg, b_bg);
			} else if (!strcmp(str, "FGCO") ) {
				if(a<0 || a>255 || b<0 || b>255 || c<0 || c>255) {
					printf("Warning : Couleur non définie\n");
					return;
				}
				r_fg =a; g_fg =b; b_fg =c;
			} else if (!strcmp(str, "BGCO") ) {
				if(a<0 || a>255 || b<0 || b>255 || c<0 || c>255) {
					printf("Warning : Couleur non définie\n");
					return;
				}
				r_bg =a; g_bg =b; b_bg =c;
			}
		}
	}
	fclose(fp); //Fermeture du fichier


	// enregistrement de l'image
	image_save("test.ppm", matrix_r, matrix_g, matrix_b, height, width);
	return 0;
}

