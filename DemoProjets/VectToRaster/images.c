
#include "images.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/**
* Fonction d'affichage d'un message d'erreur
*/
static inline void info_trace(char *function, char *error, char* msg)
{
	printf("[Trace %s] :: %s (%s)\n", function, error, msg);
	exit(1);
}


int** new_matrix(int vsize,int hsize)
{
	int i;
	int **matrix;
	int *imptr;

	matrix=(int**)malloc( sizeof(int*)*vsize );
	if (matrix==NULL) printf("Probleme d'allocation memoire");

	imptr=(int*)malloc( sizeof(int)*hsize*vsize );
	if (imptr==NULL) printf("Probleme d'allocation memoire");
	 
	for(i=0;i<vsize;i++,imptr+=hsize) 
		matrix[i]=imptr;
	
	return matrix;
}

void new_image(int*** p_matrix_r, int*** p_matrix_g, int*** p_matrix_b, int height, int width)
{
	int i,j;
	*p_matrix_r = new_matrix(height, width);
	*p_matrix_g = new_matrix(height, width);
	*p_matrix_b = new_matrix(height, width);
	int **line = *p_matrix_r;
	for(i=0; i< height; i++, line++) {
		int *col=*line;
		for(j=0; j<width; j++, col++) {
			*col=0;
		}
	}
	line = *p_matrix_g;
	for(i=0; i< height; i++, line++) {
		int *col=*line;
		for(j=0; j<width; j++, col++) {
			*col=0;
		}
	}
	line = *p_matrix_b;
	for(i=0; i< height; i++, line++) {
		int *col=*line;
		for(j=0; j<width; j++, col++) {
			*col=0;
		}
	}
}


void image_load(char *file_name, int*** p_matrix_r, int*** p_matrix_g, int*** p_matrix_b, int *p_height, int *p_width)
{
	FILE *file;
	char log[200];
	int depth, i,j;
	unsigned char color_canal;

	// On ouvre un fichier en lecture
	file=fopen(file_name, "r");
	if(file == NULL) {
		info_trace("image_load", "Impossible d'ouvrir le fichier", file_name);
	}
	
	// fgets la première ligne du fichier et le place dans la variable log (dans la limite de la taille de log)
	fgets(log, 4, file);
	if(strcmp(log, "P6\n")) {
		info_trace("image_load", "Mauvais format d'image", "NOT P6");
	}

	// sur la ligne suivante du fichier se trouve les éventuels commentaires de GIMP (n'utilise pas des commentaires multilignes), on les lits, mais on n'en fait rien
	fgets(log, 200, file);

	// lecture formatée de la taille de l'image et de la profondeur de l'image
	fscanf(file, "%d %d", p_width, p_height);
	fscanf(file, "%d\n", &depth);

	// Allocation mémoire de la place pour une nouvelle matrice
	*p_matrix_r = new_matrix(*p_height, *p_width);
	*p_matrix_g = new_matrix(*p_height, *p_width);
	*p_matrix_b = new_matrix(*p_height, *p_width);

	// lecture successive, 3 par 3, des emplacements mémoires. Le fichier est organisé de la sorte rgbrgbrgbrgb... pour coder les pixels
	for(i=0;i<*p_height;i++) {
		for(j=0;j<*p_width;j++)
		{
			fread(&color_canal,1,1,file);
			(*p_matrix_r)[i][j] = (int)color_canal;
			fread(&color_canal,1,1,file);
			(*p_matrix_g)[i][j] = (int)color_canal;
			fread(&color_canal,1,1,file);
			(*p_matrix_b)[i][j] = (int)color_canal;
		}
	}
	//On ferme le fichier
	fclose(file);
}


void image_save(char* file_name, int **matrix_r, int **matrix_g, int **matrix_b, int height, int width)
{
	FILE *file;
	int i, j;

	file = fopen(file_name, "w");
	if(file == NULL) {
		info_trace("ppm_save", "Impossible d'ouvrir le fichier", file_name);
	}

	fprintf(file, "P6\n");
	fprintf(file, "# Image generee manuellement\n");
	fprintf(file, "%d %d\n", width, height);
	fprintf(file, "255\n");

	for(i=0;i<height;i++) {
		for(j=0;j<width;j++){
			//Ecriture dans le fichier
			fprintf(file, "%c%c%c", (char)matrix_r[i][j], (char)matrix_g[i][j], (char)matrix_b[i][j]);
		}
	}
	fclose(file);
}



