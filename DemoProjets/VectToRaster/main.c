
#include "images.h"
#include <stdio.h>

int main(int argc, char** argv)
{
	// Representation d'une image sous la forme de trois matrices : 3 couches R, G, B. 
	// ex. matrix_r[34][12] est la valeur de la composante rouge du pixel à la ligne 34 et à la colonne 12.
	//	-> Les valeurs de composantes doivent être entre 0 et 255.
	//	-> Rappel : Aucune vérification n'est faite sur 1) l'accès à une case existante, 2) la valeur d'une composante dans les limites
	int **matrix_r, **matrix_g, **matrix_b;
	int height, width;

	// Chargement d'une image
	image_load("Image.ppm",&matrix_r, &matrix_g, &matrix_b, &height, &width);

	// Recopie d'une image
	int **matrix_r_2, **matrix_g_2, **matrix_b_2;
	new_image(&matrix_r_2, &matrix_g_2, &matrix_b_2, height,width);
	
	int i,j;
	for(i=0; i< height; i++) {
		for(j=0; j<width; j++) {
			matrix_r_2[i][j]=matrix_r[i][j];
			matrix_g_2[i][j]=matrix_g[i][j];
			matrix_b_2[i][j]=matrix_b[i][j];
			
			// On peut également "afficher" l'image
			//printf("[%d, %d] : %lf %lf %lf\n", i,j, matrix_r[i][j], matrix_g[i][j], matrix_b[i][j]);
		}
	}

	// enregistrement de l'image
	image_save("test.ppm", matrix_r_2, matrix_g_2, matrix_b_2, height, width);
	return 0;
}

