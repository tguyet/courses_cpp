
/*
* Fonction de chargement d'une image à partir d'un fichier. 
* La fonction lit le fichier file_name (au format d'image ppm) et créés des matrices correspondantes aux couches rouge, verte et bleue de l'image. L'utilisateur de la fonction doit passer en paramètre des pointeurs sur des matrices à remplir. La fonction alloue l'espace nécessaire à l'image. L'utilisateur doit passer des pointeurs sur des variables qui recevront la taille de l'image (hauteur, largeur)
* 	file_name : nom du fichier à ouvrir
*	p_matrix_r, p_matrix_g, p_matrix_b : pointeurs sur des matrices 2D cosntruites
*	p_length, p_width : pointeur remplit par la taille de l'image
*/
void image_load(char *file_name, int*** matrix_r, int*** matrix_g, int*** matrix_b, int *height, int *width);


/**
* Fonction d'enregistrement d'une image dans un fichier
* Les paramètres sont :
*	file_name : le nom du fichier dans lequel enregistrer l'image, 
*	matrix_r, matrix_g, matrix_b : les matrices des couches de l'image,
*	height, width : la taille de l'image.
*/
void image_save(char* filename, int **matrix_r, int **matrix_g, int **matrix_b, int height, int width);


/*
* Fonction d'allocation d'une matrice 2D de taille vsize x hsize.
* Pour faire une image, il faut 3 matrices.
*/
int** new_matrix(int vsize,int hsize);
