/*********************
* \file demineur.c
* \author T. Guyet, Agrocampus Ouest
* \date mars 2012
* Programme de démineur
*/


#include <stdio.h>
#include <time.h>


#define DIMENSION 8	//Taille de tableau
#define NBMINES 4	//Nombre de mines dans le tableau


int masque[DIMENSION][DIMENSION]; 	// Représentation du masque : définie les cases visibles ou non pas le joueur, lorsqu'une case est 0, elle est masquée. Si elle est 1 alors elle est affichée. Finalement si elle vaut 2, alors elle correspond à une case marquée (mais cachée)

int mines[DIMENSION][DIMENSION]; 	// Représentation des mines : -1, il y a une mine, sinon le nombre indique le nombre de mines à côté de la case


/**
* Détermine combien il y a de mines qui touchent la case (i,j)
*/
int compter(int i, int j) {
	int nb=0;
	int bi,fi,bj,fj;
	if( i == 0 ) {
		bi=i;
		fi=i+1;
	} else if ( i==(DIMENSION-1) ){
		bi=i-1;
		fi=i;
	} else {
		bi=i-1;
		fi=i+1;
	}
	if( j == 0 ) {
		bj=j;
		fj=j+1;
	} else if ( j==(DIMENSION-1) ){
		bj=j-1;
		fj=j;
	} else {
		bj=j-1;
		fj=j+1;
	}

	for(i=bi;i<=fi;i++) {
		for(j=bj;j<=fj;j++) {
			if( mines[i][j]==-1 ) {
				nb++;
			}
		}
	}
	return nb;
}

/**
* Initialisation des tableaux mines et masques
*/
void init_mines() {
	int i,j;
	for(i=0; i<DIMENSION;i++) {
		for(j=0; j<DIMENSION;j++) {
			mines[i][j]=0;
		}
	}
	for(i=0; i<DIMENSION;i++) {
		for(j=0; j<DIMENSION;j++) {
			masque[i][j]=0;
		}
	}
	for(i=0; i<NBMINES;i++) {
		int x=rand()%DIMENSION;
		int y=rand()%DIMENSION;
		while( mines[x][y]==-1 ) {
			x=rand()%DIMENSION;
			y=rand()%DIMENSION;
		}
		mines[x][y]=-1;
	}
	for(i=0; i<DIMENSION;i++) {
		for(j=0; j<DIMENSION;j++) {
			if( mines[i][j]==0 )
				mines[i][j]=compter(i,j);
		}
	}
}


/**
* Affichage du jeu
* \param t si t vaut 1, c'est un affichage normal, si t vaut 0 alors on affiche uniquement les mines
*/
void print(int t) {
	int i,j;
	for(i=0; i<DIMENSION;i++) {
		for(j=0; j<DIMENSION;j++) {
			if( !t ) {
				if( mines[i][j]==-1 ) {
					printf("X ");
				} else {
					printf(" ");
				}
			} else {
				if( masque[i][j]==0 ) {
					printf("? ");
				} else if( masque[i][j]==2 ) {
					printf("P ");
				} else {
					if( mines[i][j]==0 ) {
						printf("  ");
					} else {
						printf("%d ", mines[i][j]);
					}
				}
			}
		}
		printf("\n");
	}
}

/**
* Vérification que la case (i,j) contient une mine
* \return 1 si c'est une mines, 0 sinon
*/
int boum(int i, int j) {
	if( mines[i][j]==-1 ) {
		return 1;
	}
	return 0;
}

/**
* Vérification de la fin du jeu
*/
int gagne() {
	int nb=0, i, j;
	for(i=0; i<DIMENSION;i++) {
		for(j=0; j<DIMENSION;j++) {
			if( masque[i][j]==0 || masque[i][j]==2) nb++;
		}
	}
	return nb==NBMINES;
}

/**
* \brief Fonction permettant de découvrir une zone récursive
*/
void decouvre(int i, int j) {
	if( mines[i][j]==-1 || masque[i][j]==1 ) {
		return;
	} else  {
		masque[i][j]=1;
		if( mines[i][j]!=0 ) return;

		int bi,fi,bj,fj;
		if( i == 0 ) {
			bi=i;
			fi=i+1;
		} else if ( i==(DIMENSION-1) ){
			bi=i-1;
			fi=i;
		} else {
			bi=i-1;
			fi=i+1;
		}
		if( j == 0 ) {
			bj=j;
			fj=j+1;
		} else if ( j==(DIMENSION-1) ){
			bj=j-1;
			fj=j;
		} else {
			bj=j-1;
			fj=j+1;
		}

		int x,y;
		for(x=bi;x<=fi;x++) {
			for(y=bj;y<=fj;y++) {
				if( i==x && y==j ) continue;
				decouvre(x,y);
			}
		}
	}
}


/**
* Programme principale
*/
int main ()
{
	int x, y, nb, m;
	srand ( time(NULL) );
	init_mines();

	while(1) {
		print(1);
		printf("rentrer les coordonnées suivis d'un 0 ou un 1 pour faire le marquage\n");
		if( 3!=scanf("%d %d %d", &x, &y, &m) || x<0 || x>=DIMENSION || y<0 || y>=DIMENSION ) {
			continue;
		}
		x--;
		y--;
		if( m!= 0 ) {
			masque[x][y]=2;
		} else {
			if( boum(x,y) ) {
				printf("BOUM\n");
				print(0);
				break;
			}
			decouvre(x,y);
			if( gagne() ) {
				print(1);
				printf("Gagné!!");
				break;
			}
		}
	}

}
