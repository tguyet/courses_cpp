#include <iostream>
#include <istream>
#include <sstream>
#include <fstream>

#include <jpeg-c++.h>

#define HSIZE 20

using namespace std;

void chargerHisto(char *file, int **histo, int *size);
void sauverHisto(char *file, int *histo, int size);

void calculerHisto(unsigned char *img,int height,int width, int *histo[],int size);
void normalizedHisto(int **histo,int size);

int main(int argc, char* argv[]) {
  std::ostringstream jpeg_in_memory;
  jpeg::Decompress jpg_to_bytes(jpeg_in_memory);
  jpeg::Compress bytes_to_jpg;

  int width,height,depth,size;
  std::ifstream foo;
  unsigned char *img_histoEx;

  int *histo=new int[HSIZE*HSIZE*HSIZE];
  int Hsize=HSIZE;

  //Ouverture de l'image exemple
  foo.open(argv[1]);
  if(!foo)
    {
      std::cerr << "Cannot read Histo \""<< argv[2]<<"\" file. Aborting." << std::endl;
      return 1;
    }
  jpg_to_bytes.setInputStream(foo);
  jpg_to_bytes.readHeader(width,height,depth);
  size = width*height*depth;
  img_histoEx = new unsigned char[size];
  jpg_to_bytes.readImage(img_histoEx);
  foo.close();

  //Construction de l'histogramme
  calculerHisto(img_histoEx, height, width, &histo, Hsize);

  delete[](img_histoEx);

  //Normalisation
  normalizedHisto(&histo, Hsize);

  //Enregistrement
  sauverHisto("histoVert.his",histo,Hsize);
  delete[](histo);

  return 1;
}

/**
\fn calculerHisto(unsigned char *img,int height,int width, int *histo[],int size)
\brief Construction d'un histogramme de couleur à partir d'une image d'exemples
\param img pointeur sur une image exemple (matrice de char de taille heigh*width=
\param width,height taille de l'image
\param histo pointeur sur l'histogramme resultant (doit être alloué à l'exterieur)
\param size taille de l'histogramme (précision entre 1 et 256)
*/
void calculerHisto(unsigned char *img,int height,int width, int *histo[],int size)
{
    //Mise à zero de l'histogramme
    for(int i=0; i<size;i++) {
        for(int j=0; j<size;j++) {
            for(int k=0; k<size;k++) {
                (*histo)[i+(j+k*size)*size]=0;
            }
        }
    }
    //Construction effective de l'histogramme
    unsigned char *p=img;
    for(int i=0; i<height;i++) {
        for(int j=0; j<width;j++) {
            int r=(int)*(p++);
            int g=(int)*(p++);
            int b=(int)*(p++);

            int ri=(int)(size*(double)r/256);
            int gi=(int)(size*(double)g/256);
            int bi=(int)(size*(double)b/256);
            (*histo)[ri+(gi+bi*size)*size]++;
        }
    }
}

/**
\fn normalizedHisto(int *histo[],int size)
\brief Normalisation de l'histogramme entre 1 et 256
*/
void normalizedHisto(int *histo[], int size)
{
    // Recherche du max
    int max=0;
    int *p=*histo;
    for(int i=0; i<size;i++) {
        for(int j=0; j<size;j++) {
            for(int k=0; k<size;k++) {
                max=(max>*p?max:*p);
                p++;
            }
        }
    }
    //Normalisation
    p=*histo;
    for(int i=0; i<size;i++) {
        for(int j=0; j<size;j++) {
            for(int k=0; k<size;k++) {
                *p=(int)(256*(double)*p/(double)max);
                p++;
            }
        }
    }
}

/**
\fn sauverHisto(char *file, int *histo, int size)
Le format d'enregistrement est : sur la première ligne, on donne la taille de l'histogramme (size), puis sur les lignes suivantes on retranscrit l'histogramme.
\brief Fonction d'enregistrement d'un histogramme
\param file Nom du fichier dans lequel enregistrer
\param histo histogramme à enregistrer (vecteur de taille size*size*size)
\param size Taille de l'histogramme
*/
void sauverHisto(char *file, int *histo, int size)
{
    ofstream fin;
    fin.open(file,ofstream::out);
    if (!fin) {
        std::cerr << "Cannot read \""<< file <<"\" file. " << std::endl;
        return;
    }
    fin<< size <<"\n";
    int *p=histo;
    for(int i=0;i<size*size*size;i++,p++) {
        fin<< *p <<"\n";
    }
}

/*
void chargerHisto(char *file, int **histo, int *size)
{
    ifstream fin;
    fin.open(file,ifstream::in);
    
    if (!fin) {
        histo=NULL;
        *size=0;
        std::cerr << "Cannot read \""<< file <<"\" file. " << std::endl;
        return;
    }
 
    string s;
    getline(fin,s);
    *size=atoi(s.data());
    int taille=(int)(256/(*size));
    taille=taille*taille*taille;
    *histo = new int[taille];
    int *p=*histo;
    while (!fin.eof()) {
        string s;
        getline(fin,s);
        *p=atoi(s.data());
        p++;
    }
}
*/

