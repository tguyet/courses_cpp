/*   This file is part of JPEG-C++
 *
 *   Copyright (C) 2006,  Supelec
 *
 *   Author : Herve Frezza-Buet
 *
 *   Contributor :
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   Lesser General Public License for more details.
 *   
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *   Contact : Herve.Frezza-Buet@supelec.fr
 *
 */

#include <iostream>


#ifndef jpegSTREAM_H
#define jpegSTREAM_H

namespace jpeg {

  /**
   * Inherit from this class to allow jpeg decompression from data.
   */ 
  class IStream {
  public:
    IStream(void) {};
    virtual ~IStream(void) {};

    /**
     * Tries to read nb_to_read bytes, and stores them in a large
     * enough byte buffer dest_buf.
     */
    virtual void readBytes(char* dest_buf,int nb_to_read)=0;

    /**
     * Returns the number of bytes actually read by last readBytes call.
     */
    virtual int countReadBytes(void)=0;
  };

  /**
   * This allow the management of std::istream object.
   */
  class IStdStream : public IStream {
  private :
      
    std::istream* input; 

  public:
      
    IStdStream(void) : IStream(), input(NULL) {};
    virtual ~IStdStream(void) {};

    void setStream(std::istream& is) {input = &is;} 
    virtual void readBytes(char* dest_buf,int nb_to_read) {
      input->read(dest_buf,nb_to_read);
    }

    virtual int countReadBytes(void) {
      return (int)(input->gcount());
    }
  };

  /**
   * Inherit from this class to allow jpeg decompression from data.
   */ 
  class OStream {
  public:
    OStream(void) {};
    virtual ~OStream(void) {};

    /**
     * writes nb_to_read bytes.
     */
    virtual void writeBytes(char* src_buf,int nb_to_write)=0;
  };

  /**
   * This allow the management of std::istream object.
   */
  class OStdStream : public OStream {
  private :
      
    std::ostream* output; 

  public:
      
    OStdStream(void) : OStream(), output(NULL) {};
    virtual ~OStdStream(void) {};

    void setStream(std::ostream& os) {output = &os;} 
    
      
    virtual void writeBytes(char* src_buf,int nb_to_write) {
      output->write(src_buf,nb_to_write);
    }
      
  };


}

#endif
