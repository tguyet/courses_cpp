
1) Compiler les deux applications 
    - ConstruireHisto
    - RedressImage

Un simple Make doit suffire.

2) Utilisation

./ConstruireHisto Histo.jpg

Histo.jpg est une image JPEG contenant des points dont la couleur est représentative des marques de coins recherchées
> La fonction construit une fichier histoVert.his !

!!! ATTENTION !!! Il faut bien vérifier que les fichiers sont accessibles en lecture et ecriture !!


./RedressImage image.jpg histoVert.his
