#include <iostream>
#include <istream>
#include <sstream>
#include <fstream>

#include <jpeg-c++.h>

#define MAX_SEUIL 70
#define MIN_SEUIL 10
#define POINT_DIAMETER 30

using namespace std;

void chargerHisto(char *file, int **histo, int *size);
int valHisto(int r,int g, int b, int *histo, int size);

bool findBest4(unsigned char *img_bw,int height,int width, int diam, int **X, int **Y);
bool findBest(unsigned char *img_bw,int height,int width, int diam, int xl, int xr, int yu, int yb, int *X, int *Y);
unsigned char evalPoint(unsigned char *img_bw_point,  int width, int diam);

//Fonction pour la resolution du systeme :
void gauss_jordan(double m1[100][100],double m2[100][100],int l,int c);
void copie_mat(double m[100][100],double m1[100][100],int l,int c);
void identite_mat(double m[100][100],int l);

bool calibration(int *X,int *Y, int width, int height, int marge, double **M);
void calcPos(double *M, int Xpos, int Ypos, int *x,int *y);

int main(int argc, char* argv[]) {

  int seuil=MAX_SEUIL;        //!< Valeur de seuil pour la detection de couleur
  int widthOut=900;     //!< Taille de l'image de sortie
  int heightOut=1200;    //!< Taille de l'image de sortie
  bool ok=false;
  int marge=50;			//!< Marge de postionnement des marques sur le cadre (en pixels)
  
  std::ostringstream jpeg_in_memory;
  jpeg::Decompress jpg_to_bytes(jpeg_in_memory);

  jpeg::Compress   bytes_to_jpg;

  int width,height,depth,size;
  std::ifstream foo;
  std::ifstream foo2;
  std::ofstream bar;
  unsigned char* img_buffer, *img_buffer_out,*img_bw;

  int *histo=new int[1000];
  int Hsize=10;

  // Chargement du modele de l'histogramme  
  chargerHisto(argv[2],&histo,&Hsize);

  foo2.open(argv[1]);
  if(!foo2)
    {
      std::cerr << "Cannot read \""<< argv[1]<<"\" file. Aborting." << std::endl;
      return 1;
    }
  jpg_to_bytes.setInputStream(foo2); 

  //Ouverture de l'image :
  jpg_to_bytes.readHeader(width,height,depth);
  size = width*height*depth;
  img_buffer = new unsigned char[size];
  jpg_to_bytes.readImage(img_buffer);
  foo2.close();
  
  //Calcule des proba par couleurs
  img_bw=new unsigned char[width*height];
  unsigned char *p=img_buffer;
  unsigned char *pbw=img_bw;
  for(int i=1; i<height;i++) {
     for(int j=1; j<width;j++) {
       int r=(int)*(p++);
       int g=(int)*(p++);
       int b=(int)*(p++);

       *pbw=(unsigned char)valHisto(r,g,b,histo,Hsize);
       pbw++;
     }
  }
  //Imb_bw contient l'image des probabilites de presence
  delete[](histo);
  
  int *Xpos= new int[4];
  Xpos[0]=0;Xpos[1]=0;Xpos[2]=0;Xpos[3]=0;
  int *Ypos= new int[4];
  Ypos[0]=0;Ypos[1]=0;Ypos[2]=0;Ypos[3]=0;
  
  while(!ok && seuil>MIN_SEUIL) {
    //Seuillage
    pbw=img_bw;
    for(int i=1; i<height;i++) {
        for(int j=1; j<width;j++) {
            if(*pbw>=seuil)
                *pbw=255;
            else
                *pbw=0;
            pbw++;
        }
    }

    //Zonage en 4 et recuperation des meilleurs
    ok=findBest4(img_bw,height,width,POINT_DIAMETER, &Xpos, &Ypos);
    if(!ok) {
        seuil-=10;
        printf("\t abaissement du seuil : %d\n", seuil);
    }
  }

  if(!ok) {
    printf("Impossible de trouver les 4 points\n");
    
    bar.open("out_seuilError.jpg");
    bytes_to_jpg.setOutputStream(bar);
    bytes_to_jpg.writeImage(width,height,1,img_bw,100);
    bar.close();
    
    delete[](Xpos);
    delete[](Ypos);
    delete[](img_bw);
    delete[](img_buffer);
    return 0;
  }
  
  delete[](img_bw);
  
  //Construction de la matrice de transformation projective
  double *M=new double[9];
  ok=calibration(Xpos,Ypos,widthOut,heightOut,marge,&M);
  if(!ok) {
    printf("Impossible de calibrer la transformation\n");
    return 0;
  }
  delete[](Xpos);
  delete[](Ypos);
  
  //Recalcul de la nouvelle image
  img_buffer_out=new unsigned char[widthOut*heightOut*3];
  p=img_buffer_out;
  for(int i=0; i<heightOut;i++) {
     for(int j=0; j<widthOut;j++) {
        int x,y;
        
        calcPos(M, i, j, &x, &y);

        if(x<0 || y<0 || x>=width || y>=height) {
            *p=0;
            p++;
            *p=0;
            p++;
            *p=0;
            p++;
        } else {
            unsigned char *val=img_buffer+3*(y*width+x);
            *p=*(val);
            p++;
            *p=*(val+1);
            p++;
            *p=*(val+2);
            p++;
        }
     }
  }
  delete[](img_buffer);
  delete[](M);


  if(argc>=4) {
    bar.open(argv[3]);
  } else {
    bar.open("out.jpg");
  }

  if(!bar)
    {
      std::cerr << "Cannot write \"bar.jpg\" file. Aborting." << std::endl;
      return 1;
    }
  bytes_to_jpg.setOutputStream(bar);
  bytes_to_jpg.writeImage(widthOut,heightOut,3,img_buffer_out,100 /* quality */);
  bar.close();

  delete(img_buffer_out);

  return 1;
}

/**
\fn valHisto(int r,int g, int b, int *histo, int size)
\brief Evaluation d'un pixel par rapport à l'histogramme
*/
int valHisto(int r,int g, int b, int *histo, int size)
{
    if(histo==NULL) {
        std::cerr << "Histo non chargee\n";
        return 0;
    }
    int ri=(int)(size*(double)r/256);
    int gi=(int)(size*(double)g/256);
    int bi=(int)(size*(double)b/256);
    return histo[ri+(gi+bi*size)*size];
}

/**
\fn chargerHisto(char *file, int **histo, int *size)
\brief Chargement d'un histogramme des couleurs pour les points recherches
*/
void chargerHisto(char *file, int **histo, int *size)
{
    ifstream fin;
    fin.open(file,ifstream::in);

    if (!fin) {
        histo=NULL;
        *size=0;
        std::cerr << "Cannot read \""<< file <<"\" file. " << std::endl;
        return;
    }

    string s;
    getline(fin,s);
    *size=atoi(s.data());
    int taille= (*size) * (*size)* (*size);
    *histo = new int[taille];
    int *p=*histo;
    int j=0;
    while (!fin.eof() && j<taille ) {
        string s;
        getline(fin,s);
        *p=atoi(s.data());
        p++;
        j++;
    }
}


/**
* \fn findBest4(unsigned char *img_bw,int height,int width, int diam, int **X, int **Y)
* \brief Partage de l'image en 4 zones pour trouver quatres points.
*/
bool findBest4(unsigned char *img_bw,int height,int width, int diam, int **X, int **Y)
{
    bool ret=true;
    if( (*X)[0]==0 ) {
        if(!findBest(img_bw, height, width, diam,  diam,    width/2,    diam,     height/2,    &((*X)[0]), &((*Y)[0])))
            ret=false;
    }
    if( (*X)[1]==0 ) {
        if(!findBest(img_bw, height, width, diam,  width/2, width-diam, diam,     height/2,    &((*X)[1]), &((*Y)[1])))
            ret=false;
    }
    if( (*X)[2]==0 ) {
        if(!findBest(img_bw, height, width, diam,  diam,    width/2,    height/2, height-diam, &((*X)[2]), &((*Y)[2])))
            ret=false;
    }
    if( (*X)[3]==0 ) {
        if(!findBest(img_bw, height, width, diam,  width/2, width-diam, height/2, height-diam, &((*X)[3]), &((*Y)[3])))
            ret=false;
    }
    return ret;
}

/**
* \fn findBest(unsigned char *img_bw,int height,int width, int diam, int xl, int xr, int yu, int yb, int *X, int *Y)
* \brief Recherche dans une zone la position comportant le plus de points blanc dans une zone de diametre diam
*/
bool findBest(unsigned char *img_bw,int height,int width, int diam, int xl, int xr, int yu, int yb, int *X, int *Y)
{
    int max=0;
    int argmaxX=-1;
    int argmaxY=-1;
    unsigned char *pbw=img_bw+(yu*width+xl);
    for(int i=0; i<yb-yu;i++) {
        for(int j=0; j<xr-xl;j++) {
            int val=evalPoint(pbw, width, diam);
            if(val>max) {
                max=val;
                argmaxX=xl+j,
                argmaxY=yu+i;
            }
            pbw++;
        }
        pbw+=width/2+diam;
    }
    if(max==0)
        return false;
    *X=argmaxX;
    *Y=argmaxY;
    return true;
}

/**
* \fn evalPoint(unsigned char *img_bw_point, int width, int diam)
* Zone parcourue verifiee, mais deborde (en lecture uniquement !) !!
* \brief 
*/
unsigned char evalPoint(unsigned char *img_bw_point, int width, int diam)
{
    unsigned char count=0;
    for(int i=-diam/2; i<diam/2;i++) {
        for(int j=-diam/2; j<diam/2;j++) {
            if(*img_bw_point==255) count++;
            img_bw_point++;
        }
        img_bw_point+=width-diam;
    }
    return count;
}


/**
* \fn gauss_jordan(double m1[100][100],double m2[100][100],int l,int c)
* \brief Inversion de matrice par une méthode de Gauss-Jordan
*/
void gauss_jordan(double m1[100][100],double m2[100][100],int l,int c)
{
    int i;
    int j;
    double tmp,tmp2;
    double m3[100][100];
    double m4[100][100];
    int k=0;
    identite_mat(m4,l);

    for(i=0;i<l;i++)
    {
        copie_mat(m1,m3,l,c);

        //Placement du 1!
        tmp=m1[i][i];

        //Switch de ligne si lepivot est nul
        if(tmp==0) {
            bool ok=false;
            for(j=i+1;j<l;j++) {
                if(m1[j][i]!=0) {
                    //switch i <-> j
                    for(int mj=0;mj<c;mj++){
                        m1[i][mj]=m3[j][mj];
                        m1[j][mj]=m3[i][mj];
                    }
                    ok=true;
                    break;
                }
            }
            if(!ok) {
                printf("Erreur : gauss_jordan, Rang de la matrice insuffisant\n");
                return;
            }
            tmp=m1[i][i];
            copie_mat(m1,m3,l,c);
        }
        for(j=0;j<c;j++)
        {
            m1[i][j]=m1[i][j]/tmp;
            m4[i][j]=m4[i][j]/tmp;
        }
        //Placement des 0
        for(k=0;k<l;k++)
        {
            if(k!=i)
            {
                tmp2=m1[k][i]/tmp;
                for(j=0;j<c;j++)
                {
                    m1[k][j]=m1[k][j]-tmp2*m3[i][j];
                    m4[k][j]=m4[k][j]-tmp2*m4[i][j];
                }
            }
        }
    }
    copie_mat(m4,m2,l,c);
}

void copie_mat(double m[100][100],double m1[100][100],int l,int c)
{
    int i,j;
    for(i=0;i<l;i++) {
        for(j=0;j<c;j++) m1[i][j]=m[i][j];
    }
}


void identite_mat(double m[100][100],int l)
{
    int i,j;
    for(i=0;i<l;i++) {
        for(j=0;j<l;j++) m[i][j]=0;
        m[i][i]=1;
    }
}

/**
\fn calibration(int *X,int *Y, int width, int height, double **M) 
\brief Fonction qui construit la matrice de transformation projective à partir des positions des repères dans l'image.
\param X vecteur des 4 abscisses des 4 points de référence
\param Y vecteur des 4 ordonnées des 4 points de référence
\param width Largeur de sortie
\param height Hauteur de sortie
\param marge Nombre de pixel définissant la position des marques dans le cadre
\param M Pointeur sur une matrice codant la transformation homomorphique
\return Vrai si la calibration est ok, faux sinon (Probleme d'inversion de matrice)
*/
bool calibration(int *X,int *Y, int width, int height, int marge, double **M) 
{
    double c[100][100], d[100][100];
    int i1,j1,i2,j2,i3,j3,i4,j4;

    //Construction du systeme de calibrage!
    /*
    i1=0;
    j1=0;
    i2=height;
    j2=0;
    i3=0;
    j3=width;
    i4=height;
    j4=width;
    */
    i1=marge;
    j1=width-marge;
    i2=height-marge;
    j2=width-marge;
    i3=marge;
    j3=marge;
    i4=height-marge;
    j4=marge;

    c[0][0]=i1;
    c[0][1]=j1;
    c[0][2]=1;
    c[0][3]=0;
    c[0][4]=0;
    c[0][5]=0;
    c[0][6]=-X[0]*i1;
    c[0][7]=-X[0]*j1;
    c[0][8]=-X[0];

    c[1][3]=i1;
    c[1][4]=j1;
    c[1][5]=1;
    c[1][0]=0;
    c[1][1]=0;
    c[1][2]=0;
    c[1][6]=-Y[0]*i1;
    c[1][7]=-Y[0]*j1;
    c[1][8]=-Y[0];

    c[2][0]=i2;
    c[2][1]=j2;
    c[2][2]=1;
    c[2][3]=0;
    c[2][4]=0;
    c[2][5]=0;
    c[2][6]=-X[1]*i2;
    c[2][7]=-X[1]*j2;
    c[2][8]=-X[1];

    c[3][3]=i2;
    c[3][4]=j2;
    c[3][5]=1;
    c[3][0]=0;
    c[3][1]=0;
    c[3][2]=0;
    c[3][6]=-Y[1]*i2;
    c[3][7]=-Y[1]*j2;
    c[3][8]=-Y[1];

    c[4][0]=i3;
    c[4][1]=j3;
    c[4][2]=1;
    c[4][3]=0;
    c[4][4]=0;
    c[4][5]=0;
    c[4][6]=-X[2]*i3;
    c[4][7]=-X[2]*j3;
    c[4][8]=-X[2];

    c[5][3]=i3;
    c[5][4]=j3;
    c[5][5]=1;
    c[5][0]=0;
    c[5][1]=0;
    c[5][2]=0;
    c[5][6]=-Y[2]*i3;
    c[5][7]=-Y[2]*j3;
    c[5][8]=-Y[2];

    c[6][0]=i4;
    c[6][1]=j4;
    c[6][2]=1;
    c[6][3]=0;
    c[6][4]=0;
    c[6][5]=0;
    c[6][6]=-X[3]*i4;
    c[6][7]=-X[3]*j4;
    c[6][8]=-X[3];

    c[7][3]=i4;
    c[7][4]=j4;
    c[7][5]=1;
    c[7][0]=0;
    c[7][1]=0;
    c[7][2]=0;
    c[7][6]=-Y[3]*i4;
    c[7][7]=-Y[3]*j4;
    c[7][8]=-Y[3];

    c[8][0]=0;
    c[8][1]=0;
    c[8][2]=0;
    c[8][3]=0;
    c[8][4]=0;
    c[8][5]=0;
    c[8][6]=0;
    c[8][7]=0;
    c[8][8]=1;

    //Resolution du systeme
    gauss_jordan(c,d,9,9);

    //Construction de la matrice de transformation
    (*M)[0]=d[0][8];
    (*M)[1]=d[1][8];
    (*M)[2]=d[2][8];
    (*M)[3]=d[3][8];
    (*M)[4]=d[4][8];
    (*M)[5]=d[5][8];
    (*M)[6]=d[6][8];
    (*M)[7]=d[7][8];
    (*M)[8]=d[8][8];
    return true;
}

/**
\fn calcPos(double *M, int Xpos, int Ypos, int *x,int *y)
\brief Calcul de la position d'un pixel de l'image d'origine dans l'image transformée.
\param M matrice de transformation
\param Xpos,Ypos position dans l'image d'origine
\param x,y pointeur sur les positions dans la nouvelle image
*/
void calcPos(double *M, int Xpos, int Ypos, int *x,int *y)
{
    double a,b,c;
    a=M[0]*Xpos+M[1]*Ypos+M[2];
    b=M[3]*Xpos+M[4]*Ypos+M[5];
    c=M[6]*Xpos+M[7]*Ypos+M[8];
    *x=(int)(a/c);
    *y=(int)(b/c);
}
