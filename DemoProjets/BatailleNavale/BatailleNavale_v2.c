/**
* Jeu de bataille navale (sans intelligence artificielle)
* \author T. Guyet, AGROCAMPUS OUEST
* \date mars 2012
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 10		//< taille de la matrice de jeu
#define MAXT 5		//< nombre maximum de tests pour l'insertion


//Définition des tailles de tableau par type
#define SIZE_PORTEAVION 5
#define SIZE_CROISEUR 4
#define SIZE_CONTRETORPILLEUR 3
#define SIZE_SOUSMARIN 3
#define SIZE_TORPILLEUR 2


void print();

/**
* La fonction teste si un bateau de taille size à la position (x,y) dans l'orientation définie se superpose avec un autre bateau.
* \param orientation : 0 vers le bas, 1 vers le droite
* \return 1 si l'insertion d'un bateau est possible, 0 sinon
*/
int test_insertion(int x, int y, int size, int orientation /*0: bas, 1: droite*/, int **bateaux) {
	int i;
	if( orientation ) {
		for(i=x; i<(size+x); i++) {
			if( bateaux[i][y]!=0 ) {
				return 0;
			}
		}
	} else {
		for(i=y; i<(size+y); i++) {
			if( bateaux[x][i]!=0 ) {
				return 0;
			}
		}
	}
	return 1;
}

/**
* Positionnement d'un bateau (avec le numéro id) de taille size dans la matrice de bateaux, sans recouvrement d'un autre bateau.
* La méthode est stochastique : on génère des positions aléatoire et on regarde si c'est compatible ! Ca fonctionne super, sinon on recommence !
* Le nombre maximal de tentative est MAXT
* \param size taille du bateau
* \param id numéro du bateau
* \param bateaux tableau de bateaux déjà positionné dans lequel le bateau est ajouté !
*/
void positionner_bateau_alea(int size, int id, int **bateaux) {
	int t=0;
	int i;
	if( rand()%2 ) {
		//vers la droite
		while( t<MAXT ) {
			int posy=rand()%SIZE;
			int posx=rand()%(SIZE-size);
			if( test_insertion(posx, posy, size, 1, bateaux) ) {
				for(i=posx; i<(size+posx); i++) {
					bateaux[i][posy]=id;
				}
				break;
			}
			t++;
		}
	} else {
		//vers le bas
		while( t<MAXT ) {
			int posx=rand()%SIZE;
			int posy=rand()%(SIZE-size);
			if( test_insertion(posx, posy, size, 0, bateaux) ) {
				for(i=posy; i<(size+posy); i++) {
					bateaux[posx][i]=id;
				}
				break;
			}
			t++;
		}
	}
	if( t==MAXT ) {
		fprintf(stderr, "Erreur de positionnement du bateau %d\n", id);
		print();
		exit( EXIT_FAILURE );
	}
}

/**
* \brief Fonction de génération automatique et aléatoire de positions de bateaux
* \param nb_porteavion, nb_croiseur, ... : donne les nombres de bateaux par type qui doivent être ajoutés
* \param bateaux : matrice de bateau à remplir avec les identifiants des bateaux à leurs positions
* \return Un tableau indiquant la taille de chaque bateau (eg la i-eme valeur du tableau donne la taille du bateau i+1 !!)
*
* Attention, si les tentatives de positionnement des bateaux ne réussissent pas, la fonction sort du programme !
* Les numéros des bateaux sont générés entre 1 et le nombre total de bateaux, la correspondance avec le tableau retourné doit être décalée !
*/
int * positionner_alea(int nb_porteavion, int nb_croiseur, int nb_contretorpilleur, int nb_ssmarin, int nb_torpilleur, int **bateaux)
{
	int i=0;
	int id_avion=1;
	int *couler=(int*)malloc( sizeof(int) * (nb_porteavion + nb_croiseur + nb_contretorpilleur + nb_ssmarin + nb_torpilleur) );
	for(i=0; i<nb_porteavion; i++) {
		positionner_bateau_alea(SIZE_PORTEAVION, id_avion, bateaux);
		couler[id_avion-1]=SIZE_PORTEAVION;
		id_avion++;
	}
	for(i=0; i<nb_croiseur; i++) {
		positionner_bateau_alea(SIZE_CROISEUR, id_avion, bateaux);
		couler[id_avion-1]=SIZE_CROISEUR;
		id_avion++;
	}
	for(i=0; i<nb_contretorpilleur; i++) {
		positionner_bateau_alea(SIZE_CONTRETORPILLEUR, id_avion, bateaux);
		couler[id_avion-1]=SIZE_CONTRETORPILLEUR;
		id_avion++;
	}
	for(i=0; i<nb_ssmarin; i++) {
		positionner_bateau_alea(SIZE_SOUSMARIN, id_avion, bateaux);
		couler[id_avion-1]=SIZE_SOUSMARIN;
		id_avion++;
	}
	for(i=0; i<nb_torpilleur; i++) {
		positionner_bateau_alea(SIZE_TORPILLEUR, id_avion, bateaux);
		couler[id_avion-1]=SIZE_TORPILLEUR;
		id_avion++;
	}
	
	return couler ;
}

/**
* \brief initialisation du jeu sans bateau et aucune case visible !!
*/
void init(int **bateaux, int **ocean) {
	int i, j;
	for(i=0; i<SIZE; i++) {
		for(j=0; j<SIZE; j++) {
			ocean[i][j]=0;
			bateaux[i][j]=0;
		}
	}
}

/**
* \brief Fonction d'affichage du jeu
* \param bateaux matrice de position des bateaux
* \param ocean matrice indiquant les cases visibles (jouées)
*
* L'affichage est correct pour un nombre de bateau inférieur ou égal de 10 !
*/
void print(int **bateaux, int **ocean)
{
	int i, j;
	int red=1, green=1, blue=5;
	printf("\x1B[48;5;%dm", 16 + (red * 36) + (green * 6) + blue);
	printf("|");
	for(j=0; j<SIZE; j++) {
		printf("-");
	}
	printf("|\n");
	for(i=0; i<SIZE; i++) {
		printf("|");
		for(j=0; j<SIZE; j++) {
			if(ocean[i][j]==1) {
				if(bateaux[i][j]==0) {
					printf(" ");
				} else {
					printf("%d", bateaux[i][j]);
					//printf("X");
				}
			} else {
				printf("*");
			}
		}
		printf("|\n");
	}
	printf("|");
	for(j=0; j<SIZE; j++) {
		printf("-");
	}
	printf("|\n");
	printf("\x1B[0m\n");
}

/**
* \brief Fonction pour tester et afficher si un tire touche ou coule un bateau
* \param x,y position du tir
* \param couler matrice contenant le nombre de position restant à toucher pour chaque bateau
* \param bateaux matrice de position des bateaux
* \param infos si 0 alors seule l'information des "coulés" sont affichés, sinon les détails de l'informations sur le tire sont affichés
* \return 1 si le jeu est gagné, 0 sinon
*/
void coule(int i, int j, int *couler, int **bateaux, int infos) {
	int b;
	if( (b=bateaux[i][j])!=0 ) {
		if(infos) printf("\t>\033[31;01mLe bateau %d a été touché\033[00m\n", b);
		couler[ b-1 ]--;
		if( couler[ b-1 ]==0 ) {
			if( infos ) {
				printf("\t>\033[31;01mLe bateau %d a été COULÉ\033[00m\n", b);
			} else {
				printf("\033[31;01mCoulé !!\033[00m\n");
			}
		}
	} else if (infos) {
		printf("\t>\033[32;01mAucun bateau n'a été touché\033[00m\n");
	}
}


/**
* \brief Fonction pour tester si le jeu est gagné ou non
* \param bateaux matrice de position des bateaux
* \param ocean matrice indiquant les cases visibles (jouées)
* \return 1 si le jeu est gagné, 0 sinon
*/
int gagne(int **bateaux, int **ocean) {
	int i, j;
	for(i=0; i<SIZE; i++) {
		for(j=0; j<SIZE; j++) {
			if( bateaux[i][j]!=0 && ocean[i][j]==0 ) return 0;
		}
	}
	return 1;
}




/**
* Allocation d'une matrice de taille SIZExSIZE
* Pas de vérification de la disponibilité mémoire
*/
int **alloc_matrix() {
	int **mat = calloc( sizeof(int*), SIZE);
	int i;
	for(i=0; i<SIZE;i++) {
		mat[i]=calloc( sizeof(int), SIZE );
	}
	return mat;
}


/**
* Fonction automatique de jeu ...
* ICI, rien de particulier n'a été développé : uniquement un tirage aléatoire des positions !
*/
void jouer_auto(int *x, int *y)
{
	*x=rand()%SIZE;
	*y=rand()%SIZE;
}


void main()
{
	int x,y;
	srand( time(NULL) );
	
	// Structures de données utiles à la représentation du jeu : uniquement des matrices carrees
	// Les matrices sont allouées ici !
	
	//Masques pour les deux océans : 0 case non-visible, 1 case visible
	int **ocean_J1 = alloc_matrix();
	int **ocean_J2 = alloc_matrix();
	int **bateaux_J1 = alloc_matrix(); //Position des bateaux du premier joueur J1
	int **bateaux_J2 = alloc_matrix(); //Position des bateaux du premier joueur J2
	
	//Initialisation des tableaux à 0 (non-visible, sans bateau)
	init((int**)bateaux_J1, (int**)ocean_J1);
	init((int**)bateaux_J2, (int**)ocean_J2);
	
	//Génération aléatoire des positions de bateau
	int *couler_J1=positionner_alea(1, 2, 1, 1, 2, (int**)bateaux_J1);
	int *couler_J2=positionner_alea(1, 2, 1, 1, 2, (int**)bateaux_J2);

	int g=0;
	while( 1 ) {
		//Joueur Ordinateur
		printf("Ordinateur\n");
		jouer_auto(&x, &y); //< utilisation d'une fonction qui indique ce qu'il faut jouer !! : Intelligence artificielle au choix !!
		printf("\t>joue (%d %d)\n", x, y);
		ocean_J1[x][y]=1;
		coule(x, y, couler_J1, (int**)bateaux_J1, 1);
		g=gagne((int**)bateaux_J1, (int**)ocean_J1);
		if( g ) {
			printf("Vous avez perdu !!\n");
		}
		
		//Joueur Humain
		//On récupère les coordonnées à jouer auprès de l'utilisateur 
		printf("Joueur\n");
		printf("\033[33;01mDonner les coordonnées à jouer (entre 0 et %d)\033[33;00m\n", SIZE);
		if( 2!=scanf("%d %d", &x, &y) || x>=SIZE || x<0) {
			printf("Erreur de saisie, vous passez votre tour !\n");
			continue;
		}
		if( ocean_J2[x][y]==1 ) {
			printf("La case avait déjà été jouée !!\n");
			continue;
		}
		
		ocean_J2[x][y]=1; //< On découvre la case jouée
		print((int**)bateaux_J2, (int**)ocean_J2); //<Affichage
		coule(x, y, couler_J2, (int**)bateaux_J2, 0); //< On regarde si on a touché ou coulé un bateau
		g=gagne((int**)bateaux_J2, (int**)ocean_J2); //< On regarde si le joueur A gagné
		if( g ) {
			printf("Vous avez gagné !!\n");
		}
	}
}

