#include <stdio.h>
#include <math.h>
 
#define STEPSIZE 0.005  /* valeur du pas */
#define MAX_LOOPS 10000.0 /* maximum de points calculés*/ 
#define G 9.81

/*Variables globales : se sont les paramètres des fonction*/
double m_inv;
double S;

double f(double x, double y, double vx, double vy) {
	return -S * sqrt(vx*vx+vy*vy) * vx * m_inv;
}

double g(double x, double y, double vx, double vy) {
	return (-G-S * sqrt(vx*vx+vy*vy) * vy * m_inv;
}

int main(void)
{
	double x, y, vx,vy;
	int j=0; /* j keeps track of the step number */
	
	FILE *output=fopen("euler.dat", "w");
	
	/*parametres en USI (poids en kg*/
	m_inv = 1/0.058; //< ICI m_inv est l'inverse de la masse : il est plus rapide pour le processeur de faire des multiplications que des divisions !
	S = 0.002;
	/*Conditions initiales  en USI (positions en m, vitesses en m/s)*/
	x=0;
	y=2.7;
	vx=20;
	vy=-2.3;
	
	fprintf (output, "%g %g\n", x, y);
	
	while(1)
	{
		double vxnew=vx+STEPSIZE * f(x,y,vx,vy);
		double vynew=vy+STEPSIZE * g(x,y,vx,vy);
		double xnew=x+STEPSIZE * vx;
		double ynew=y+STEPSIZE * vy;
		
		fprintf (output, "%g %g\n", x, y);
		if( y<0 || j>=MAX_LOOPS ) break;
		
		vx=vxnew;vy=vynew;x=xnew;y=ynew;
		j++;
	}
	printf("%g %g %g %g\n",x,y,vx,vy);
	printf("ok (%d)\n",j);
	fclose(output);
	return 0;
}

