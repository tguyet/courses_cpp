
set xrange [0:23.78]
set yrange [0:3]
set size ratio -1
set mytics 1
set xtics 0,30
set arrow from 11.89,0 to 11.89,0.914
set arrow from 18.29,0 to 18.29,0.1 nohead
set nokey
plot "traj.dat" with lines
pause -1;