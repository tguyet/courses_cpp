#include <stdio.h>
#include <math.h>
 
#define STEPSIZE 0.005  /* valeur du pas */
#define MAX_LOOPS 10000.0 /* maximum de points calculés*/ 

/*Variables globales : se sont les paramètres des fonction*/
double m;
double S;

double f(double x, double y, double vx, double vy) {
	return -S * sqrt(vx*vx+vy*vy) * vx /m;
}

double g(double x, double y, double vx, double vy) {
	return -1-S * sqrt(vx*vx+vy*vy) * vy /m;
}

int main(void)
{
	double x, y, vx,vy;
	int j=0; /* j keeps track of the step number */
	
	FILE *output=fopen("traj.dat", "w");
	
	/*parametres en USI (poids en kg*/
	m = 0.058;
	S = 0.002;
	/*Conditions initiales  en USI (positions en m, vitesses en m/s)*/
	x=0;
	y=2.7;
	vx=20;
	vy=-2.3;
	
	fprintf (output, "%g %g\n", x, y);
	
	while(1)
	{
		double k1_vx=STEPSIZE * f(x,y,vx,vy);
		double k1_vy=STEPSIZE * g(x,y,vx,vy);
		double k1_x=STEPSIZE * vx;
		double k1_y=STEPSIZE * vy;
		
		double k2_vx=STEPSIZE * f(x + 0.5*k1_x, y+0.5*k1_y, vx+ 0.5*k1_vx, vy+ 0.5*k1_vy);
		double k2_vy=STEPSIZE * g(x + 0.5*k1_x, y+0.5*k1_y, vx+ 0.5*k1_vx, vy+ 0.5*k1_vy);
		double k2_x=STEPSIZE * (vx + 0.5*k1_vx);
		double k2_y=STEPSIZE * (vy + 0.5*k1_vy);
		
		double k3_vx=STEPSIZE * f(x + 0.5*k2_x, y+0.5*k2_y, vx+ 0.5*k2_vx, vy+ 0.5*k2_vy);
		double k3_vy=STEPSIZE * g(x + 0.5*k2_x, y+0.5*k2_y, vx+ 0.5*k2_vx, vy+ 0.5*k2_vy);
		double k3_x=STEPSIZE * (vx + 0.5*k2_vx);
		double k3_y=STEPSIZE * (vy + 0.5*k2_vy);
		
		double k4_vx=STEPSIZE * f(x + k3_x, y+k3_y, vx+ k3_vx, vy+ k3_vy);
		double k4_vy=STEPSIZE * g(x + k3_x, y+k3_y, vx+ k3_vx, vy+ k3_vy);
		double k4_x=STEPSIZE * (vx + k3_vx);
		double k4_y=STEPSIZE * (vy + k3_vy);
		
		double vxnew=vx+(k1_vx + 2*k2_vx + 2*k3_vx  + k4_vx)/6.;
		double vynew=vy+(k1_vy + 2*k2_vy + 2*k3_vy  + k4_vy)/6.;
		double xnew=x+(k1_x + 2*k2_x + 2*k3_x  + k4_x)/6.;
		double ynew=y+(k1_y + 2*k2_y + 2*k3_y  + k4_y)/6.;
		
		fprintf (output, "%g %g\n", x, y);
		if( y<0 || j>=MAX_LOOPS ) break;
		
		vx=vxnew;vy=vynew;x=xnew;y=ynew;
		j++;
	}
	printf("%g %g %g %g\n",x,y,vx,vy);
	printf("ok (%d)\n",j);
	fclose(output);
	return 0;
}

