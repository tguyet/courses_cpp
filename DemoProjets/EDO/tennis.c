/**
* Calcul d'une trajectoire de balle de tennis (avec effet Magnus) en utilisant 
* différentes méthodes de résolution d'une équation différentielle ordinaire.
*
* Guyet T., AGROCAMPUS-OUEST
* Compilation : gcc -o tennis -Wall -lm -O2 tennis.c
*/

#include <stdio.h>
#include <math.h>


#define MAX_LOOPS 1000000.0L //< maximum de points calculés
#define G 9.81 //<gravite

//> Définition de la structure de données représentant l'état du système dynamique à un instant donné
typedef struct {
	double vx;
	double vy;
	double x;
	double y;
	double w;
} Vecteur;

//Variables globales : se sont les paramètres des fonctions en USI (poids en kg)
//Les mots clés static const indiquent que la variable ne peut être utilisé que dans ce fichier (static) et ne peut être modifié pendant l'exécution (const) : ceci d'aider le compilo à optimiser le code
static const double m_inv= 1/0.058; //< ICI m_inv est l'inverse de la masse : il est plus rapide pour le processeur de faire des multiplications que des divisions !
static const double S=0.002;
static const double M=0.2;

//Déclaration des fonctions :
double f1(double vx, double vy, double w);
double f2(double vx, double vy, double w);
Vecteur d_dt_euler(Vecteur V_t, double h);
Vecteur d_dt_rk2(Vecteur V_t, double h);
Vecteur d_dt_rk4(Vecteur V_t, double h);


int main(void)
{
	int t=0; 		//< t keeps track of the step number
	Vecteur V_t;	//< V_t est le vecteur de l'état courant (à la date t) du système
	FILE *output=fopen("traj.dat", "w");
	
	// Conditions initiales en USI (positions en m, vitesses en m/s)
	double theta=M_PI/10;
	double v_init=18;
	V_t.x=0;
	V_t.y=0.7;
	V_t.vx=v_init*cos(theta);
	V_t.vy=v_init*sin(theta);
	V_t.w=0;

	// Quelques conditions initiales intéressantes :
	//	- v_init=18, V_t=0.7, theta=M_PI/10, V_t.w=0 : un coup droit qui sort
	//	- v_init=18, V_t=0.7, theta=M_PI/10, V_t.w=1 : on a juste ajouté un peu de slice pour que le coup droit rendre dans le terrain
	
	fprintf (output, "%g %g\n", V_t.x, V_t.y);
	
	while(1)
	{
		//V_t = d_dt_euler(V_t, 0.001);	//< Résolution par la méthode d'euler
		//V_t = d_dt_rk2(V_t, 0.001);	//< Résolution par la méthode Runge-Kutta d'ordre 2
		V_t = d_dt_rk4(V_t, 0.001);	//< Résolution par la méthode Runge-Kutta d'ordre 4
		
		fprintf (output, "%g %g\n", V_t.x, V_t.y);
		if(V_t.y<0 ) {
			//La balle à touché le sol !
			break;
		}
		if(t>=MAX_LOOPS ) {
			printf("not enough steps\n");
			fclose(output); //< attention: ne pas oublier de fermer le fichier, même lors d'un arrêt avec erreur !
			return 1;
		}
		
		t++;
	}
	printf("ok (%d)\n",t);
	fclose(output);
	return 0;
}

/**
* Fonction de calcul caractéristique du système différentiel selon les x
* Cette fonction dépend du système différentiel !
* @param vx : vitesse selon les x à t
* @param vy : vitesse selon les y à t
* @param w : vitesse de rotation à t
* @return un différentiel de vx entre t et t+1
*/
double f1(double vx, double vy, double w)
{
	return (w * M * vy - S * vx) * m_inv / sqrt(vx*vx+vy*vy);
}

/**
* Fonction de calcul caractéristique du système différentiel selon les y
* Cette fonction dépend du système différentiel !
* @param vx : vitesse selon les x à t
* @param vy : vitesse selon les y à t
* @param w : vitesse de rotation à t
* @return un différentiel de vy entre t et t+1
*/
double f2(double vx, double vy, double w)
{
	return - G - ( w * M * vx + S * vy) * m_inv / sqrt(vx*vx+vy*vy);
}

/**
* Fonction de calcul de l'état du système à l'instant t+1 à partir de l'état à l'instant t, par la méthode d'Euler
* La fonction utilise les fonctions f1 et f2
* @param V_t : état du système à l'instant t
* @param h : valeur du pas
*/
Vecteur d_dt_euler(Vecteur V_t, double h)
{
	Vecteur V;
	V.x=V_t.x+h*V_t.vx;
	V.y=V_t.y+h*V_t.vy;
	V.vx=V_t.vx+h*f1(V_t.vx,V_t.vy, V_t.w);
	V.vy=V_t.vy+h*f2(V_t.vx,V_t.vy, V_t.w);
	V.w=V_t.w;
	return V;
}

/**
* Fonction de calcul de l'état du système à l'instant t+1 à partir de l'état à l'instant t, par la méthode de runge kutta d'ordre 2
* La fonction utilise les fonctions f1 et f2
* @param V_t : état du système à l'instant t
* @param h : valeur du pas
*/
Vecteur d_dt_rk2(Vecteur V_t, double h)
{
	Vecteur V1,V2;
	
	V1.vx=h * f1(V_t.vx,V_t.vy, V_t.w);
	V1.vy=h * f2(V_t.vx,V_t.vy, V_t.w);
	V1.x=h * V_t.vx;
	V1.y=h * V_t.vy;
	V1.w=0;
	
	V2.vx=V_t.vx + h * f1( V_t.vx+ 0.5*V1.vx, V_t.vy + 0.5*V1.vy, V_t.w+ 0.5*V1.w); //On préfère multiplier par 0.5 que de diviser par 2 : c'est plus rapide !
	V2.vy=V_t.vy + h * f2( V_t.vx+ 0.5*V1.vx, V_t.vy + 0.5*V1.vy, V_t.w+ 0.5*V1.w);
	V2.x=V_t.x + h * (V_t.vx + 0.5*V1.vx);
	V2.y=V_t.y + h * (V_t.vy + 0.5*V1.vy);
	V2.w=V_t.w;
	
	return V2;
}

/**
* Fonction de calcul de l'état du système à l'instant t+1 à partir de l'état à l'instant t, par la méthode de runge kutta d'ordre 4
* La fonction utilise les fonctions f1 et f2
* @param V_t : état du système à l'instant t
* @param h : valeur du pas
*/
Vecteur d_dt_rk4(Vecteur V_t, double h)
{
	Vecteur V1,V2,V3, V4, V;
	
	V1.vx=h * f1(V_t.vx,V_t.vy, V_t.w);
	V1.vy=h * f2(V_t.vx,V_t.vy, V_t.w);
	V1.x=h * V_t.vx;
	V1.y=h * V_t.vy;
	V1.w=0;
	
	V2.vx=h * f1( V_t.vx+ 0.5*V1.vx, V_t.vy + 0.5*V1.vy, V_t.w+ 0.5*V1.w); //On préfère multiplier par 0.5 que de diviser par 2 : c'est plus rapide !
	V2.vy=h * f2( V_t.vx+ 0.5*V1.vx, V_t.vy + 0.5*V1.vy, V_t.w+ 0.5*V1.w);
	V2.x=h * (V_t.vx + 0.5*V1.vx);
	V2.y=h * (V_t.vy + 0.5*V1.vy);
	V2.w=0;
	
	V3.vx=h * f1( V_t.vx+ 0.5*V2.vx, V_t.vy + 0.5*V2.vy, V_t.w+ 0.5*V2.w);
	V3.vy=h * f2( V_t.vx+ 0.5*V2.vx, V_t.vy + 0.5*V2.vy, V_t.w+ 0.5*V2.w);
	V3.x=h * (V_t.vx + 0.5*V2.vx);
	V3.y=h * (V_t.vy + 0.5*V2.vy);
	V3.w=0;
	
	V4.vx=h * f1( V_t.vx+ V3.vx, V_t.vy + V3.vy, V_t.w+ V3.w);
	V4.vy=h * f2( V_t.vx+ V3.vx, V_t.vy + V3.vy, V_t.w+ V3.w);
	V4.x=h * (V_t.vx + V3.vx);
	V4.y=h * (V_t.vy + V3.vy);
	V4.w=0;
	
	V.vx=V_t.vx + h * (V1.vx + 2*V2.vx + 2*V3.vx  + V4.vx)/6.;
	V.vy=V_t.vy + h * (V1.vy + 2*V2.vy + 2*V3.vy  + V4.vy)/6.;
	V.x=V_t.x + h * (V1.x + 2*V2.x + 2*V3.x  + V4.x)/6.;
	V.y=V_t.y + h * (V1.y + 2*V2.y + 2*V3.y  + V4.y)/6.;
	V.w=V_t.w;
	
	return V;
}

