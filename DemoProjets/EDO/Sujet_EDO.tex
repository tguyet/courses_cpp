\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{../../TP/exos}

\author{Thomas Guyet}
\title{EDO : Equation différentielles ordinaires}
\date{2011-2012}

\begin{document}

\maketitle

\section{Résolution numérique d'équations différentielles}

\subsection{Équation à une dimension}

Une équation différentielle ordinaire est de la forme~:
$$\frac{dy(t)}{dt}=f(t,y), \; y(0)=y_0$$

La méthode de résolution numérique d'une équation différentielle consiste à calculer, de proche en proche, les valeurs de $y(t)$.

La méthode d'Euler est la méthode la plus simple pour faire cela. Elle permet de calculer $y(t+h)$ à partir de $y(t)$ de la manière suivante~:
\begin{equation}
 y(t+h) = y(t) + h*f(t)
\label{equ:euler}
\end{equation}

où $h$ est une petite valeur qui désigne le pas de la méthode. Plus le pas sera petit, plus la résolution sera précise (mais plus elle sera longue à calculer).

En fait, par définition de la différentielle suivante~: $$\frac{dy(t)}{dt} = \lim_{h->0} \frac{y(t+h) - y(t)}{h},$$ si $h$ est petit on a $$\frac{dy(t)}{dt} = \frac{y(t+h) - y(t)}{h} + O(h^2)\approx \frac{y(t+h) - y(t)}{h}.$$ Soit $$f(t,y)\approx \frac{y(t+h) - y(t)}{h},$$ et donc on en déduit la formule de calcul de $y(t+h)$ ci-dessus (\ref{equ:euler}).

La méthode de Runge-Kutta d'ordre 2 (Equation (\ref{equ:rk2})) et d'ordre 4 (Equation (\ref{equ:rk4})) affinent l'approximation par des schémas de calcul réduisant l'erreur d'approximation commise à chaque pas~:

\begin{equation}
y(t+h) = y(t) + h f(t+h/2, y(t)) + O(h^3)
\label{equ:rk2}
\end{equation}
\begin{equation}
y(t+h) = y(t) + h/6 * (k1+2.k2+2.k3+k4)+ O(h^5)
\label{equ:rk4}
\end{equation}

avec $$k1 = h*f(t, y(t)),\; k2 = h*f(t+h/2, y(t) + k1),$$ $$k3 = h*f(t+h/2, y(t) + k2),\;k4 = h*f(t+h, y(t) + k3)$$


La Figure \ref{fig:Euler} illustre le fonctionnement des méthodes d'Euler et de Runge-Kutta d'ordre 2.
\begin{figure}[ht]
 \begin{center}
 \includegraphics[width=7cm]{euler.png}
 \includegraphics[width=7cm]{rungekutta2.png}
\end{center}
\caption{Illustration de la méthode d'Euler (à gauche) et de Runge-Kutta d'ordre 2 (à droite).}
\label{fig:Euler}
\end{figure}


\begin{exercice}[Étude de la divergence des méthodes]
On considère l'equation différentielle ordinaire~:
$$\dot{y}(t) = y(t)^2;\; y(0) = 1$$

\question{Résoudre cette équation numériquement avec la méthode d'Euler sur $[0, 1]$ (en fonction de $h$). Pour différentes valeurs de $h$, comparer à la solution exacte $y(t)=-\frac{1}{t-1}$ en calculant la valeur suivante~:}
$$\sum_{t=0}^{1}{ \left(\tilde{y}(t)+\frac{1}{t-1}\right)^2 } $$
\question{Résoudre cette équation numériquement avec la méthode RK2 et RK4 sur $[0, 1]$. Faire les mêmes comparaisons avec la solution exacte.}
\question{Tracer les courbes comparant les erreurs des différentes méthodes en fonction de $h$.}
\end{exercice}


\subsection{Système d'équations différentielles}

Dans le cas d'un problème différentiel à plusieurs variables, l'équation générale s'écrit alors ainsi :
$$\frac{d\vec{X}(t)}{dt}=\vec{f}(t,\vec{X}), \; X(0)=X_0$$
où $\vec{X}=\{x_1, x_2, ...,x_n\}$ est un vecteur, et $X_0$ est l'état initial du système.

Le problème s'exprime alors comme un système d'équations différentielles : 

$$\left\{ \begin{array}{l}
\frac{dx_1}{dt}= f_1(t,x_1, x_2, ...,x_n) \\
\frac{dx_2}{dt}= f_2(t,x_1, x_2, ...,x_n) \\
...\\
\frac{dx_n}{dt}= f_n(t,x_1, x_2, ...,x_n) \\
\end{array} \right.$$

Ce système se résout de la même manière que précédemment. Par exemple, avec le schéma d'Euler, on aura :

$$\left\{ \begin{array}{l}
x_1(t+h)= x_1(t) +h*f_1(t,x_1(t), x_2(t), ...,x_n(t)) \\
x_2(t+h)= x_2(t) +h*f_2(t,x_1(t), x_2(t), ...,x_n(t)) \\
...\\
x_n(t+h)= x_n(t) +h*f_n(t,x_1(t), x_2(t), ...,x_n(t)) \\
\end{array} \right.$$



\section{Trajectoire d'une balle de tennis}

Comment se comporte une balle de tennis en l'air~? La trajectoire est difficile à calculer car, il faut tenir compte de plusieurs forces. Cette trajectoire ne peut pas être obtenue analytiquement. Il faut donc passer par des approximations numériques.

\subsection{Modélisation du système}

On souhaite construire des trajectoires réalistes d'une balle de tennis au service du joueur que l'on entraine. Pour cela, on utilise un modèle réaliste en 2 dimensions tenant compte des forces suivantes :
\begin{itemize}
 \item le poids $\vec{G}=m.\vec{g}$ où $\vec{g}=(0,-g)$ est le vecteur de l'accélération due à la gravitation;
 \item la force de frottement $\vec{S}=-S \frac{\vec{v}}{\|\vec{v}\|}$, dont la direction est opposée à celle de $v$, le vecteur de vitesse de la balle;
 \item la force de Magnus $\vec{M}=M \frac{\vec{\omega} \times \vec{v}}{\|\vec{v}\|}$ cette force est perpendicuaire à $\vec{v}$ et $\vec{\omega}$, où $\vec{\omega}$ est le vecteur représentant la rotation de la balle.
\end{itemize}

Pour une balle de tennis, on peut négliger la décélération de la révolution de la balle, donc on considère que $\omega$ est constant. Dans la mesure où on travail en 2 dimension, on suppose que le vecteur $\vec{\omega}$ est perpendiculaire au plan d'étude.

Les grandeurs des forces de frottement $S$ et de Magnus $M$ sont des coefficients que l'on suppose être obtenus expérimentalement ($S$ dépend de la surface de section de la balle dans l'air, $M$ dépend de la texture de la balle et les deux dépendent de paramètres de l'air comme sa densité).


La trajectoire de la balle est alors définie par les équations de Newton pour le vecteur de position $\vec{r}(t)$ :

$$ m \frac{d^2\vec{r}(t)}{dt^2}=m.\vec{g}-S \frac{\vec{v}(t)}{\|\vec{v}(t)\|} + M \frac{\vec{\omega} \times \vec{v}(t)}{\|\vec{v}(t)\|}$$

avec les conditions initiales :
$$ \vec{v}(0) = \vec{v}_0, \vec{r}(0) = \vec{r}_0$$

En projetant sur les axes, on obtient les équations suivantes :

$$
\ddot{x} = -\frac{S}{m} \frac{\dot{x}}{v} + \frac{\omega.M}{m.v}\dot{y}
$$
$$
\ddot{y} =  -g -\frac{S}{m} \frac{\dot{y}}{v} - \frac{\omega.M}{m.v}\dot{x}
$$

où $v=\sqrt{\dot{x}^2+\dot{y}^2}$

En raison de la nature non linéraire du problème, il faut utiliser des méthodes numériques pour le résoudre. Il faut dans un premier temps le mettre sous une forme permettant de le résoudre numériquement par les méthodes usuelles, c'est-à-dire sous la forme :
$$\frac{d\vec{X}(t)}{dt}=\vec{f}(t,\vec{X})$$

Ici, le vecteur $\vec{X}$ aura cette allure :
$$\vec{X}=\left[ \begin{array}{c}
                  X_1 \\X_2 \\X_3\\X_4\\X_5
                 \end{array}\right]=
\left[ \begin{array}{c}
                  \dot{x} \\\dot{y} \\x\\y \\w
                 \end{array}\right]
$$

et le système différentiel pourra s'écrire ainsi :
$$\left\{ \begin{array}{c}
\frac{dX_1}{dt}= -\frac{S}{m.\sqrt{X_1^2+X_2^2}} X_1 + \frac{\omega.M}{m.\sqrt{X_1^2+X_2^2}}X_2 \\
\frac{dX_2}{dt}= -g -\frac{S}{m.\sqrt{X_1^2+X_2^2}} X_2 - \frac{\omega.M}{m.\sqrt{X_1^2+X_2^2}}X_1 \\
\frac{dX_3}{dt}= X_1\\
\frac{dX_4}{dt}= X_2\\
\frac{dX_5}{dt}= 0
\end{array} \right.$$

\subsection{Programme de résolution}

Maintenant, on passe (enfin) à la partie programmation en C ...

\question{Définir un nouveau type de données \lstinline!struct Vecteur! qui permette de représenter un vecteur d'état $\vec{X}$.}

\question{Écrire deux fonctions \lstinline!double f1(double, double, double)! et \lstinline!double f2(double, double, double)! qui réalisent les calculs suivants :}

$$f_1(vx, vy, \omega)= -\frac{S}{m.\sqrt{vx^2+vy^2}}vx + \frac{\omega.M}{m.\sqrt{vx^2+vy^2}}vy$$
$$f_2(vx,vy, \omega)= -g -\frac{S}{m.\sqrt{vx^2+vy^2}}vy - \frac{\omega.M}{m.\sqrt{vx^2+vy^2}}vx$$

\underline{Remarque :} Pour chaque constante du problème vous pourrez les définir comme des variables globales ou\footnote{Pour savoir si vous devez définir une variable globale ou une constante, vous devez savoir, par exemple, si un utilisateur pourrait avoir à modifier ces valeurs} des constantes du programme (à destination du préprocesseur). Voici la liste des valeurs pouvant être utilisées pour ces constantes (en unité du système internationnal)\footnote{Attention, \textit{a priori} ce n'est pas très réaliste ...} :
\begin{itemize}
 \item $g = 9.81$ : gravité,
 \item $x_0=0$, $y_0=2.7$ : position de départ de la balle au service,
 \item $\dot{x}_0=v\_init*cos(theta)$, $\dot{y}_0=v\_init*sin(theta)$ : vitesse de départ de la balle avec $v\_init=200$, $theta=-M\_PI/30$\footnote{$M\_PI$ est la valeur du $\pi$ disponible dans la librairie \texttt{math.h})},
 \item $w = 50$ : vitesse de rotation de la balle,
 \item $m = 0.058$ : poids d'une balle de tennis,
 \item $S = 0.002$ : constante de frottement,
 \item $M = 0.2$ : constante de Magnus (peut être négatif)
\end{itemize}

\question{Écrire une fonction \lstinline!Vecteur d_dt_euler(Vecteur V_t, double h)! qui retourne le vecteur au pas de temps $t+h$ à partir du vecteur au pas de temps $t$. $h$ est la valeur du pas de temps.}

\question{Écrire un programme qui implémente la méthode d'Euler et qui enregistre toutes les positions calculées dans un fichier \texttt{traj.dat}}

Le fichier devra comprendre uniquement deux colonnes (position en 2D de la balle) formatées ainsi ``\lstinline!%g %g!''.

\textbf{Vous arrêterez les calculs lorsque la ``balle touche le sol'' ou que le nombre de d'itération dépasse une valeur prédéfinie.}

\question{Tracé d'une trajectoire}
Pour tracer les trajectoires calculées, vous pouvez utiliser \texttt{gnuplot} sous linux, un tableur, Excel ou OOCalc, ou encore R. Avec \texttt{gnuplot}, on fournit un script \texttt{tennis.pl} dans les ressources du sujet. Pour se servir de ce script, il faut :
\begin{itemize}
 \item copier le script dans le répertoire contenant le fichier \texttt{traj.dat}
 \item depuis un terminal, lancer le script avec la commande suivante : \texttt{\$ gnuplot tennis.pl}
\end{itemize}
Le dessin qui s'affiche la courbe et figure les limites d'un terrain de tennis (le bord de l'affichage), et le filet.

\subsection{Améliorations du programme}

\subsubsection{Améliorations de la résolution numérique}

Si le programme est bien conçues, les modifications suivantes permettent de n'apporter que quelques modifications mineurs au programme en plus de la création de fonctions spécifiques.

\question{Refaire les calculs en ajoutant du vent (uniquement sur l'axe $x$)}

\question{Utiliser la méthode de Runge-Kutta d'ordre 2, puis d'ordre 4 pour faire le calcul des trajectoires}

%\question{Modifier le programme pour calculer les trajectoires en 3D (nécessite de projecter le système d'équations différentielles)}

\subsubsection{Service interactif}

\question{Construire un programme qui demande à l'utilisateur la vitesse initiale et l'angle de frappe et qui indique si le service est bon ou non ?}

\subsubsection{Estimer l'angle idéal (\hard)}

On veut maintenant se servir de la simulation pour aider le joueur qu'on entraine à améliorer son service.

\question{Construire un programme qui demande à l'utilisateur la vitesse initiale de frappe et qui estime l'angle de frappe idéal (\ie qui tombe sur la ligne du carré de service à $18.29m$ de la ligne de service)}


\end{document}
