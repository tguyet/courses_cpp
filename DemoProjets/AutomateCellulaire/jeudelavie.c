/*Jeu de la vie
Thomas Guyet, à partir du travail de Sophie Birot (2010)

Compilation :
g++ -o jdlv -lX11 interface.c jeudelavie.c
*/

#include "interface.h" // importation des fonctionnalités d'affichage

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h> // pour sleep()
#include <string.h> // pour memcpy()


#define MAT_SIZE 20 //< On définit la taille de la matrice (carrée)

int buffer[MAT_SIZE][MAT_SIZE]; //< Matrice annexe qui servira à faire l'application des règles !

/**
* @brief Fonction permettant de déterminer l'état de la cellule à l'itération suivante
* @param g matrice de l'état du monde
*/
void regle(int g[MAT_SIZE][MAT_SIZE])
{
	int i,j;

	//On parcourt toutes la matrice
	for (i=0;i<MAT_SIZE;i++) {
		for (j=0;j<MAT_SIZE;j++) {
			//Declaration d'une matrice [3][3] correspondant a une cellule et ses 8 voisins
			int motif[3][3];
			//On traite les bords (aux cellules situees sur la première colonne, les voisins de gauches correspondent  à la dernière colonne)
			if (j==0&&(i!=0||i!=MAT_SIZE-1))
			{
				motif[0][0]=g[i-1][MAT_SIZE-1];
				motif[0][1]=g[i-1][j];
				motif[0][2]=g[i-1][j+1];
				motif[1][0]=g[i][MAT_SIZE-1];
				motif[1][1]=g[i][j];
				motif[1][2]=g[i][j+1];
				motif[2][0]=g[i+1][MAT_SIZE-1];
				motif[2][1]=g[i+1][j];
				motif[2][2]=g[i+1][j+1];
			}
			//On traite les bords (aux cellules situees sur la dernière colonne, les voisins de gauches correspondent  à la première colonne)
			else if (j==MAT_SIZE-1&&(i!=0||i!=MAT_SIZE-1))
			{
				motif[0][0]=g[i-1][j-1];
				motif[0][1]=g[i-1][j];
				motif[0][2]=g[i-1][0];
				motif[1][0]=g[i][j-1];
				motif[1][1]=g[i][j];
				motif[1][2]=g[i][0];
				motif[2][0]=g[i+1][j-1];
				motif[2][1]=g[i+1][j];
				motif[2][2]=g[i+1][0];
			}
			//On traite les bords (aux cellules situees sur la première ligne, les voisins de gauches correspondent  à la dernière ligne)
			else if (i==0&&(j!=0||j!=MAT_SIZE-1))
			{
				motif[0][0]=g[MAT_SIZE-1][j-1];
				motif[0][1]=g[MAT_SIZE-1][j];
				motif[0][2]=g[MAT_SIZE-1][j+1];
				motif[1][0]=g[i][j-1];
				motif[1][1]=g[i][j];
				motif[1][2]=g[i][j+1];
				motif[2][0]=g[i+1][j-1];
				motif[2][1]=g[i+1][j];
				motif[2][2]=g[i+1][j+1];
			}
			//On traite les bords (aux cellules situees sur la dernière ligne, les voisins de gauches correspondent  à la première ligne)
			else if (i==MAT_SIZE-1&&(j!=0||j!=MAT_SIZE-1))
			{
				motif[0][0]=g[i-1][j-1];
				motif[0][1]=g[i-1][j];
				motif[0][2]=g[i-1][j+1];
				motif[1][0]=g[i][j-1];
				motif[1][1]=g[i][j];
				motif[1][2]=g[i][j+1];
				motif[2][0]=g[0][j-1];
				motif[2][1]=g[0][j];
				motif[2][2]=g[0][j+1];
			}
			//Pour chaque cellule de la matrice g, on fait correspondre dans la matrice motif la cellule et ses 8 voisins
			else
			{
				motif[0][0]=g[i-1][j-1];
				motif[0][1]=g[i-1][j];
				motif[0][2]=g[i-1][j+1];
				motif[1][0]=g[i][j-1];
				motif[1][1]=g[i][j];
				motif[1][2]=g[i][j+1];
				motif[2][0]=g[i+1][j-1];
				motif[2][1]=g[i+1][j];
				motif[2][2]=g[i+1][j+1];
			}


			//On calcule le nombre de voisins qu'à la cellule
			int sum=motif[0][0]+motif[0][1]+motif[0][2]+motif[1][0]+motif[1][2]+motif[2][0]+motif[2][1]+motif[2][2];

			if( (sum==3) && (motif[1][1]==0) ) {
					//Si la cellule est morte et a 3 voisines vivantes, elle devient alors vivante
					buffer[i][j]=1;
			} else if ( ((sum==2)||(sum==3)) && (motif[1][1]==1) ) {
					//Si la cellule est vivante et a 2 ou 3 voisines vivantes, elle reste vivante
					buffer[i][j]=1;
			} else {
				//dans les autres cas, la cellule meurt
				buffer[i][j]=0;
			}
		}
	}

	//Maintenant on applique la transformation à g : la fonction memcpy recopie efficacement tout un emplacement mémoire !
	memcpy(g,buffer,sizeof(int)*MAT_SIZE*MAT_SIZE);

	/*
	//Autre solution moins élégante :
	for (i=0;i<MAT_SIZE;i++) {
		for (j=0;j<MAT_SIZE;j++) {
			g[i][j]=buffer[i][j];
		}
	}//*/

}

//Debut du programme
int main (void)
{

	int g[MAT_SIZE][MAT_SIZE]; //Declaration de la matrice carre de taille MAT_SIZE
	int i,j;

	//Initialisation de la fenêtre d'affichage
	initWindow(600,600);

	//On initialise la matrice à 0
	for (i=0;i<MAT_SIZE;i++) {
		for(j=0;j<MAT_SIZE;j++) {
			g[i][j]=0;
		}
	}

	srand(time(NULL));
    // On remplit aléatoirement un carré centrale de la matrice
    for (i=MAT_SIZE/2-MAT_SIZE/4;i<MAT_SIZE/2+MAT_SIZE/4;i++) {
        for(j=MAT_SIZE/2-MAT_SIZE/4;j<MAT_SIZE/2+MAT_SIZE/4;j++) {
            //On remplie aléatoirement le carré central  avec des 0 et 1
            int rnum=rand()%2;
            g[i][j]=rnum;
        }
    }


	int t=0; //< t est la variable représentant le temps

	//On itère indéfiniement pour appliquer les règles de transformation
	// à la matrice g
	while (1)
	{
		//Transformation de g par l'application des règles
		regle(g);
		//Affichage de la matrice
		draw_matrice( (int *)&g, MAT_SIZE, MAT_SIZE, 2, 0);
		t++;
		//printf("%d\n",t);

		//Ajout d'un temps entre chaque affichage pour ralentir le processus
		//sleep(0.1);
	}

	return 0;
}
