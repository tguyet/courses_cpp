\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{../../../TP/exos}

\title{Projet C : automates cellulaires}
\author{Guyet T.}
\date{2011-2012}

\begin{document}

\maketitle

%Le présent exercice consiste à implémenter en C une version sur damier torique d'un automate cellulaire, en particulier avec les règles du jeu de la vie de John Conway.

\section{Présentation du jeu}

L'objectif de ce projet est d'implémenter le mécanisme d'application des règles d'un automate cellulaire. Ce type de modèle dynamique de l'espace est utilisé en écologie du paysage pour simuler des processus physiques spaciaux (progression d'un feu dans une forêt, colonisation par des insectes, transfert de gènes entre parcelles agricoles, ...)

Un automate cellulaire représente l'espace sous la forme d'une grille en 2 dimensions. Chaque case de la grille contient un entier (valeur définie dans un ensemble fini de valeurs, par exemple, dans le ``jeu de la vie'' seules deux valeurs sont possibles, mais on va ici généraliser). Chaque valeur correspond à un ``état'' de la case.

%La grille est remplie de valeurs parmis les valeurs acceptables (aucune case vide). En particulier, il faut un état initial à votre grille qui sera chargé à partir d'un fichier.

Un ensemble de règles permet de définir l'évolution des valeurs de la grille dans le temps. Dans un contexte de feu de forêt, on peut mettre des règles du genre ``si la cellule est de la forêt et qu'au moins deux cellules voisines sont en 
feu, alors la cellule devient feu''. On peut ainsi observer l'évolution d'un phénomène.

Globalement, le programme suit l'algorithme suivant~:

\begin{verbatim}
PROGRAMME:
  Initialiser la grille G;
  REPETER:
    Afficher la grille G;
    Compute(G);
    sleep(100);
\end{verbatim}

La fonction \texttt{Compute} sera vue par la suite, elle permet de transformer la grille G. la fonction \lstinline!sleep(100)! est une fonction C de la librairie \texttt{time.h}, qui permet de suspendre l'exécution du programme pendant 100ms le temps que l'utilisateur visualise le résultat de l'affichage.

\section{Représentation de la grille}

On pourra représenter la grille par un tableau d'entiers. En préambule du programme, vous définirez une constante correspondant à la taille de la grille.
Même si il est généralement conseillé d'éviter de déclarer des variables globales, vous définirez la grille en variable globale, \ie une variable qui sera visible par toutes les fonctions et qui évitera de la passer toujours en paramètre.



\begin{lstlisting}[numbers=none]
#define SIZE 30

int monde[SIZE][SIZE];
\end{lstlisting}

\question{Créer une fonction \lstinline!void init_alea(int val_max)! une fonction de génération aléatoire des valeurs de la grille. Les valeurs de la grille seront choisies entre 0 et \lstinline!val_max!.}

\question{Écrire le programme principal ci-dessous qui vous permettra de tester l'affichage avec les fonctions fournies (dans une fenêtre ou dans un terminal).}

\begin{verbatim}
G[20][20];

PROGRAMME:
  val_max=10;
  init_alea(val_max);
  REPETER:
    Afficher la grille G;
    init_alea(val_max);
    sleep(100);
\end{verbatim}

\subsection{Grille torique}
Représenter la grille de l'automate cellulaire par un tableau est la solution la plus simple. Mais il y reste le problème des bords pour l'application des règles. Comme on ne peut pas représenter un tableau infini, plusieurs choix sont alors possibles pour résoudre cette limite~:
\begin{itemize}
 \item considérer l'absence de voisin (au bord du tableau) comme une cellule dans un état particulier;
 \item jouer sur un damier torique.
\end{itemize}

La seconde solution, respecte mieux le jeu initial. On doit considérer que les bords droit et gauche du
tableau sont reliés entre eux, ainsi que les bords supérieurs et inférieurs. Le voisinage des points est donc
le suivant :

\includegraphics{GrilleTorique}

Ceci devra être pris en compte dans la suite.


\section{Évolution}

La principale difficulté de ce projet se trouve dans la fonction d'évolution de la grille. À côté de la grille se trouvent un ensemble de règles de transformation qui indique la valeur que doit prendre une case au temps $t+1$ en fonction des valeurs des cases voisines au temps $t$. Les cases voisines d'une case $(i,j)$ comprennent $(i,j)$ elle-même, les cases qui touches $(i,j)$, par exemple $(i+1, j)$, et éventuellement des cases plus lointaines $(i+2,j)$. On se restreindra aux cases immédiatement en contact d'une case à modifier.


Le principe de cette évolution est illustré par l'algorithme ci-dessous.

\begin{verbatim}
Compute(Grille G1):
  G2 = copie(G1);
  POUR CHAQUE CASE (i,j) de G2:
    nv <- regle(G2, [i,j]);
    G1[i,j] <- nv;
  detruire G2;
\end{verbatim}

La solution proposée ici consiste à utiliser une grille annexe, \texttt{G2}, qui représente la grille au temps $t$ pour modifier tranquillement la grille \texttt{G1} (passée en paramètre), qui représente la grille au temps $t+1$. 

La difficultée qu'il vous reste à traiter est celle de la fonction \lstinline!regle! qui consiste à implémenter une règle en fonction des cellules de \texttt{G2} autour de la position $[i,j]$ permettant de trouver la nouvelle valeur $nv$ de \texttt{G1}

\espace

Dans ce projet, on vous demande de réaliser deux jeux de règles (\cf sections suivantes), il est donc important de conserver l'idée d'avoir une fonction \lstinline!regle()! pour appliquer les modifications de la grille. Votre programme devra donc fonctionner indépendamment des transformation de cette fonction pour la modifier facilement.

\question{Créer une fonction \lstinline!regle()! qui retourne la valeur de la cellule $(i,j)$ de la grille en paramètre (\ie une règle qui ne fait rien).}
\question{Implémenter la fonction \lstinline!compute()!}
\question{Modifier votre programme précédant pour faire appliquer les transformations de la fonction \lstinline!compute()!}

\section{Le jeu de la vie}

\subsection{Historique}
John Conway était un mathématicien de l'Université de Cambridge. Très prolifique en matière de jeux
mathématiques, il décrivit en 1970 le jeu de la vie, visant à modéliser d'une façon simple l'évolution
d'organismes vivants. Le jeu de la vie est une application particulière d'automates cellulaires.

\subsection{Règles du jeu de la vie}
Le jeu de la vie évolue normalement sur un damier infini. Chaque case est occupée par une cellule qui peut
être vivante ou morte. À chaque génération, chaque cellule peut naître, mourir, ou rester dans son état. Les
règles qui permettent de passer d'une génération à l'autre sont précises et ont été choisies avec soin pour
que l'évolution des organismes soit intéressante et imprévisible. En premier lieu, notons que sur un damier
infini, chaque case a exactement 8 voisins. Les règles données par J. Conway sont les suivante :
\begin{itemize}
 \item Une cellule ayant exactement 2 ou 3 voisins vivants survit à la génération suivante.
\item Une cellule ayant au moins 4 cellules voisines vivantes meurt d'étouffement à la génération suivante.
\item Une cellule ayant au plus une cellule voisine vivante meurt d'isolement à la génération suivante.
\item Sur une case vide ayant exactement 3 voisins vivants, une cellule naîtra à la génération suivante.
\end{itemize}

Notons que c'est l'ensemble de la génération actuelle qui doit être pris en compte pour l'établissement de
l'état des cellules à la génération suivante.
Voici un exemple de figure sur un petit damier. Les cellules qui devront mourir à la génération suivante
sont grisées :

%\begin{figure}
 \includegraphics[width=\textwidth]{GrillesGeneration.png}
%\end{figure}

\question{Créer une fonction \lstinline!regle_conway()! qui implémente les règles du jeu de la vie.}
%\question{Créer une fonction \lstinline!regle_conway()! qui implémente les règles du jeu de la vie.}

\subsection{Intérêt du jeu de la vie}
Malgré son apparente simplicité, le jeu de la vie recèle de nombreuses surprises. En partant d'un tableau
aléatoire, on peut difficilement les observer même si l'apparition d'un planeur (voir l'exemple du début)
ou de formes périodiques (à la fin) ne sont pas rares.
Certaines personnes ont répertoriées des centaines des formes aux propriétés particulières :
\begin{itemize}
 \item générateurs de planeurs (les lanceurs),
 \item portes logiques,
 \item formes périodiques de grande période,
 \item formes stables...
\end{itemize}

La possibilité d'initialiser le damier avec une forme particulière est donc intéressante pour pouvoir observer
ces phénomènes. On pourra donc permettre à l'utilisateur de lire l'état des cases dans un fichier, afin qu'il
choisisse l'état initial du damier.

\section{Application aux feux de forêt}

Le modèle d'évolution du feu de forêt de Christophe Le Page, François Bousquet (Cirad) (\cf \url{http://cormas.cirad.fr/fr/applica/fireautomata.htm}) illustre les principes des automates cellulaires. Chaque cellule peut se trouver dans quatre états: \texttt{feu}; \texttt{arbre}; \texttt{cendre}; \texttt{vide}. Chaque cellule de la grille spatiale est initialement, selon une probabilité $p$, un arbre, et selon une probabilité $1-p$, vide. On met le feu à une cellule et on suit la diffusion du feu à travers la grille spatiale selon la fonction de transition suivante: une cellule prend feu au temps $t$ si l'une au moins de ses 4 voisines (nord, est, sud, ouest) est en feu au temps $t-1$. Les cellules en feu passeront en cendres au temps suivant, les cellules en cendres deviendront vides au temps suivant.

\question{Créer une fonction \lstinline!regle_feuforet()! qui implémente ce modèle et reproduire les expérimentations.}

\end{document}
