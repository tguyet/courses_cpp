/*
* Interface pour l'affichage graphique dans une fenêtre et la mise à jour (séquentielle) d'une
* matrice d'entier (avec ou sans chiffre, avec ou sans couleur)
* Guyet Thomas, AGROCAMPUS-OUEST, 2012
*/

#ifndef INTERFACE_H
#define INTERFACE_H

#include <stdio.h>

//Si vous êtes sous Linux : décommentez la ligne avec X11 pour avoir 
// des fenêtres, si vous êtes sous Windows, il faut décommenter la 
// ligne avec WINDOWS
#define X11
//#define WINDOWS

/**
* Fonction d'initialisation d'une fenêtre graphique : il est impératif d'initialiser la fenêtre avant de faire quelconque affichage.
* @param width largeur, en pixel, de la fenêtre
* @param height hauteur, en pixel, de la fenêtre
*/
void initWindow(int width, int height);

/**
* 
* Lors de l'appel de la fonction draw_matrice, la fonction dessine dans l'interface, puis arrête son traitement des évènements (non threadé)!
* Les couleurs utilisées sont celle de getColor(), sauf si la fonction getColor retourne 1, auquel cas des couleurs spécifiques à chacune des valeurs utilisées dans la matrice seront générées automatiquement. Dans ce dernier cas, les valeurs de la matrices sont supposées être entre 0 et matmax-1 ! Dans le cas d'une utilisation "normale" de la fonction getColor(), il n'y a pas de restriction.
*
* @param mat est un pointeur sur l'emplacement mémoire contenant les données à afficher (NB: bien qu'on affiche une matrice, on passe ici simplement un pointeur : ceci oblige d'avoir un emplacement contigüe d'une matrice !)
* @param width nombre de colonne à la matrice
* @param height nombre de ligne de la matrice
* @param matmax valeur maximale +1 des éléments de la matrice
* @param grid si grid est nul, on n'affiche pas la grille (sous Linux uniquement).
*/
void draw_matrice(int *mat, int width, int height, int matmax, int grid);

/**
* Fermeture (propre ?) de la fenêtre : peut provoquer des seg fault  si la fenêtre a déjà été supprimée par l'utilisateur, mais c'est pas grave !!
*/
void closeWindow();


/**
* Fonction utilisée pour définir l'association d'une couleur (sous la forme RGB entre 0 et 255)
* à une valeur de la matrice
* Cette fonction peut être modifiée à votre convenance !
* @param val valeur d'une case de la matrice à afficher
* @param pr, pg, pb (pointeurs) désigne les composantes de la couleur qu'il faut
* 		afficher en fonction de val.
* @return si la fonction retourne 0 alors la couleur est utilisée, sinon une génération
		automatique des couleurs sera utilisée !
*/
static int getColor(int val, int *pr, int *pg, int *pb) {
	*pr=255;
	*pb=255;
	*pg=255;
	switch(val) {
		case 0:
			*pr=255;
			*pb=0;
			*pg=0;
			break;
		case 1:
			*pr=0;
			*pb=0;
			*pg=255;
			break;
		case 2:
			*pr=40;
			*pb=230;
			*pg=45;
			break;
		case 3:
			*pr=23;
			*pb=23;
			*pg=155;
			break;
		default:
			printf("couleur non-définie pour %d\n", val);
	}
	return 0;
};

#endif

