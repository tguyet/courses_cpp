/*
Thomas Guyet (2012)
Programme de test pour l'affichage des matrices

Compilation sous Linux :
g++ -o test_interface -lX11 interface.c test_interface.c
*/

#include "interface.h" // importation des fonctionnalités d'affichage

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h> // pour sleep()

#define MAT_SIZE 20

int matrice[MAT_SIZE][MAT_SIZE];

int main (void)
{

	int i,j;

	//Initialisation de la fenêtre d'affichage
	initWindow(600,600);

	srand(time(NULL));

	//boucle infinie pour afficher des matrices à l'infinie !
	while (1)
	{
		//On remplie aléatoirement le carré central avec des valeurs entre 0 et 3
		for (i=MAT_SIZE/2-MAT_SIZE/4;i<MAT_SIZE/2+MAT_SIZE/4;i++) {
		    for(j=MAT_SIZE/2-MAT_SIZE/4;j<MAT_SIZE/2+MAT_SIZE/4;j++) {
		        int rnum=rand()%4;
		        matrice[i][j]=rnum;
		    }
		}
		//Affichage de la matrice (notez le passage de la matrice en paramètre) !
		draw_matrice( (int *)&matrice, MAT_SIZE, MAT_SIZE, /*matmax*/ 3, /*grid*/0);
	}

	return 0;
}
