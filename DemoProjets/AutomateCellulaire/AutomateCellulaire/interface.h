

#ifndef H_INTERFACE
#define H_INTERFACE

#include "cv.h"
#include "highgui.h"


void draw(int **, int, int);
void draw2(int *, int , int);

void initWindows(int w, int h);
void closeWindows();

int get_windows_width();
int get_windows_height();


#endif
