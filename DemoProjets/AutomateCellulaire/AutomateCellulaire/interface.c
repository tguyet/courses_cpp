
#include "interface.h" //inclue récursivement les autres fichiers inclus dans interface.h


//Le mot clé "static" indique que cette variable n'est visible que dans ce fichier !
static int window_width, window_height;
static IplImage* frame;

/**
* \param w,h taille de la fenêtre en nombre de pixels
*/
void initWindows(int w, int h) {
    window_width=w;
    window_height=h;
    //Image noir et blanc (1) codée sur 8 bits (IPL_DEPTH_8U) de la bonne taille
    frame=cvCreateImage(cvSize(w,h),IPL_DEPTH_8U,1);

    //Construction d'une fenêtre d'affichage
    cvNamedWindow( "AutoCell", 1 );
}

void closeWindows() {
    /* free memory */
    cvDestroyWindow( "AutoCell" );
}

int get_windows_width() {
    return window_width;
}

int get_windows_height() {
    return window_height;
}

/**
* \param mat est une matrice de valeurs entre 0 et 255. Attention, je mets ici un double pointeur, mais il faut en fait un tableau à deux dimensions contigue !!
* \param w, h taille de la matrice
* Si w et h sont plus grand que les dimensions de la matrice, alors l'affichage sera coupé ! Sinon il y aura un suréchantillonnage qui permettra à l'affichage d'étirer l'affichage
*/
void draw(int **mat, int w, int h) {
    float pasx, pasy;
    pasx = (double)window_width/(double)w;
    pasy = (double)window_height/(double)h;
    if (pasx<1) pasx=1;
    if (pasy<1) pasy=1;

    int i,j; //positions dans l'image
    int x=0, y=0; //positions dans la matrice
    int cumulx=0, cumuly=0;
    for(j=0; j<window_height; j++) {
        cumulx=0;
        x=0;
        for(i=0; i<window_width; i++) {
            CvScalar value = cvScalar((double)(mat[x][y]),0,0,0);
			cvSet2D(frame, j, i, value);

			cumulx++;
			if( (double)cumulx > pasx ) {
			    cumulx=0;
			    x++;
			    if( x==w ) x=0;
			}
        }
        cumuly++;
        if( (double)cumuly > pasy ) {
            cumuly=0;
            y++;
            if(y==h) break;
        }
    }

    //Déclenche l'affichage de l'image
    cvShowImage( "AutoCell", frame );
}

/**

* \param mat est une matrice de valeurs entre 0 et 255. Attention, je mets ici un double pointeur, mais il faut en fait un tableau à deux dimensions contigue !! C'est un pointeur sur l'emplacement mémoire alloué sous la forme int[][] !!
* \param w, h taille de la matrice
* Si w et h sont plus grand que les dimensions de la matrice, alors l'affichage sera coupé ! Sinon il y aura un suréchantillonnage qui permettra à l'affichage d'étirer l'affichage
*/
void draw2(int *mat, int w, int h) {
    float pasx, pasy;
    pasx = (double)window_width/(double)w;
    pasy = (double)window_height/(double)h;
    if (pasx<1) pasx=1;
    if (pasy<1) pasy=1;

    int i,j; //positions dans l'image
    int x=0, y=0; //positions dans la matrice
    int cumulx=0, cumuly=0;
    for(j=0; j<window_height; j++) {
        cumulx=0;
        x=0;
        for(i=0; i<window_width; i++) {
            CvScalar value = cvScalar((double)(mat[x+y*w]),0,0,0);
			cvSet2D(frame, j, i, value);

			cumulx++;
			if( (double)cumulx > pasx ) {
			    cumulx=0;
			    x++;
			    if( x==w ) x=0;
			}
        }
        cumuly++;
        if( (double)cumuly > pasy ) {
            cumuly=0;
            y++;
            if(y==h) break;
        }
    }

    //Déclenche l'affichage de l'image
    cvShowImage( "AutoCell", frame );
}

