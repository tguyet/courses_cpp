
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include "interface.h"

void drawAlea(int **img, int w, int h ) {
    int x,y;
	for(y = 0; y<h; y++ ) {
		for(x = 0; x<w; x++ ) {
			img[x][y]=(rand()%255);
		}
	}
}

int** create_matrix(int size_x, int size_y)
{
    int** matrix;
    int i, j;
    matrix = calloc(size_x, sizeof(int*));
    for( i= 0;i<size_x;i++) {
        matrix[i] = calloc(size_y, sizeof(int));
    }
    for( i = 0;i<size_x;i++) {
        for( j = 0;j<size_y;j++) {
                matrix[i][j] = 0;
        }
    }
    return matrix;
}


int main() {
    initWindows(640,480);
    int **mat= create_matrix(10,20);
    int key=0;

	srand( time(NULL) );

    while( key != 'q' && key!=1048689 ) {/* quit if user press 'q' */
        drawAlea( mat, 10, 20 );

        //lance l'affichage de la matrice
        draw(mat, 10, 20);

        // un peu d'attente pour laisser le temps à l'ordinateur de récupérer une saisie clavier
        key = cvWaitKey( 2000 );
    }

    closeWindows();
    return 0;
}

