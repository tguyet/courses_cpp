/*
* Code pour l'affichage graphique dans une fenêtre et la mise à jour (séquentielle) d'une
* matrice d'entier (avec ou sans chiffre, avec ou sans couleur)
* Guyet Thomas, AGROCAMPUS-OUEST, 2012
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "interface.h"

#define SPACE 0 //< Espaces (verticaux et horizontaux) entre deux rectangles

#ifdef X11
#include <X11/Xlib.h>
//>Quelques variables globales pour la gestion de la fenêtre
static Display *display;
static Window win;
static GC gc;
static XGCValues gcv;
static Pixmap db;
static XWindowAttributes window_attributes;
static int depth;

#elif defined(WINDOWS)

#include <windows.h>
#include <stdio.h>

int *shared_mat; //pointeur sur la matrice à dessiner !
int shared_width, shared_height, shared_matmax;
HWND Hw; //Handler sur la fenêtre

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

int Register(HINSTANCE HIn) {
    WNDCLASSEX Wc;

    Wc.cbSize=sizeof(WNDCLASSEX);
    Wc.style=0;
    Wc.lpfnWndProc=WndProc; //pointeur de fonction : définie la fonction à appeler pour traiter les évènements!!
    Wc.cbClsExtra=0;
    Wc.cbWndExtra=0;
    Wc.hInstance=HIn;
    Wc.hIcon=LoadIcon(NULL,IDI_APPLICATION);
    Wc.hCursor=LoadCursor(NULL,IDC_ARROW);
    Wc.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
    Wc.lpszMenuName=NULL;
    Wc.lpszClassName="MyWin";
    Wc.hIconSm=LoadIcon(NULL,IDI_APPLICATION);

    return RegisterClassEx(&Wc);
};

#endif


int force_redraw()
{
#ifdef X11
	XEvent e;
	while(XCheckWindowEvent(display, win, ExposureMask, &e))
	{
		XCopyArea(display, db, win, gc, 0, 0, window_attributes.width, window_attributes.height, 0, 0);
        /* on force le rafraichissement de l'écran */
        XFlush(display);
	}
#elif defined(WINDOWS)
    //*
    MSG Msg;
    //PeekMessage(&Msg,NULL,0,0,PM_NOREMOVE);
    //do {
        while(PeekMessage(&Msg,NULL,0,0,PM_REMOVE)) {
            TranslateMessage(&Msg);
            DispatchMessage(&Msg);
        }
    //}while(Msg.message);
    /*if( Msg.message!=WM_QUIT ) {
        return 0;
    }*/
#endif
    return 1;
}

void closeWindow() {
#ifdef X11
	if(!display) {
		fprintf(stderr, "Display not initialized\n");
		return;
	}
	XDestroyWindow( display , win );
	XCloseDisplay( display );
#endif
}

void initWindow(int width, int height)
{
#ifdef X11
	XEvent e;

	display=NULL;

	char *display_name = NULL;
	if ((display=XOpenDisplay(display_name)) == NULL) {
		fprintf(stderr,"initWindow: cannot connect to X server %s\n", XDisplayName(display_name));
		exit(-1);
	}

	win = XCreateSimpleWindow(display, RootWindow(display,DefaultScreen(display)), SPACE, SPACE, width, height, 2, BlackPixel(display,DefaultScreen(display)), WhitePixel(display,DefaultScreen(display)));

	gcv.foreground = BlackPixel(display,DefaultScreen(display));
	gc= XCreateGC(display,win, GCForeground, &gcv);

	XSelectInput(display, win, ExposureMask);
	XMapWindow(display, win);

	//Sequence d'instruction pour le bon fonctionnement de l'utilisation des colormap !
	Colormap cmap;
	XColor near_color, true_color;
	cmap = DefaultColormap( display, 0 );
	XAllocNamedColor( display, cmap, "black", &near_color, &true_color );

	do{
		XNextEvent( display, &e);
	} while( e.type != Expose );

	//initialisation de la pixmap qui sert de buffer
	XGetWindowAttributes(display, win, &window_attributes);
	depth = DefaultDepth(display,DefaultScreen(display));
	db = XCreatePixmap(display, win, window_attributes.width, window_attributes.height, depth);
#elif defined(WINDOWS)
    HMODULE HIn=GetModuleHandle(NULL);
    if(!Register(HIn)) return;

    Hw=CreateWindowEx(WS_EX_TOPMOST,"MyWin","",WS_OVERLAPPEDWINDOW, 0, 0, width, height,  GetDesktopWindow(), NULL, HIn, NULL);
    if(Hw==NULL) exit(1);

    ShowWindow(Hw,SW_SHOW);
    UpdateWindow(Hw);
    SetFocus(Hw);

    shared_mat=0;
#else
	printf("sans fenetrage X11 ou Windows : affichage dans un terminal\n");
#endif
}

/**
* Fonction pour la transformation des coordonnées HSV en RGB
*/
static void HsvToRgb (float h, float s, float v, int *r, int *g, int *b)
{
	int i;
	float aa, bb, cc, f;
	float fr, fg, fb;
	if (s == 0) /* Niveau de gris */
		fr = fg = fb = v;
	else {
		if (h == 1.0) h = 0;
		h *= 6.0;
		i = (int)h;
		f = h - i;
		aa = v * (1 - s);
		bb = v * (1 - (s * f));
		cc = v * (1 - (s * (1 - f)));
		switch (i) {
			case 0: fr = v; fg = cc; fb = aa; break;
			case 1: fr = bb; fg = v; fb = aa; break;
			case 2: fr = aa; fg = v; fb = cc; break;
			case 3: fr = aa; fg = bb; fb = v; break;
			case 4: fr = cc; fg = aa; fb = v; break;
			case 5: fr = v; fg = aa; fb = bb; break;
		}
	}

	*r = (int)(255*fr);
	*g = (int)(255*fg);
	*b = (int)(255*fb);
}

#ifdef X11
/**
* Fonction pour récupérer le code RBG de XColor à partir d'un nom de couleur
*/
static void set_color_str( Display* dis, char* fg_color_name )
{
	Colormap cmap;
	XColor near_color, true_color;
	cmap = DefaultColormap( dis, 0 );
	XAllocNamedColor( dis, cmap, fg_color_name, &near_color, &true_color );
	XSetForeground( dis, gc, BlackPixel(dis, DefaultScreen(dis))^near_color.pixel);
}

/**
* Fonction pour récupérer le code RBG de XColor des valeurs RGB
*/
static void set_color( Display* dis, int fg_r, int fg_g, int fg_b )
{
	char fg_str[30];
	sprintf(fg_str, "rgb:%02x/%02x/%02x", fg_r, fg_g, fg_b);

	set_color_str(dis, fg_str);
}

void draw_matrice(int *mat, int width, int height, int matmax, int grid)
{
	int i, j, r, g, b;
	if(!display) {
		fprintf(stderr, "Display not initialized\n");
		return;
	}

	if(!mat) {
		fprintf(stderr, "Null matrix\n");
		return;
	}

	//On remplit le buffer en noir
	set_color(display, 0,0,0);
	XFillRectangle(display, db, gc, 0, 0, window_attributes.width, window_attributes.height);

	//Taille élémentaire d'un rectangle
	int rect_width = (int)( (double)(window_attributes.width-SPACE*(width+1)) / (double) ( width ) );
	int rect_height = (int)( (double)(window_attributes.height-SPACE*(height+1)) / (double) ( height ) );

	for( i=0; i< width; i++) {
		int x= SPACE + (SPACE +rect_width) * i;
		for( j=0; j< height; j++) {
			int y= SPACE + (SPACE +rect_height) * j;

			if( getColor(mat[i*width+j], &r,&g,&b) ) {
				float h=(float)mat[i*width+j]/(float)matmax;
				HsvToRgb(h,1.0,1.0,&r,&g,&b);
			}


			set_color(display, r,g,b);
			XFillRectangle(display, db, gc, x, y, rect_width, rect_height);

			if( grid ) {
				set_color(display, 0,0,0);
				XDrawRectangle(display, db, gc, x, y, rect_width, rect_height);
			}
		}
	}

	//Force l'affichage de la fenêtre à partir du pixmap directement !
	force_redraw();
}

#elif defined(WINDOWS)

LRESULT CALLBACK WndProc(HWND hwnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    printf("wndproc\n");
    //HDC hdc;
    int i, j, r, g, b;
    PAINTSTRUCT ps;   // paint data for Begin/EndPaint

    if(!shared_mat) {
		fprintf(stderr, "Null matrix\n");
		return 1;
	}

	RECT windowRect;
    GetWindowRect(hwnd, &windowRect);

    //To access members
    int ww=windowRect.right-windowRect.left;
    int wh=windowRect.bottom-windowRect.top;

    //Taille élémentaire d'un rectangle
	int rect_width = (int)( (double)(ww-SPACE*(shared_width+1)) / (double) ( shared_width ) );
	int rect_height = (int)( (double)(wh-SPACE*(shared_height+1)) / (double) ( shared_height ) );

    printf("paint %d %d %d %d %d %d\n", shared_width, shared_height, (int)shared_mat, shared_mat[10*shared_width+10], ww, wh);
    switch(Msg) {
        case WM_PAINT:
            BeginPaint(hwnd, &ps);
            for( i=0; i< shared_width; i++) {
                int x= SPACE + (SPACE +rect_width) * i;
                for( j=0; j< shared_height; j++) {
                    int y= SPACE + (SPACE +rect_height) * j;

                    if( getColor(shared_mat[i*shared_width+j], &r,&g,&b) ) {
                        float h=(float)shared_mat[i*shared_width+j]/(float)shared_matmax;
                        HsvToRgb(h,1.0,1.0,&r,&g,&b);
                    }
                    HBRUSH brush=CreateSolidBrush(RGB(r,g,b));
                    SelectObject(ps.hdc,brush);
                    Rectangle(ps.hdc, x, y, x+rect_width, y+rect_height);
                    DeleteObject(brush);
                }
            }
            EndPaint(hwnd, &ps);
            break;
        case WM_DESTROY:
            PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
            break;
        default:
            ;//return DefWindowProc(hwnd,Msg,wParam,lParam);
    }
    return 0L;
}

void draw_matrice(int *mat, int width, int height, int matmax, int grid)
{
    //printf("draw\n");
    //On passe aux variables locales
    shared_mat=mat;
    shared_width=width;
    shared_height=height;
    shared_matmax=matmax;

    if(!mat) return;

    RECT windowRect;
    GetWindowRect(Hw, &windowRect);
    InvalidateRect(Hw, &windowRect, 1);
    printf("%d\n",UpdateWindow(Hw));
    //On dessine
	force_redraw();
	Sleep(500);
}
#else

void draw_matrice(int *mat, int width, int height, int matmax, int grid)
{
	int i,j;
	for( i=0; i< width; i++) {
		for( j=0; j< height; j++) {
			printf("%d ", mat[i*width+j]);
		}
		printf("\n");
	}
}

#endif


