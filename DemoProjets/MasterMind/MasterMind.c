/*
 ============================================================================
 Name        : MasterMind.c
 Author      : Thomas Guyet, AGROCAMPUS-OUEST, 2011
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//>definition d'une macro pour le calcul d'un minimum
#define min(a,b) (a<b?a:b)

#define NB_TENTATIVES 12
#define LINESIZE 6
#define COLORMAX 6
char colors[COLORMAX]={'r', 'g', 'b', 'y', 'm', 'w'};

//Une ligne est un ensemble d'entiers (pas de vérification de la limite de valeur) !
typedef int line[LINESIZE];

typedef struct {
	int nbexact;
	int nbcolor;
} result;

void print(line l) {
	int i=0;
	printf("|");
	while(i<LINESIZE) {
		if( l[i]>=COLORMAX || l[i]<0 ) {
			fprintf(stderr, "impossible d'afficher la valeur %d", l[i]);
			continue;
		}
		printf("%c ", colors[l[i]]);
		i++;
	}
	printf("|\n");
}

void printresult(result r) {
	printf("%d couleurs valides et %d couleurs bien placées\n", r.nbcolor, r.nbexact);
}

/**
 * \param p proposition
 * \param m modele à découvrir
 */
result cmp(line p, line m) {
	int i,j;
	result r={0,0};

	//Recherche des exacts !
	for(i=0; i<LINESIZE; i++) {
		if( p[i]==m[i] ) {
			r.nbexact++;
		}
	}

	//Recherche des nombres de couleurs exactes

	//On remplit le tableau suivant :
	int nbcolors_p[COLORMAX];
	int nbcolors_m[COLORMAX];
	for(j=0; j<COLORMAX; j++) {
		nbcolors_p[j]=0;
		nbcolors_m[j]=0;
	}

	for(i=0; i<LINESIZE; i++) {
		nbcolors_p[ p[i] ]++;
	}
	for(i=0; i<LINESIZE; i++) {
		nbcolors_m[ m[i] ]++;
	}
	for(j=0; j<COLORMAX; j++) {
		r.nbcolor+=min( nbcolors_m[j], nbcolors_p[j] );
	}

	r.nbcolor -= r.nbexact;

	return r;
}


void scanline(line l) {
	char c;
	int i,j,found;

	//On analyse la saisie
	while( 1 ) {
		i=0;
		while(i<LINESIZE) {
			found=0;
			//On fait une saisie d'un caractère
			scanf("%c", &c);

			//On saute les espaces
			if( c==' ' ) continue;

			if( c=='\n' && i!=LINESIZE-1) {
				printf("Rentrer %d valeurs valides\n", LINESIZE);
				break;//sort du for(i...) uniquement !!
			}

			//Maintenant , on vérifie que les lettres qui ont été tapées font bien partir des couleurs.
			for(j=0; j<COLORMAX; j++) {
				if(c==colors[j]) {
					found=1;
					//On remplit la case, et on passe à la case suivante
					l[i]=j;
					i++;
					break; // sort du for(j...)
				}
			}
			if( !found ){
				printf("La couleur %c n'existe pas\n", c);
				break; //sort du for(i...) uniquement !!
			}
		}
		if( found ) {
			break;
		}

		if( i>=LINESIZE) {
			printf("Vous avez entré plus de %d valeurs, seules les premières ont été prises en compte\n", LINESIZE);
		}
	}
}

void generate_line(line l) {
	int i;
	srand( time(NULL) );
	for(i=0; i<LINESIZE; i++) {
		l[i]=rand()%COLORMAX;
	}
}


int main(void) {
	int i=0;
	line m, l;
	result r;
	generate_line(m);
	printf("a trouver:\n");
	print(m);

	while (i<NB_TENTATIVES) {
		printf("Il vous reste %d tentatives ... entrez une proposition\n", NB_TENTATIVES-i);
		scanline(l);
		printf("j'ai compris ceci :");
		print(l);
		printf("Resultat :\n");
		r=cmp(l,m);
		if( r.nbexact==LINESIZE ) {
			break;
		}
		printresult(r);
		i++;
	}
	if( r.nbexact==LINESIZE ) {
		printf("Bien joué\n");

	}
	return EXIT_SUCCESS;
}
