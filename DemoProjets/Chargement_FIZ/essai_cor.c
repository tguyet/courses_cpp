//PROJET C: Dataset exportation from Fizz software and preparation data treatment//
//Dataset collected from Sorted Napping Task//
//Authors: BAUDRY Perrine, GRUEL Adeline, MARTIN C�line//
//Date: 24/04/2011//

//libraries//

#include<stdio.h>
#include <string.h>
#include <stdlib.h>

//Programm//

//*********************************************************************//

//SKIP function//
//FONCTION: This fonction is designed to remove columns of the dataset, and return the modified dataset//
//PARAMETERS: Char s stands for the dataset, char c stands for the separation symbol and intSkipped stands for the number of columns to erase//

char* skip(char* s, char c, int nSkipped)
{
    int nSeparator = 0;
    int nChar = 0;
    int length = strlen(s);         //fonction strlength : get string length (library string)
    while(nChar < length && nSeparator < nSkipped)  //Protection from out of bounds errors
    {
        if (s[nChar]==c){
            nSeparator ++;
        }
        nChar++;
    }
    return &s[nChar];
}
//**********************************************************************//

// GETNEXT fonction//
//FONCTION: This fonction is designed to return an integer corresponding to the length of the index//
//PARAMETERS: Char s stands for the data, index stands for the first value, and c is the separator//

int getNext(char* s, int index, char c)
{
    int nChar = index;
    int length = strlen(s);
    while(nChar < length && s[nChar]!=c)    //While a separator is not reached, the iteration continues//
    {
        nChar++;
    }
    return (nChar<length)?nChar:length;
}
//**********************************************************************//


//GETLINE fonction//
//FONCTION: This fonction is designed to get a line in a file//
//PARAMETERS: line is a place allocated in memory, FILE stands for the file to apply the fonction//


char* getLine(char* line, FILE* fin)
{
    memset(line, 0, 5000);      //memset fills a block of the memory
    int index = 0;
    char c = 0;
    while (c != '\n' && !feof(fin) && !ferror(fin) && index < 5000)
    {
        fscanf(fin, "%c", &c);      //Reading of the file
        line[index]=c;
        index++;                    //incrementation of the variable index
    }
    line[index]=0;
    return line;
}


//**************************************************************************************//
//**************************************************************************************//

// Principal programm
int main()
{
    //Creation and importation of the files used in the programm
    FILE*fin,*fin2,*fout;
    fin  = fopen ("Entries.csv","r");
    fin2 = fopen ("Products.csv","r");
    fout = fopen ("Results.csv","w+");

    printf("Data extracted from file: Entries.csv. Importation of Products.csv. Creation of the file: Results.csv");

    //initialization of the variables
    int prod = 0;

    int i = 0;

    //Allocation of the memory
    char* line = malloc(5000*sizeof(char)) ;
    char* prev = malloc(5000*sizeof(char)) ;

    //Creation of 500 empty lines
    char** tab = (char**)malloc(500*sizeof(char*));
    tab[0] = (char*)malloc(500*sizeof(char)*5000);
    char *p=tab[0];
	for(i=0; i<500; i++, p+=5000) {
		tab[i]=p;
		memset(tab[i],0, sizeof(char)*5000);
	}

    // suppression of the first line
    getLine(line, fin2);            //Call of the fonction get line

	for(i=0; i<500 && !feof(fin2); i++) {
        line = getLine(line, fin2);
        if (strlen(line)==0) break;
        line[strlen(line)-1]=0;
        sprintf(tab[i], "%s;", line);   //Write into the array
	}

    prod = i;


    getLine(line, fin);         //Call of the fontion getline

    int judge = 0;
    while( !feof(fin) && !ferror(fin) )
    {
        line = getLine(line, fin);              //Get the line for the Judge i


        // suppression of the 7 first values
        char* s = skip(line, ';', 7);           //Call of the skip fonction
        int begin = 0;                          //Initialization ot the variables
        int end = 0;

        //Loop on the number of products//

        for (i = 0; i < prod*2; i++)            //Prod*2 because of 2 coordinates per product
        {
            end = getNext(s, begin, ';');       //Call of the Getnext fonction
            if (begin < end && s[end-1]=='\n')  //preparation of the dataset
            {
                s[end-1]=0;
            }
            else
                s[end]=0;
            strcpy(prev, tab[i/2%prod]);        //strcopy copy the data from the tab in prev
            sprintf(tab[i/2%prod],"%s%s;", prev, &s[begin]);// Save the data n the pointed array
            s[end]=';';                         //Format of the end (separator)
            begin = end+1;
        }

        char* t = skip(&s[begin], ';', prod);
        begin = end = 0;                        //Initialization of the variables
        for (i = 0; i < prod; i++)              //Preparation of the dataset
        {
            end = getNext(t, begin, ';');
            if (begin < end && t[end-1]=='\n')
            {
                t[end-1]=0;
            }
            else
                t[end]=0;
            strcpy(prev, tab[i]);
            sprintf(tab[i],"%s%s;", prev, &t[begin]);
            t[end]=';';
            begin = end+1;
        }
        judge++;                               //Incrementation of the variable Judge return at the beginning of the loop
    }

    fprintf(fout, "Products;");             //Creation of the labels for the file
    for (i=1;i<=judge;i++)
    {
        fprintf(fout, "J_x%d;J_y%d;Gr%d;", i, i, i); //Incrementation of the judges numbers for the labels
    }
    fprintf(fout, "\n");

    for(i=0;i<prod;i++)                     //Exportation of the data in the file out
    {
        fprintf(fout, "%s\n", tab[i]);
    }

    if (line)
        free(line);
    free(prev);
	free(tab[0]);
	
    if (tab)
        free(tab);
        
    fclose (fin);

    return 0;
}


