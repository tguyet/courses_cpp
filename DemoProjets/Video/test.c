#include <stdio.h>
#include <stdlib.h>
#include <highgui.hpp>
#include <cv.h>
#include <math.h>

IplImage *image;


int main(int argc, char *argv[])
{
	int i, acqu = 0, touche, ref = 0;
	int b=0;

	//---------------------------------------------------------------------
	// Test avec une image
	//---------------------------------------------------------------------

	// Ouverture de l'image
	image = cvLoadImage("aviron-france_jo2008.jpg", 1);

	cvNamedWindow("Image", CV_WINDOW_AUTOSIZE);

	cvShowImage("Image", image);


	//---------------------------------------------------------------------
	// Utilisation d'une caméra branché sur un port USB (type webcam)
	//---------------------------------------------------------------------

	cvcamStart();

	CvCapture *capture = 0;
	IplImage *frame;
	IplImage *framevideo;

	capture = cvCaptureFromCAM( -1 );

	cvNamedWindow("video", CV_WINDOW_AUTOSIZE);
	for(;;)
	{
	b = cvWaitKey(10);
	if(b=='c')
	{
	break;
	}
	framevideo = cvQueryFrame( capture );
	cvShowImage("video", framevideo);
	}

	cvWaitKey(0);
	cvcamStop();
	//cvcamExit();


	system("PAUSE");
	return 0;
}

