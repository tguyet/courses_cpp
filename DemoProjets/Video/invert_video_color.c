/**
 * Programme d'inversion de couleurs
 * Compilation gcc -O3 -o invert `pkg-config --cflags --libs opencv` invert_video_color.c 
 * \author T. Guyet, AGROCAMPUS OUEST
 * \date 2012
 */

//Mettre ces trois ligne pour pouvoir utiliser la librairie OpenCV
#include <stdio.h>
#include "cv.h"
#include "highgui.h"


/**
* Fonction pour la transformation d'une image de la vidéo
*/
void invert( IplImage *img )
{
	int x, y;
	CvSize size = cvGetSize(img);	//Récupère la taille de l'image
	CvScalar pixel;						// un CvScalar est un pixel de l'image

	//Parcours de l'image, pixel par pixel :
	for(y = 0; y < size.height; y++ ) {
		for(x = 0; x < size.width; x++ ) {
			//Pour chaque pixel, à la position (x,y) :
			//On récupère la valeur de ce pixel :
			pixel = cvGet2D(img, y, x);

			//pixel.val est un tableau de 4 valeurs BGRA
			pixel.val[0] = 255-pixel.val[0]; //Transformation de la composante bleu
			pixel.val[1] = 255-pixel.val[1]; //Transformation de la composante verte
			pixel.val[2] = 255-pixel.val[2]; //Transformation de la composante rouge
			
			//On modifie la valeur du pixel (x,y) en lui attribuant le pixel transformé :
			cvSet2D(img, y, x, pixel);
			
		}
	}
}
 
int main( int argc, char** argv )
{
    CvCapture *webcam; 	// déclaration d'un pointeur vers une variable qui représente la webcam
    IplImage  *frame;	// frame correspond à une image de la vidéo
    int key=0;			// variable qui va contenir la valeur d'une touche appuyée par l'utilisateur
 
    // Initialisation de la caméra
    webcam = cvCaptureFromCAM( 0 );
 
    // Création d'une fenêtre pour afficher la vidéo. Le premier parametre sert à identifier le nom de la fenêtre, le 1 est un paramètre qui spécifie le comportement de la fenêtre.
    cvNamedWindow( "video originale", 1 );
	// Seconde fenêtre
	cvNamedWindow( "video transformee", 1 );

    while( key != 'q' && key!=1048689 ) {
        // La fonction cvQueryFrame() permet de récupérer une image à partir de la webcam
    	frame = cvQueryFrame( webcam );

		// Affiche l'image frame dans la fenêtre (ici, frame est l'image originale)
		cvShowImage( "video originale", frame );
 
		// Appel de la fonction invert qui transforme l'image frame
        invert( frame );

		// Affiche l'image frame dans la fenêtre (ici, frame est l'image transformee)
		cvShowImage( "video transformee", frame );
 
        // capture "à la volée" une touche au clavier ... (sans attente)
        key = cvWaitKey( 10 );
    }
 
    //Libération de la mémoire
    cvReleaseCapture( &webcam	 );
    cvDestroyWindow( "video" ); 
    return 0;
}
