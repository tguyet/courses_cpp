/**
 * Programme d'inversion de couleurs
 * Compilation gcc -O3 -o correction `pkg-config --cflags --libs opencv` correction.c 
 * \author T. Guyet, AGROCAMPUS OUEST
 * \date 2012
 */

//Mettre ces trois ligne pour pouvoir utiliser la librairie OpenCV
#include <stdio.h>
#include "cv.h"
#include "highgui.h"

#define SIZEH 20

/**
* Fonction pour la transformation d'une image de la vidéo
*/
IplImage *binarisation( IplImage *img, float histo[SIZEH][SIZEH][SIZEH], float seuil)
{
	int x, y, r, m;
	CvSize size = cvGetSize(img);	//Récupère la taille de l'image
	CvScalar pixel;					// un CvScalar est un pixel de l'image
	CvScalar pixel_blanc;
	pixel_blanc.val[0]=255;
	pixel_blanc.val[1]=255;
	pixel_blanc.val[2]=255;
	pixel_blanc.val[3]=255;
	CvScalar pixel_noir;
	pixel_noir.val[0]=0;
	pixel_noir.val[1]=0;
	pixel_noir.val[2]=0;
	pixel_noir.val[3]=255;

	IplImage* nI = cvCreateImage(size, IPL_DEPTH_8U, 4);

	//Parcours de l'image, pixel par pixel :
	for(y = 0; y < size.height; y++ ) {
		for(x = 0; x < size.width; x++ ) {
			//Pour chaque pixel, à la position (x,y) :
			//On récupère la valeur de ce pixel :
			pixel = cvGet2D(img, y, x);

			float p = histo[(int)((SIZEH*pixel.val[0])/256)][(int)((SIZEH*pixel.val[1])/256)][(int)((SIZEH*pixel.val[2])/256)];

			//On modifie la valeur du pixel (x,y) en lui attribuant le pixel transformé :
			if( p>seuil ) {
				cvSet2D(nI, y, x, pixel_blanc);
			} else {
				cvSet2D(nI, y, x, pixel_noir);
			}
		}
	}
	return nI;
}

void fusion( IplImage *img_modified, IplImage *img_fond, IplImage *img_masque ) {
	CvScalar pixel;
	CvSize size = cvGetSize(img_modified);
	int x,y;
	for(y = 0; y < size.height; y++ ) {
		for(x = 0; x < size.width; x++ ) {
			pixel = cvGet2D(img_masque, y, x);
			if( pixel.val[0]==255 && pixel.val[1]==255 && pixel.val[2]==255 ) {
				cvSet2D(img_modified, y, x, cvGet2D(img_fond, y, x));
			}
		}
	}	
}


void modifier_histo(IplImage *img, int x, int y, int w, int h, float histo[SIZEH][SIZEH][SIZEH], int *nb) {
	CvScalar pixel;
	int i,j;

	for(j = y; j < y+h; j++ ) {
		for(i = x; i < x+w; i++ ) {
			pixel = cvGet2D(img, j, i);
			histo[(int)((SIZEH*pixel.val[0])/256)][(int)((SIZEH*pixel.val[1])/256)][(int)((SIZEH*pixel.val[2])/256)] ++;
		}
	}
	*nb+=h*w;
}

void construct_histo(float histo[SIZEH][SIZEH][SIZEH]) {
	CvCapture *webcam; 
    IplImage  *frame;
    int key=0;
	int nb=0;
	int i,j,k;

	//initialisation de l'histogramme
	for(i=0; i< SIZEH; i++) {
		for(j=0; j<SIZEH; j++) {
			for(k=0; k<SIZEH; k++) {
				histo[i][j][k]=0;
			}
		}
	}
 
    // Initialisation de la caméra
    webcam = cvCaptureFromCAM( 0 );
 
    // Création d'une fenêtre pour afficher la vidéo. Le premier parametre sert à identifier le nom de la fenêtre, le 1 est un paramètre qui spécifie le comportement de la fenêtre.
    cvNamedWindow( "capture", 1 );


    while( key != 'q' && key!=1048689 ) {
        // La fonction cvQueryFrame() permet de récupérer une image à partir de la webcam
    	frame = cvQueryFrame( webcam );
		CvSize size = cvGetSize(frame);

		if( key=='g' || key==1048679 ) {
			printf("get image\n");
			modifier_histo(frame, 3*size.width/8, 3*size.height/8, size.width/4, size.height/4, histo, &nb);
		}

		// Affiche l'image frame dans la fenêtre (ici, frame est l'image transformee)
		cvRectangle( frame, cvPoint( 3*size.width/8, 3*size.height/8 ), cvPoint( 5*size.width/8, 5*size.height/8 ), CV_RGB( 255, 0, 0 ), 1, 8, 0 );
		cvShowImage( "capture", frame );
 
        // capture "à la volée" une touche au clavier ... (sans attente)
        key = cvWaitKey( 10 );
    }
 
    //Libération de la mémoire
    cvReleaseCapture( &webcam);
	cvDestroyWindow( "capture" ); 

	//normalisation
	for(i=0; i< SIZEH; i++) {
		for(j=0; j<SIZEH; j++) {
			for(k=0; k<SIZEH; k++) {
				histo[i][j][k]/=(float)nb;
			}
		}
	}
}

 
int main( int argc, char** argv )
{
    CvCapture *webcam; 	// déclaration d'un pointeur vers une variable qui représente la webcam
    IplImage  *frame, *frameT=0;	// frame correspond à une image de la vidéo
    int key=0;			// variable qui va contenir la valeur d'une touche appuyée par l'utilisateur
	float seuil=0.1;

	float histo[SIZEH][SIZEH][SIZEH];
	construct_histo(histo);



	IplImage* img_fond=0; 
  	img_fond=cvLoadImage("fond.png", CV_LOAD_IMAGE_COLOR);
  	if(!img_fond) {
		printf("Could not load image file\n");
		exit(1);
	}
 
    // Initialisation de la caméra
    webcam = cvCaptureFromCAM( 0 );
 
    // Création d'une fenêtre pour afficher la vidéo. Le premier parametre sert à identifier le nom de la fenêtre, le 1 est un paramètre qui spécifie le comportement de la fenêtre.
    cvNamedWindow( "video originale", 1 );
	// Seconde fenêtre
	cvNamedWindow( "video transformee", 1 );

    while( key != 'q' && key!=1048689 ) {
        // La fonction cvQueryFrame() permet de récupérer une image à partir de la webcam
    	frame = cvQueryFrame( webcam );

		if( key=='p' || key==1048688) {
			seuil*=2;
			printf("seuil : %f\n", seuil);
		} else if( key=='m' || key==1048685) {
			seuil/=2;
			printf("seuil : %f\n", seuil);
		}

		// Affiche l'image frame dans la fenêtre (ici, frame est l'image originale)
		cvShowImage( "video originale", frame );
 
		// Appel de la fonction invert qui transforme l'image frame
		if(frameT) cvReleaseImage(&frameT);
        frameT =binarisation( frame, histo, seuil );

		fusion(frame, img_fond, frameT);

		// Affiche l'image frame dans la fenêtre (ici, frame est l'image transformee)
		cvShowImage( "video transformee", frame );
 
        // capture "à la volée" une touche au clavier ... (sans attente)
        key = cvWaitKey( 10 );
    }
 
    //Libération de la mémoire
    cvReleaseCapture( &webcam	 );
    cvDestroyWindow( "video originale" ); 
	cvDestroyWindow( "video transformee" ); 
    return 0;
}
