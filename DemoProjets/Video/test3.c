/**
 * Display video from webcam
 * Compilation gcc -O3 -o test3 `pkg-config --cflags --libs opencv` test3.c 
 */
#include <stdio.h>
#include "cv.h"
#include "highgui.h"

void detectOrange( IplImage *img );
IplImage *base;
long histo[256][256][256];
long histo_size;

void constructHisto(CvCapture *);
 
int main( int argc, char** argv )
{
    CvCapture *capture;
    IplImage  *frame;
    int       key;
 
    /* initialize camera */
    capture = cvCaptureFromCAM( 0 );
 
    /* always check */
    assert( capture );
 
    /* create a window */
    cvNamedWindow( "video", 1 );

	constructHisto(capture);
 
    base = cvQueryFrame( capture );
    while( key != 'q'&& key!=1048689 ) {
        /* get a frame */
        frame = cvQueryFrame( capture );
 
        /* always check */
        if( !frame ) break;
 
        /* detect faces and display video */
        detectOrange( frame );
 
        /* quit if user press 'q' */
        key = cvWaitKey( 10 );
        //printf("%d\n", key);
        if(key=='g' || key==1048679) {
        	printf("get image\n");
			//IplImage  *old;
        	base=0;
        	while( !base ) {
        		base = cvCloneImage( cvQueryFrame(capture) );
        	}
			//Libération de old ??
			//cvReleaseImage(&old);
        }
    }
 
    /* free memory */
    cvReleaseCapture( &capture );
    cvDestroyWindow( "video" ); 
    return 0;
}


void constructHisto(CvCapture *capture)
{
	int key;
	IplImage  *frame;
	int x,y, i,j,k;

	//initialisation de l'histogramme
	histo_size=0;
	for(i=0; i<256; i++)
		for(j=0; j<256; j++)
			for(k=0; k<256; k++)
				histo[i][j][k]=0;

	while( key != 'q'&& key!=1048689 ) {
        /* get a frame */
        frame = cvQueryFrame( capture );
 
        /* always check */
        if( !frame ) break;

		//On récupère la taille de l'image
		CvSize size = cvGetSize(frame);

		if(key=='g' || key==1048679) {
        	printf("get image\n");
        	for(y = 3*size.height/8; y < 5*size.height/8; y++ ) {
				for(x = 3*size.width/8; x < 5*size.width/8; x++ ) {
					CvScalar s = cvGet2D(frame, y, x);
					histo[ (int)s.val[0]][ (int)s.val[1]][ (int)s.val[2]]++;
					//printf("  B=%f, G=%f, R=%f\n",s.val[0],s.val[1],s.val[2]);
				}
        	}
			histo_size+=2*size.height/8*2*size.width/8;
			printf("Histo size : %ld\n",histo_size);
		}

        cvRectangle( frame, cvPoint( 3*size.width/8, 3*size.height/8 ), cvPoint( 5*size.width/8, 5*size.height/8 ), CV_RGB( 255, 0, 0 ), 1, 8, 0 );
		cvShowImage( "video", frame );

		key = cvWaitKey( 10 );
    }
	
	//Affichage des éléments de l'histogramme
	/*
	for(i=0; i<256; i++)
		for(j=0; j<256; j++)
			for(k=0; k<256; k++)
				if( histo[i][j][k]>0 )
					printf("%d %d %d %ld\n", i, j ,k, histo[i][j][k]);
	*/

	printf("\n**Construct histogram finished**\n");
}


void detectOrange( IplImage *img )
{
	int x, y;
	CvSize size = cvGetSize(img);
	CvScalar s, s2;
//	float d,r;
	long d;

	float threshold = (float)(1*histo_size)/(float)(size.height*size.width/16);

	for(y = 0; y < size.height; y++ ) {
		for(x = 0; x < size.width; x++ ) {
			s = cvGet2D(img, y, x);

			//On récupère la valeur de l'histogramme correspondant au pixel
			d= histo[ (int)s.val[0]][ (int)s.val[1]][ (int)s.val[2]];			
			
			if( d>threshold ) {
				/* ajout de bleu
				s2.val[0]=255.0;
				s2.val[1]=0;
				s2.val[2]=0;
				/*/
				// ajout d'une image de fond
				s2 = cvGet2D(base, y, x);
				//*/
				cvSet2D(img, y, x, s2);
			}
			
		}
	}
    cvShowImage( "video", img );
}


