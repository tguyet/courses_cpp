/**
 * Programme d'inversion de couleurs
 * Compilation gcc -O3 -o correction `pkg-config --cflags --libs opencv` correction.c 
 * \author T. Guyet, AGROCAMPUS OUEST
 * \date 2012
 */

//Mettre ces trois ligne pour pouvoir utiliser la librairie OpenCV
#include <stdio.h>
#include "cv.h"
#include "highgui.h"


/**
* Fonction pour la transformation d'une image de la vidéo
*/
IplImage *binarisation( IplImage *img, CvScalar model)
{
	int x, y, r, m;
	CvSize size = cvGetSize(img);	//Récupère la taille de l'image
	CvScalar pixel;					// un CvScalar est un pixel de l'image
	CvScalar pixel_blanc;
	pixel_blanc.val[0]=255;
	pixel_blanc.val[1]=255;
	pixel_blanc.val[2]=255;
	pixel_blanc.val[3]=255;
	CvScalar pixel_noir;
	pixel_noir.val[0]=0;
	pixel_noir.val[1]=0;
	pixel_noir.val[2]=0;
	pixel_noir.val[3]=255;

	IplImage* nI = cvCreateImage(size, IPL_DEPTH_8U, 4);

	//Parcours de l'image, pixel par pixel :
	for(y = 0; y < size.height; y++ ) {
		for(x = 0; x < size.width; x++ ) {
			//Pour chaque pixel, à la position (x,y) :
			//On récupère la valeur de ce pixel :
			pixel = cvGet2D(img, y, x);

			m=0;
			r=pixel.val[0]- model.val[0];
			m+=r*r;
			r=pixel.val[1]- model.val[1];
			m+=r*r;
			r=pixel.val[2]- model.val[2];
			m+=r*r;

			//On modifie la valeur du pixel (x,y) en lui attribuant le pixel transformé :
			if( m< 20000 ) {
				cvSet2D(nI, y, x, pixel_blanc);
			} else {
				cvSet2D(nI, y, x, pixel_noir);
			}
		}
	}
	return nI;
}

void fusion( IplImage *img_modified, IplImage *img_fond, IplImage *img_masque ) {
	CvScalar pixel;
	CvSize size = cvGetSize(img_modified);
	int x,y;
	for(y = 0; y < size.height; y++ ) {
		for(x = 0; x < size.width; x++ ) {
			pixel = cvGet2D(img_masque, y, x);
			if( pixel.val[0]==255 && pixel.val[1]==255 && pixel.val[2]==255 ) {
				cvSet2D(img_modified, y, x, cvGet2D(img_fond, y, x));
			}
		}
	}	
}
 
int main( int argc, char** argv )
{
    CvCapture *webcam; 	// déclaration d'un pointeur vers une variable qui représente la webcam
    IplImage  *frame, *frameT=0;	// frame correspond à une image de la vidéo
    int key=0;			// variable qui va contenir la valeur d'une touche appuyée par l'utilisateur

	IplImage* img_fond=0; 
  	img_fond=cvLoadImage("fond.png", CV_LOAD_IMAGE_COLOR);
  	if(!img_fond) {
		printf("Could not load image file\n");
		exit(1);
	}

	CvScalar model;
	model.val[0]=230;
	model.val[1]=10;
	model.val[2]=10;	
 
    // Initialisation de la caméra
    webcam = cvCaptureFromCAM( 0 );
 
    // Création d'une fenêtre pour afficher la vidéo. Le premier parametre sert à identifier le nom de la fenêtre, le 1 est un paramètre qui spécifie le comportement de la fenêtre.
    cvNamedWindow( "video originale", 1 );
	// Seconde fenêtre
	cvNamedWindow( "video transformee", 1 );

    while( key != 'q' && key!=1048689 ) {
        // La fonction cvQueryFrame() permet de récupérer une image à partir de la webcam
    	frame = cvQueryFrame( webcam );

		// Affiche l'image frame dans la fenêtre (ici, frame est l'image originale)
		cvShowImage( "video originale", frame );
 
		// Appel de la fonction invert qui transforme l'image frame
		if(frameT) cvReleaseImage(&frameT);
        frameT =binarisation( frame, model );

		fusion(frame, img_fond, frameT);

		// Affiche l'image frame dans la fenêtre (ici, frame est l'image transformee)
		cvShowImage( "video transformee", frame );
 
        // capture "à la volée" une touche au clavier ... (sans attente)
        key = cvWaitKey( 10 );
    }
 
    //Libération de la mémoire
    cvReleaseCapture( &webcam	 );
    cvDestroyWindow( "video" ); 
    return 0;
}
