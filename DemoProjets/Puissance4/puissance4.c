/**********************************************************************
* Puissance 4 interactif avec "intelligence artificielle"
*
* Auteurs principales : E. Ruelle, C. Paillette
* Améliorations : T. Guyet
* AGROCAMPUS-OUEST, 2010
**********************************************************************/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define SIZE 7 	// fixe la taille du tableau de jeu
#define VICT 4 	// fixe le nombre de pions a aligner pour gagner
#define IA 3 		// définition le nombre de coup d'avance que l'IA considere

#define DEBUG 0	// mode debug ou non ! (le mode debug affiche des informations sur ce qui se passe)


static float strategie;	// Configuration de l'IA (>0), si <1 : strategie aggressive (favorise l'attaque), si >1 favorise la défense
static const float victory_weight=1500;

/**
* @brief affichage le plateau de jeu
* @param tab : représentation du plateau à afficher
*/
void affichetab(int tab[SIZE][SIZE])
{
	int k,j=0;
	for (k=0; k<SIZE; k++) {
		printf(" ");
        for (j=0; j<SIZE; j++) {
			printf("%d | ", tab[k][j]);
        }
        printf("\n");
        for (j=1; j<SIZE; j++)
        {
			printf("___|");
        }
        printf("___|\n");     
	}
	printf("\n");
}

/**
* @brief fonction qui verifie si l'insertion du pion "joueur" à la position (ligne,colonne) permet de gagner la partie. La fonction vérifie l'alignement de VICT pions. Si print est !=0, alors un message affiche la victoire sur la sortie standard.
* @param tab : plateau courant
* @param ligne, colonne : dernière insertion dont on cherche
* @param joueur : identité du pion
* @param print : si !=0, alors des messges de victoire sont affichés, rien sinon !
* @return 1 si l'insertion du pion est victorieuse, et 0 sinon
*/
int victoire(int tab[SIZE][SIZE], int ligne, int colonne, int joueur, int print)
{
	int k, cpt, pos;

	//verification de VICT en ligne 
	cpt=1;
	pos=colonne+1;
	//direction E
	while( pos<SIZE && tab[ligne][pos]==joueur ) { 
		cpt++;
		pos++;
	}
	//direction O
	pos=colonne-1;
	while( pos<SIZE && tab[ligne][pos]==joueur ) { 
		cpt++;
		pos--;
	}
	if (cpt>=VICT) {
		if(print) printf("Bravo joueur %d belle ligne !\n",joueur);
		return 1;
	}

	//verification de VICT pions en colonne : ici, on se contente de descendre puisque l'ajout a nécessairement été fait par le dessus !
	cpt=1;
	pos=ligne+1;
	//direction S
	while( pos<SIZE && tab[pos][colonne]==joueur ) { 
		cpt++;
		pos++;
	}
	if (cpt>=VICT) {
		if(print) printf("Bravo joueur %d belle colonne !\n",joueur);
		return 1;
	}

	//Comptage de la premiere diagonale
	cpt=1;
	k=1;
	//recherche de pions sur la diagonale, de la position de jeu en direction SE
	while(colonne+k<SIZE && ligne+k<SIZE && tab[ligne+k][colonne+k]==joueur ) { 
		cpt++;
		k++;
	}
	k=1;
	//recherche de pions sur la diagonale, de la position de jeu en direction NO
	while (colonne-k>-1 && ligne-k>-1 && tab[ligne-k][colonne-k]==joueur) { 
		cpt++;
		k++;
	}
	if (cpt>VICT-1) {
		if(print) printf("Bravo joueur %d belle diago!!!!\n",joueur);
		return 1;
	} 

	//Comptage de la seconde diagonale
	cpt=1;
	k=1;
	//direction NE
	while( colonne+k<SIZE && ligne-k>-1 && tab[ligne-k][colonne+k]==joueur ) { 
		cpt++;
		k++;
	}
	k=1;
	//direction SO
	while( colonne-k>-1 && ligne+k<SIZE && tab[ligne+k][colonne-k]==joueur ) {
		cpt++;
		k++;
	}

	if (cpt>VICT-1) {
		if(print) printf("Bravo joueur %d belle diago!!!!\n",joueur);
		return 1;
	} 

	return 0;
}

/**
* @fn int intart(int tab[SIZE][SIZE], int ligne, int colonne, int cpt) {
* @brief Fonction d'exploration recursive
* @param tab : plateau courant
* @param cpt : niveau de récursion
*/
float intart(int tab[SIZE][SIZE], int cpt)
{
	int i, k, x, z;
	float prob=0;

	for (i=0; i<SIZE; i++) {
		//l'ordinateur regarde ce que peux faire l'humain 
		//pour chaque colonne selon ce qu'il envisage

		k=SIZE-1;
		while( k>-1 && tab[k][i]!=0 ) k--;
		if( tab[k][i]!=0 ) continue; //Pas de place dans la colonne i, on passe a la suivante
		//ICI, k est la premiere ligne de la colonne i disponible

		tab[k][i]=1;
		
		if( victoire(tab, k, i, 1, 0) ) {
			tab[k][i]=0;
			return -(IA+2-cpt); //On pénalise ce chemin
		}

		if (cpt<IA) { //on fait jouer l'ordinateur pour un coup virtuel
			for (x=0; x<SIZE; x++) {
				z=SIZE-1;
				while( z>-1 && tab[z][x]!=0 ) z--;
				if( tab[z][x]!=0 ) continue; //Pas de place dans la colonne x, on passe au x suivant !

				//ICI, z est la premiere case libre de la colonne x

				tab[z][x]=2;
				if( !victoire(tab, z, x, 2, 0) ) {
					prob +=intart(tab, cpt+1);
				} else {
					prob += (IA+2-cpt);
				}
				tab[z][x]=0; // On libere la case
			}
		}
		tab[k][i]=0; // On libere la case
	}
	return prob;
}

/**
* @brief Tour de jeu par l'ordinateur
* @param tab : plateau de jeu courant
* @return 1 si gagné, et 0 sinon
*/
int jeux_PC(int tab[SIZE][SIZE])
{
	int k, i;
	int joue[SIZE];
	int joueur=1;

	for (k=0; k<SIZE; k++) joue[k]=0;

	int cpt2=0;
	float sup=- victory_weight*10;
	float prob;

	for (k=0; k<SIZE; k++) { //On teste toutes les colonnes
		prob=0;
		if (tab[0][k]==0) {
			i=SIZE-1;
			while( tab[i][k]!=0 ) i--; // Recherche de la premiere case disponible dans la colonne k (on sait que ca s'arrete)
			// ICI, i est la premiere ligne de la colonne k disponible !
			
			tab[i][k]=joueur;
			prob=victoire(tab, i, k, joueur, 0)*victory_weight;
			if( prob==0 ) {
				//Si on ne gagne pas immédiatement, on explore les autres possibilités de gagner ...
				prob+=intart(tab, 1);
			}
			tab[i][k]=0;
		}
		
		if( prob<0 ) prob = - prob * strategie;
		#if DEBUG > 0
		printf("IA prob %lf\n", prob); //Affichage des proba calculées
		#endif
		
		if( prob>sup ) {
			joue[0]=k+1;
			int w;
			for (w=1;w<SIZE;w++) {
				joue[w]=0;
			}
			sup=prob;
			cpt2=1;
		}else if (prob==sup) {
			joue[cpt2]=k+1;
			cpt2++;
		}
	}
	
	// Affichage des options coups possibles
	#if DEBUG > 0
	printf("Joue :\n");
	for(k=0; k<SIZE; k++) printf("%d,",joue[k]);
	printf("\n");
	#endif

	//L'ordinateur joue aleatoirement parmis les jeux non-nuls
	int pioup=0;
	while (pioup==0) {
		int r=rand()%SIZE;
		pioup=joue[r];	
	}
	pioup=pioup-1;

	i=SIZE-1;
	while(i>-1 && tab[i][pioup]!=0) i--;
	if (tab[i][pioup]==0) {
		tab[i][pioup]=joueur;
	} else {
		// Colonne pioup pleine (peu probable, impossible ?)
		printf("erreur : pas de place pour jouer !\n");
		exit(1);
	}
	return victoire(tab,i,pioup,joueur,1);
}


/**
* @brief Tour de jeu par l'utilisateur
* @param tab : plateau de jeu courant
* @return 1 si gagné, et 0 sinon
*/
int jeux_User(int tab[SIZE][SIZE])
{
	int place;
	int V=0;
	int joueur=2;
	while(1)//si on a joue dans un emplacement non autorise on rejoue
	{
		printf("Joueur %d, selectionnez la colonne ou vous voulez jouer (1a%d): ", joueur, SIZE);
		if( !scanf("%d",&place) ) { //on demande au joueur dans quelle colonne il veut jouer
			printf("Tant pis pour toi, passe ton tour !!\n");
			break;
		}

		int k=SIZE-1;
		if (place>0 && place<SIZE+1 && tab[0][place-1]==0) { //on verifie que le joueur a joue dans le tableau et dans une colonne qui n'est pas deja pleine
			while(tab[k][place-1]!=0) k--; //On sait que ca s'arrete d'apres la ligne precedente !
			tab[k][place-1]=joueur;
			V=victoire(tab, k, place-1, joueur, 1);
			return V;
		}
	}
	return 0;
}


/**
* @brief main()
*/
int main(int argc, char *argv[])
{
	int tab[SIZE][SIZE];
	int k;
	int j, *pa;
	pa=(int*)tab;
	//creation du tableau rempli de 0
	for (k=0; k<SIZE; k++) {
		for (j=0; j<SIZE; j++) {
			*pa=0;
			pa++;            
		}
	}
	//Initialisation du generateur aleatoire !
	srand( time(NULL) );
	affichetab(tab);
	
	strategie = 1.5;

	//choix de "couleur"
	char choix;
	int joueur;
	printf("Voulez-vous commencer ? (O ou N) ");
	scanf("%c", &choix);
	if (choix=='O') {
		joueur=1;
	} else {
		joueur=2;
	}

	int V=0;
	int cptG=1;
	while (V==0 && cptG<SIZE*SIZE+1) {//tant que personne n'a gagne et que le jeu n'est pas plein
		if (joueur==2) {
			V=jeux_PC(tab);
		} else {
			V=jeux_User(tab);
		}
		affichetab(tab);
		if(joueur==1) {//on change de joueur a la fin du tour
			joueur=2;
		} else {
			joueur=1;
		}
		cptG++;
		 
		if (cptG==SIZE*SIZE) {
			printf("personne n'a gagne \n");
		} 
	}
	return 0;
}

