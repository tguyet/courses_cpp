/**
* Résolution du sudoku par 
*	- backtracking récursif !
* 	- backtracking itératif (avec matrice de protection) !
* 
* Thomas Guyet, AGROCAMPUS-OUEST, 2011
*/

#define EMPTY 0					//< Valeur par défault pour une case vide
#define NOT_EMPTY 1				//< Valeur par défault pour une case non-vide (matrice d'occupation pour l'iteratif)
#define DEFAULT_SUDOKU_SIZE 9			//< Taille d'un sudoku
#define DEBUG_LEVEL 0				//< Niveau d'affichage pour debugger le programme

/**
* Définition du type pour une grille de sudoku
*/
typedef int sudoku[DEFAULT_SUDOKU_SIZE][DEFAULT_SUDOKU_SIZE];



/**
* Fonction de chargement d'une grille de sudoku à partir d'un fichier
* @param filename nom du fichier à télécharger
* @return 0 si tout c'est bien passé, sinon erreur
*/
int load_sudoku(sudoku s, const char *filename);

/**
* Fonction d'affichage d'une grille de sudoku
* @param s grille de sudoku
*/
void print(sudoku s);



/************ Fonctions de résolution ************/

/**
* Fonction de lancement la résolution récursive d'une grille de sudoku.
* @param s grille de sudoku
*/
void back_tracking(sudoku s);

/**
* Fonction iteratif la résolution d'une grille de sudoku
* @param s grille de sudoku
* @return Retourne 1 si le sudoku a une solution, et 0 sinon
*/
int back_tracking_ite(sudoku s);



/************ Fonctions utiles ************/

/**
* Fonction d'évaluation d'une case protégée
* La fonction vérifie les erreurs de position ... ceci permet d'éviter des accès en dehors des matrices
* @param i,j position évaluée
* @return La valeur de la case (EMPTY si case vide) si les coordonnées sont correcte et -1 sinon
*/
int val_case(sudoku s, uint i, uint j);

/**
* Fonction de modification d'une valeur de la grille. Si la valeur est incorrecte, la valeur insérée est EMPTY.
* La fonction vérifie les erreurs de position
* @param i,j position à modifier
* @param val valeur à insérer
* @param s grille de sudoku
*/
void put_case(sudoku s, uint i, uint j, uint val);

