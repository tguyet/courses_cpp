/**
* Résolution du sudoku par 
*	- backtracking récursif !
* 	- backtracking itératif (avec matrice de protection) !
* 
* Thomas Guyet, AGROCAMPUS-OUEST, 2011
*/

#include <stdio.h>
#include <stdlib.h>
#include "sudoku_btrec_nomalloc.h"


int main(int argc, char **argv)
{
	if( argc <2 ) {
		printf("Donner le fichier d'une sudoku à résoudre (%s nom_ficher)\n",argv[0]);
		return 0;
	}

	sudoku s;

	//Chargement d'un sudoku
	if( !load_sudoku(s, argv[1]) ) {
		printf("Erreur de chargement du fichier %s.\n",argv[1]);
		return 1;
	}
	printf("Grille d'entrée :\n");
	print(s);

	//Résolution par backtracking recursif
	back_tracking(s);
	printf("Résultat (backtracking recursif):\n");
	print(s);
	
	//Résolution par backtracking iteratif
	back_tracking_ite(s);
	printf("Résultat (backtracking iteratif):\n");
	print(s);

	return 0;
}
