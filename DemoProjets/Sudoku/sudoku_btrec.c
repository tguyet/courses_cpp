/**
* Résolution du sudoku par 
*	- backtracking récursif !
* 	- backtracking itératif (avec matrice de protection) !
* 
* Thomas Guyet, AGROCAMPUS-OUEST, 2011
*/

#include <stdio.h>
#include <stdlib.h>

#define EMPTY 0					//< Valeur par défault pour une case vide
#define NOT_EMPTY 1				//< Valeur par défault pour une case non-vide (matrice d'occupation pour l'iteratif)
#define DEFAULT_SUDOKU_SIZE 9	//< Taille d'un sudoku
#define DEBUG_LEVEL 0			//< Niveau d'affichage pour debugger le programme

/**
* Définition du type pour une grille de sudoku
*/
typedef int** sudoku;

sudoku new_sudoku();
void free_sudoku(sudoku);
sudoku load_sudoku(const char *);
int val_case(sudoku s, uint i, uint j);
void put_case(sudoku s, uint i, uint j, uint val);
void print(sudoku s);
void back_tracking(sudoku s);
int back_tracking_ite(sudoku s);

int main(int argc, char **argv)
{
	if( argc <2 ) {
		printf("Donner le fichier d'une sudoku à résoudre (%s nom_ficher)\n",argv[0]);
		return 0;
	}

	//Chargement d'un sudoku
	sudoku s=load_sudoku(argv[1]);
	if( !s ) return 1;
	printf("Grille d'entrée :\n");
	print(s);

	//Résolution par backtracking recursif
	back_tracking(s);
	printf("Résultat (backtracking recursif):\n");
	print(s);
	
	//Résolution par backtracking iteratif
	back_tracking_ite(s);
	printf("Résultat (backtracking iteratif):\n");
	print(s);

	free_sudoku(s);
	return 0;
}

/**
* Fonction de test si il existe val sur la ligne i (sauf pour la colonne j)
* Retourne vrai si la contrainte du sudoku n'est pas brisée
*/
int test_ligne(sudoku s, uint i, uint j, uint val) {
	uint x;
	for(x=0; x<DEFAULT_SUDOKU_SIZE; x++) {
		if(x==i) continue;
		if( val_case(s,x,j) == val) {
#if DEBUG_LEVEL > 1
			printf("ligne (%d %d) %d\n", x, j, val);
#endif
			return 0;
		}
	}
	return 1;
}

/**
* Fonction de test si il existe val sur la colonne j (sauf pour la ligne i)
* Retourne vrai si la contrainte du sudoku n'est pas brisée
*/
int test_colonne(sudoku s, uint i, uint j, uint val) {
	uint x;
	for(x=0; x<DEFAULT_SUDOKU_SIZE; x++) {
		if(x==j) continue;
		if( val_case(s,i,x) == val) {
#if DEBUG_LEVEL > 1
			printf("colonne (%d %d) %d\n", i, x, val);
#endif
			return 0;
		}
	}
	return 1;
}

/**
* Fonction de test si il existe val dans la region de (i,j) (sauf pour la position (i,j))
* Retourne vrai si la contrainte du sudoku n'est pas brisée
*/
int test_region(sudoku s, uint i, uint j, uint val) {
	uint x, y;
	for(x=0; x<3; x++) {
		for(y=0; y<3; y++) {
			int a= (int)i/3;
			int b= (int)j/3;
			uint xpos=3*a + x;
			uint ypos=3*b + y;
			if(xpos==i && ypos==j) continue;
			if( val_case(s,xpos,ypos) == val) {
#if DEBUG_LEVEL > 1
				printf("region (%d %d) %d\n", xpos, ypos, val);
#endif
				return 0;
			}
		}
	}
	return 1;
}

/**
* La fonction teste la possibilité de l'insertion de val à la position (i,j). Retourne vrai si 
* les contraintes du sudoku sont respectées. Les trois contraintes sont vérifiées : ligne, colonne et région.
* @param s sudoku
* @param i numero de ligne de l'insertion
* @param j numero de colonne de l'insertion
* @param val valeur à insérer
*/
int test_insert(sudoku s, uint i, uint j, uint val) {
	if( test_ligne(s,i,j,val) && test_colonne(s,i,j,val) &&test_region(s,i,j,val)) {
		return 1;
	} else {
		return 0;
	}
}

/**
* Fonction récursive de résolution
* @param s grille de sudoku
* @param i,j position qu'on cherche à compléter
* @return Retourne 1 si l'insertion est faite avec succes, et 0 sinon
*/
int back_tracking_rec(sudoku s, uint i, uint j)
{
#if DEBUG_LEVEL > 0
	printf("%d %d\n", i,j);
#endif
	if( val_case(s, i, j) == EMPTY ) {
		uint val;
		for(val=1; val<=9; val++) {
			if( test_insert(s, i, j, val) ) {
				put_case(s, i, j, val);
#if DEBUG_LEVEL > 0
				print(s);
#endif
				//next case par récursion :
				if( i==DEFAULT_SUDOKU_SIZE-1 ) {
					if( j==DEFAULT_SUDOKU_SIZE-1 ) {
#if DEBUG_LEVEL > 1
						printf("fini\n");
#endif
						return 1;
					} else {
						if( back_tracking_rec(s, 0, j+1) ) return 1;
					}
				} else {
					if( back_tracking_rec(s, i+1, j) ) return 1;
				}
			}
		}
		//Si je suis ici, c'est que j'ai rien trouvé de valable : il faut revenir en arrière
		put_case(s, i, j, EMPTY);
#if DEBUG_LEVEL > 0
		printf("back (%d %d)\n", i,j);
		print(s);
#endif
		return 0;
	} else {
		//next case par recursion
		if(i==DEFAULT_SUDOKU_SIZE-1) {
			if(j==DEFAULT_SUDOKU_SIZE-1) {
				return 1;
			} else {
				if( back_tracking_rec(s, 0, j+1) ) return 1;
			}
		} else {
			if( back_tracking_rec(s, i+1, j) ) return 1;
		}
#if DEBUG_LEVEL > 0
		printf("back (completed) (%d %d)\n", i,j);
		print(s);
#endif
		return 0;
	}
#if DEBUG_LEVEL > 0
	printf("Position anormale ??\n");
#endif
	return 0;
}


/**
* Fonction récursive de iteratif
* @param s grille de sudoku
* @return Retourne 1 si le sudoku a une solution, et 0 sinon
*/
int back_tracking_ite(sudoku s)
{
	int i, j;
	
	//Construction d'une matrice d'occupation qui indique les cases à ne pas toucher !
	sudoku occupation = new_sudoku();
	for(i=0; i<DEFAULT_SUDOKU_SIZE; i++) {
		for(j=0; j<DEFAULT_SUDOKU_SIZE; j++) {
			if( val_case(s, i, j) == EMPTY )
				put_case(occupation, i, j, EMPTY);
			else
				put_case(occupation, i, j, NOT_EMPTY);
		}
	}
	
	while( i!=DEFAULT_SUDOKU_SIZE-1 && j!=DEFAULT_SUDOKU_SIZE-1 ) {
		
		//Tant qu'on tombe sur une case occupée au départ :
		while( val_case(occupation, i, j) == NOT_EMPTY && (i!=DEFAULT_SUDOKU_SIZE-1 && j!=DEFAULT_SUDOKU_SIZE-1) ) {
			//on passe à la case suivante :
			if( i==DEFAULT_SUDOKU_SIZE-1 ) {
				i=0;
				j++;
			} else {
				i++;
			}
		}
		
		//Cas d'une fin sur une case remplie :
		if(val_case(occupation, i, j) != NOT_EMPTY) break; 
		
		uint val;
		
		if( val_case(s, i, j) == EMPTY ) {
			//Si la case est vide, on part 1
			val=1;
		} else {
			//Sinon, on repart de la dernière valeur testée +1
			val=val_case(s, i, j)+1;
		}
		int inserted=0;
		for(; val<=9; val++) {
			if( test_insert(s, i, j, val) ) {
				put_case(s, i, j, val);
				inserted=1;
#if DEBUG_LEVEL > 1
				printf("insert (%d %d)\n", i,j);
				print(s);
#endif
				//on passe à la case suivante :
				if( i==DEFAULT_SUDOKU_SIZE-1 ) {
					i=0;
					j++;
				} else {
					i++;
				}
			}
		}
		
		if( !inserted) {
			//Si je suis ici, c'est que j'ai rien trouvé de valable : il faut revenir en arrière
#if DEBUG_LEVEL > 0
			printf("back (%d %d)\n", i,j);
			print(s);
#endif
			//Je reviens en arrière sur la dernière case modifiable :
			while( val_case(occupation, i, j) == NOT_EMPTY && (i!=0 && j!=0) ) {
				//on passe à la case précédente :
				if( i==0 ) {
					i=DEFAULT_SUDOKU_SIZE-1;
					j--;
				} else {
					i--;
				}
			}
			
			if( i==0 && j==0) {
				//Pas de solution
				return 0;
			}
		}
	}

	//Liberation mémoire
	free_sudoku(occupation);
	return 1;
}



/**
* Fonction d'unterface pour lancer la résolution récursive.
* @param s grille de sudoku
*/
void back_tracking(sudoku s)
{
	back_tracking_rec(s, 0, 0);
}


/**
* Fonction d'évaluation d'une case
* @param i,j position évaluée
* @return La valeur de la case (EMPTY si case vide) si les coordonnées sont correcte et -1 sinon
*/
int val_case(sudoku s, uint i, uint j) {
	if( i>= DEFAULT_SUDOKU_SIZE || j>= DEFAULT_SUDOKU_SIZE ) return -1;
	return s[i][j];
}

/**
* Fonction de modification d'une valeur de la grille. Si la valeur est incorrecte, la valeur insérée est EMPTY.
* La fonction vérifie les erreurs de position
* @param i,j position à modifier
* @param val valeur à insérer
* @param s grille de sudoku
*/
void put_case(sudoku s, uint i, uint j, uint val) {
	if( i>= DEFAULT_SUDOKU_SIZE || j>= DEFAULT_SUDOKU_SIZE ) return;
	if(val < 0 || val >9) val=EMPTY; //Si la valeur est abérrante, on la remet à EMPTY
	s[i][j]=val;
}


/**
* Fonction d'affichage d'une grille de sudoku
* @param s grille de sudoku
*/
void print(sudoku s)
{
	uint i, j;
	printf("___________________\n");
	for(i=0; i< DEFAULT_SUDOKU_SIZE; i++) {
		printf("|");
		for(j=0; j< DEFAULT_SUDOKU_SIZE; j++) {
			printf("%d|", s[i][j]);
		}
		printf("\n");
	}
	printf("___________________\n");
}

/**
* Fonction de création d'une nouvelle grille de sudoku initialisée à EMPTY
* La matrice ainsi créée est utilisable avec un adressage [][]
* @return Retourne une nouvelle grille de sudoku
*/
sudoku new_sudoku()
{
	int i;
	int **s;
	int *imptr;

	//Allocation de la place mémoire pour un sudoku
	s=(int**)malloc( sizeof(int*)*DEFAULT_SUDOKU_SIZE );
	if (s==NULL) {
		fprintf(stderr, "Erreur d'allocation de mémoire");
		return NULL;
	}

	imptr=(int*)malloc( sizeof(int)*DEFAULT_SUDOKU_SIZE*DEFAULT_SUDOKU_SIZE );
	if (imptr==NULL) {
		fprintf(stderr, "Erreur d'allocation de mémoire");
		return NULL;
	}

	//Initialisation des cases comme vide
	for(i=0;i<DEFAULT_SUDOKU_SIZE*DEFAULT_SUDOKU_SIZE;i++)
		imptr[i]=EMPTY;

	//Construction de l'adressage de la matrice :
	for(i=0;i<DEFAULT_SUDOKU_SIZE;i++,imptr+=DEFAULT_SUDOKU_SIZE) {
		s[i]=imptr;
	}

	return s;
}

/**
* Libération de la mémoire
* @param s matrice à détruire
*/
void free_sudoku(sudoku s) {
	free(*s);
	free(s);
}


/**
* Fonction de chargement d'une grille de sudoku à partir d'un fichier
* @param filename nom du fichier à télécharger
*/
sudoku load_sudoku(const char *filename)
{
	sudoku s=new_sudoku();

	FILE *file;
	// On ouvre un fichier en lecture
	file=fopen(filename, "r");
	if(file == NULL) {
		fprintf(stderr, "load_sudoku error : impossible d'ouvrir le fichier\n");
		return NULL;
	}

	uint i, j, val;
	for(i=0; i< DEFAULT_SUDOKU_SIZE; i++) {
		for(j=0; j< DEFAULT_SUDOKU_SIZE; j++) {
			if( feof(file) || !fscanf(file, "%d ", &val) ) {
				fprintf(stderr, "load_sudoku error : format incorrect\n");
			}
			if( val <0 || val >9 ) {
				fprintf(stderr, "load_sudoku error : grille invalide\n");
			}
			s[i][j]=val;
		}
	}
	
	fclose(file);
	return s;
}


