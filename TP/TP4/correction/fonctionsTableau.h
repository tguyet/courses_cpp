/**
* @file fonctionTableau.h
* @author T. Guyet, Agrocampus-Ouest
* @date 03/2011
*/


#ifndef FONCTIONS_TABLEAU //Instruction de preprocessing à reproduire ...
#define FONCTIONS_TABLEAU

/**
* @brief Fonction d'affichage de tableau avec les valeurs de 0 à length-1
* @param tab : tableau à afficher
* @param length : taille du tableau; prérequis: length doit être inférieur à la taille du tableau en mémoire
*/
void print(int *tab, int length);

/**
* @brief Fonction qui somme les éléments du tableau de 1 à length
* @param tab : tableau à traiter
* @param length : taille du tableau; prérequis: length doit être inférieur à la taille du tableau en mémoire
* @return : la somme des éléments
*/
int somme(int *tab, int length);

/**
* @brief Fonction d'initialisation d'un tableau avec des valeurs aléatoires entre 0 et mac
* @param tab : tableau à afficher
* @param length : taille du tableau; prérequis: length doit être inférieur à la taille du tableau en mémoire
* @param max :  valeur maximal des nombres aléatoires générés
*/
void initalea(int *tab, int length, int max);

/**
* @brief Fonction d'initialisation d'un tableau
* @param tab : tableau à afficher
* @param length : taille du tableau; prérequis: length doit être inférieur à la taille du tableau en mémoire
*
* Les modifications du contenu du tableau sont conservées en dehors de la fonction
*/
void init(int *tab, int length);


void decalage_droite(int *tab, int length);

void inversion(int *tab, int length);

int is_palindrome(int *tab, int length);

int cherche_premier(int *tab, int length, int elem);

void cherche_all(int *tab, int length, int elem);


#endif
