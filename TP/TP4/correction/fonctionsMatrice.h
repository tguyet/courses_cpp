/**
* @file fonctionsMatrice.h
* @author T. Guyet, Agrocampus-Ouest
* @date 03/2011
*/

#ifndef FONCTIONS_MATRICE
#define FONCTIONS_MATRICE

/**
* @brief Fonction pour la déclaration dynamique d'une matrice
* @param mat : matrice à afficher
* @param width, height : taille de la matrice
*
* La fonction se charge d'allouer correctement l'espace mémoire pour un tableau en deux dimensions de sorte à
* l'utiliser avec des notations crochetées.
*/
double **create_matrice(int width, int height);


/**
* @brief Fonction de destruction d'une matrice déclarée dynamiquement
* @param mat : matrice à détruire
* @param width, height : taille de la matrice
*/
void destroy_matrice(double **mat, int width, int height);


/**
* @brief Fonction d'affichage d'une matrice
* @param mat : matrice à afficher
* @param width, height : taille de la matrice
*/
void print(double **mat, int width, int height);


/**
* @brief Fonction qui somme les éléments de la matrice dans les limites de tailles width*height
* @param mat : matrice à afficher
* @param width, height : taille de la matrice
* @return : la somme des éléments
*/
double somme(double **mat, int width, int height);

/**
* @brief Fonction d'initialisation d'une matrice avec des valeurs aléatoires entre 0 et mac
* @param mat : matrice à afficher
* @param width, height : taille de la matrice
* @param max :  valeur maximal des nombres aléatoires générés
*/
void initalea(double **mat, int width, int height, int max);


/**
* @brief Fonction d'initialisation d'une matrice
* @param mat : matrice à afficher
* @param width, height : taille de la matrice
* 
* La case mat[i][j] est initialisé avec la valeur i+j.
*/
void init(double **mat, int width, int height);


/**
* @brief Fonction de somme des éléments diagonaux
* @param mat : matrice
* @param width, height : taille de la matrice
*/
double diag(double **mat, int width, int height);


/**
* @brief Fonction d'addition de deux matrices
* @param mat1, mat2 : matrices à additionner
* @param width, height : taille de la matrice
*
* La fonction créé une nouvelle matrice
*/
double **add(double **mat1, double **mat2, int width, int height);


/**
* @brief Fonction d'addition de deux matrices
* @param mat1, mat2 : matrices à additionner
* @param width1, height1, width2, height2 : taille des matrices
*
* Les matrices ne sont pas nécessairement carrées !
* La fonction créé une nouvelle matrice!
*/
double **mult(double **mat1, int width1, int height1, double **mat2, int width2, int height2);


#endif

