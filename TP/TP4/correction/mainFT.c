/**
* @file mainFT.c
* @author T. Guyet, Agrocampus-Ouest
* @date 03/2011
*/

#include "fonctionsTableau.h"
#include <stdio.h>

#define TAB_SIZE 4	//< Taille du tableau
#define VAL_MAX  2	//< Valeur max des éléments aléatoires

/**
* @brief Programme principal
* Le programme initialise un tableau de taille TAB_SIZE avec des valeurs 
* aléatoires entre 1 et VAL_MAX, affiche celui-ci et affiche la somme de ses
* éléments.
*/
int main()
{
	int tab[TAB_SIZE];

	//Initialisation du tableau
	initalea(tab, TAB_SIZE, VAL_MAX);

	//Affichage du tableau
	print(tab, TAB_SIZE);

	//Calcul de la somme des éléments du tableau
	int ret=somme(tab, TAB_SIZE);
	printf("La somme des éléments est %d.\n", ret);
	
	int pos = cherche_premier(tab, TAB_SIZE, 10);
	printf("Premiere occurrence de 10 à la position : %d\n",pos);
	
	cherche_all(tab, TAB_SIZE, 7);
	
	printf("Decalage\n");
	decalage_droite(tab, TAB_SIZE);
	print(tab, TAB_SIZE);
	
	printf("Inversion\n");
	inversion(tab, TAB_SIZE);
	print(tab, TAB_SIZE);
	
	printf("is_palindrome : %d\n", is_palindrome(tab, TAB_SIZE));

	return 0;
}

