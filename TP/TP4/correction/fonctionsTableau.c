/**
* @file fonctionTableau.c
* @author T. Guyet, Agrocampus-Ouest
* @date 03/2011
*/

#include <stdio.h>
#include <stdlib.h>	//Pour les fonctions rand et srand
#include <time.h> 	//Pour la fonction time
#include "fonctionsTableau.h"

void init(int *tab, int length)
{
	int i=0;
	for(i=0; i<length; i++) {
		tab[i] = i;
		//INVARIANT : tout élément j du tableau de 0 à i est initialisé avec la valeur j
	}
}

void initalea(int *tab, int length, int max)
{
	int i=0;

	//Initialisation du générateur aléatoire:
	srand ( time(NULL) );

	//On remplit effectivement le tableau	
	for(i=0; i<length; i++) {
		tab[i] = rand() % max;
		//INVARIANT : tous les éléments de 0 à i du tableau sont initialisés avec des valeurs aléatoires entre 0 et max-1 (compris)
	}
}

void print(int *tab, int length)
{
	int i=0;

	for(i=0; i<length; i++) {
		//Afficage sans retour à la ligne : les valeurs sont séparées par des virgules :
		printf("%d, ",tab[i]);
	}
	//Retour à la ligne final
	printf("\n");
}

int somme(int *tab, int length)
{
	int i=0, somme=0;

	for(i=0; i<length; i++) {
		somme+=tab[i];
		//INVARIANT: à la j-ieme boucle somme contient la somme des j premières cases du tableau
	}
	//ICI somme contient la somme des 'length' premières cases du tableau
	return somme;
}

int cherche_premier(int *tab, int length, int elem)
{
	int i;
	i=0;
	while( i<length ) {
		if( tab[i]== elem ) {
			return i;
		}
		i++;
	}
	return -1;
}

void cherche_all(int *tab, int length, int elem)
{
	int i=0;
	while( i<length ) {
		if( tab[i]== elem ) {
			printf("Element %d trouve à la position: %d\n", elem, i);
		}
		i++;
	}
}


void decalage_droite(int *tab, int length)
{
	int t, i;
	t = tab[length-1];
	i=length-2;
	while( i>0 ) {
		tab[i+1]=tab[i];
		i--;
	}
	tab[0]=t;
}

void inversion(int *tab, int length)
{
	int t, i=0;
	t = tab[length-1];
	int sign = 1-length%2;
	while( i<=length/2 ) {
		t=tab[length/2+i-sign];
		tab[length/2+i-sign]=tab[length/2-i];
		tab[length/2-i] = t;
		i++;
	}
}

int is_palindrome(int *tab, int length)
{
	int i=0;
	int sign = 1-length%2;
	while( i<=length/2 ) {
		if( tab[length/2+i-sign] != tab[length/2-i] ) {
			return 0;
		}
		i++;
	}
	return 1;
}

