/**
* @file mainMatrices.c
* @author T. Guyet, Agrocampus-Ouest
* @date 03/2011
*/

#include "fonctionsMatrice.h"
#include <stdio.h>


#define VAL_MAX 15	//< Valeur max des éléments aléatoires

/**
* @brief Programme principal
*/
int main()
{
	int width=20, height=10;

	//Allocation mémoire de la matrice
	double **mat = create_matrice(width, height);

	//Initialisation du tableau aléatoirement
	initalea(mat, width, height, VAL_MAX);

	//Affichage du tableau
	print(mat, width, height);

	//Calcul de la somme des éléments du tableau
	double ret=somme(mat, width, height);
	printf("La somme des éléments est %lf.\n", ret);


	double **mat2 = create_matrice(4, width); //< on prend comme hauteur, la valeur de width
	initalea(mat2, 4, height, VAL_MAX);
	double **matres=mult(mat, width, height, mat2, 4, width);
	print(matres, 4, height);

	//On détruit les matrices utilisées
	destroy_matrice(mat, width, height);
	destroy_matrice(mat2, 4, width);
	destroy_matrice(matres, 4, height);

	return 0;
}

