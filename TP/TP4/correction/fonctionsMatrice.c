/**
* @file fonctionsMatrice.c
* @author T. Guyet, Agrocampus-Ouest
* @date 03/2011
*/

#include <stdio.h>
#include <stdlib.h>	//Pour les fonctions rand et srand
#include <time.h> 	//Pour la fonction time
#include "fonctionsMatrice.h"

double **create_matrice(int width, int height) {
	int i;
	double **m;
	double *line;

	/* allocate pointers to rows */
	m=(double **) malloc((size_t)((nr)*sizeof(double*)));

	/* allocate rows and set pointers to them */
	line=(double *) malloc((size_t)((nr*nc)*sizeof(double)));
	for(i=0; i<nr; i++,line+=nc)
		m[i]=line;

	/* return pointer to array of pointers to rows */
	return m;
}


void destroy_matrice(double **mat, int width, int height) {
	int i;
	for(i=0; i< width; i++) {
		free(mat[i]);
	}
	free(mat);
}


void init(double **mat, int width, int height)
{
	int i=0, j=0;
	for(i=0; i<width; i++) {
		for(j=0; j<height; j++) {
			mat[i][j] = i+j;
		}
	}
}

void initalea(double **mat, int width, int height, int max)
{
	int i=0, j=0;

	//Initialisation du générateur aléatoire:
	srand ( time(NULL) );

	//On remplit effectivement la matrice
	for(i=0; i<width; i++) {
		for(j=0; j<height; j++) {
			mat[i][j] = (double)(rand() % max);
		}
	}
}

void print(double **mat, int width, int height)
{
	int i=0, j=0;

	for(i=0; i<width; i++) {
		for(j=0; j<height; j++) {
			//Afficage sans retour à la ligne : les valeurs sont séparées par des virgules :
			// Le %8.2lf spécifie un affichage sur 8 caractères avec 2 chiffres après la virgule !
			printf("%8.2lf, ",mat[i][j]);
		}
		//Retour à la ligne
		printf("\n");
	}
}

double somme(double **mat, int width, int height)
{
	int i=0, j=0;
	double somme=0.0;

	for(i=0; i<width; i++) {
		for(j=0; j<height; j++) {
			somme+=mat[i][j];
		}
	}
	return somme;
}


double diag(double **mat, int width, int height)
{
	int i=0;
	double somme=0.0;

	// La matrice n'est pas nécessairement carrée!
	// On parcours la diagonal tend qu'on ne "touche" pas un bord !
	for(i=0; i<width && i<height ; i++) {
		somme+=mat[i][i];
	}
	return somme;
}


double **add(double **mat1, double **mat2, int width, int height)
{
	int i=0, j=0;
	double **mat_somme=create_matrice(width, height);
	
	for(i=0; i<width; i++) {
		for(j=0; j<height; j++) {
			mat_somme[i][j]=mat1[i][j]+mat2[i][j];
		}
	}
	return mat_somme;
}


double **mult(double **mat1, int width1, int height1, double **mat2, int width2, int height2)
{
	if( width1!=height2) {
		//ICI la multiplication des matrices n'est pas possibles !
		return NULL;
	}
	int i=0, j=0, k=0;
	double **mat_prod=create_matrice(width2, height1);
	
	for(i=0; i<width2; i++) {
		for(j=0; j<height1; j++) {
			double somme=0;
			for(k=0; k<height2; k++) {
				somme+=mat1[k][j]*mat2[i][k];
			}
			mat_prod[i][j]=somme;
		}
	}
	return mat_prod;
}



