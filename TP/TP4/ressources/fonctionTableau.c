#include <stdio.h>

#define TAB_SIZE 40	//< Taille du tableau

/**
* @brief Fonction d'initialisation d'un tableau
* Les modifications du contenu du tableau sont conservées
* en dehors de la fonction
*/
void init(int *tab, int length)
{
	int i=0;
	for(i=0; i<length; i++) {
		tab[i] = i;
	}
}

/**
* @brief Fonction d'affichage de tableau
*/
void print(int *tab, int length)
{
	int i=0;
	for(i=0; i<length; i++) {
		printf("%d, ",tab[i]);
	}
	printf("\n");
}

/**
* @brief Programme principal
*/
int main()
{
	int tab[TAB_SIZE];

	//Initialisation du tableau
	init(tab, TAB_SIZE);

	//Affichage du tableau
	print(tab, TAB_SIZE);

	return 0;
}

