#include <stdio.h>

int fonction(int a)
{
	printf("dans fonction :\n");
	printf("\tavant: a=%d\n", a);
	a=35;
	printf("\tapres: a=%d\n", a);
	return a;
}


int pfonction(int *pa)
{
	printf("dans pfonction :\n");
	printf("\tavant: *pa=%d\n", *pa);
	*pa=27;
	printf("\tapres: *pa=%d\n", *pa);
	return *pa;
}

int main()
{
	int val =2, ret =0;

	printf("avant : val=%d\n",val);
	ret=fonction(val);
	printf("apres fonction : val=%d, ret=%d\n", val, ret);
	ret=pfonction(&val);
	printf("apres pfonction : val=%d, ret=%d\n", val, ret);
	return 0;
}

