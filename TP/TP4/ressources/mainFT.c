/**
* @file mainFT.c
* @author T. Guyet, Agrocampus-Ouest
* @date 04/2013
*/

#include <stdio.h>


/**
* @brief Fonction d'affichage de tableau avec les valeurs de 0 à length-1
* @param tab : tableau à afficher
* @param length : taille du tableau; prérequis: length doit être inférieur à la taille du tableau en mémoire
*/
void print(int *tab, int length)
{
	int i=0;

	for(i=0; i<length; i++) {
		//Afficage sans retour à la ligne : les valeurs sont séparées par des virgules :
		printf("%d, ",tab[i]);
	}
	//Retour à la ligne final
	printf("\n");
}

/**
* @brief Fonction qui somme les éléments du tableau de 1 à length
* @param tab : tableau à traiter
* @param length : taille du tableau; prérequis: length doit être inférieur à la taille du tableau en mémoire
* @return : la somme des éléments
*/
int somme(int *tab, int length)
{
	int i=0, somme=0;

	for(i=0; i<length; i++) {
		somme+=tab[i];
		//INVARIANT: à la j-ieme boucle somme contient la somme des j premières cases du tableau
	}
	//ICI somme contient la somme des 'length' premières cases du tableau
	return somme;
}

/**
* @brief Fonction d'initialisation d'un tableau avec des valeurs aléatoires entre 0 et mac
* @param tab : tableau à afficher
* @param length : taille du tableau; prérequis: length doit être inférieur à la taille du tableau en mémoire
* @param max :  valeur maximal des nombres aléatoires générés
*/
void initalea(int *tab, int length, int max)
{
	int i=0;

	//Initialisation du générateur aléatoire:
	srand ( time(NULL) );

	//On remplit effectivement le tableau	
	for(i=0; i<length; i++) {
		tab[i] = rand() % max;
		//INVARIANT : tous les éléments de 0 à i du tableau sont initialisés avec des valeurs aléatoires entre 0 et max-1 (compris)
	}
}

/**
* @brief Fonction d'initialisation d'un tableau
* @param tab : tableau à afficher
* @param length : taille du tableau; prérequis: length doit être inférieur à la taille du tableau en mémoire
*
* Les modifications du contenu du tableau tab sont conservées en dehors de la fonction
*/
void init(int *tab, int length)
{
	int i=0;
	for(i=0; i<length; i++) {
		tab[i] = i;
		//INVARIANT : tout élément j du tableau de 0 à i est initialisé avec la valeur j
	}
}



/**
* @brief Programme principal
* Le programme initialise un tableau de taille TAB_SIZE avec des valeurs 
* aléatoires entre 1 et VAL_MAX, affiche celui-ci et affiche la somme de ses
* éléments.
*/
int main()
{
	int tab[40];

	//Initialisation du tableau
	initalea(tab, 40, 15);

	//Affichage du tableau
	print(tab, 40);

	//Calcul de la somme des éléments du tableau
	int ret=somme(tab, 40);
	printf("La somme des éléments est %d.\n", ret);

	return 0;
}

