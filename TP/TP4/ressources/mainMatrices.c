/**
* @file mainMatrices.c
* @author T. Guyet, Agrocampus-Ouest
* @date 03/2011
*/

#include "fonctionsMatrice.h"
#include <stdio.h>


#define VAL_MAX 15	//< Valeur max des éléments aléatoires

/**
* @brief Programme principal
*/
int main()
{
	int width=20, height=10;


	//Allocation mémoire de la matrice
	double **mat = create_matrice(width, height);

	//Initialisation du tableau
	init(mat, width, height);

	//Affichage du tableau
	print(mat, width, height);



	//On détruit les matrices utilisées
	destroy_matrice(mat, width, height);

	return 0;
}

