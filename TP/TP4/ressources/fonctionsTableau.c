/**
* @file fonctionTableau.c
* @author T. Guyet, Agrocampus-Ouest
* @date 03/2011
*/

#include <stdio.h>
#include <stdlib.h>	//Pour les fonctions rand et srand
#include <time.h> 	//Pour la fonction time
#include "fonctionsTableau.h"

void init(int *tab, int length)
{
	int i=0;
	for(i=0; i<length; i++) {
		tab[i] = i;
		//INVARIANT : tout élément j du tableau de 0 à i est initialisé avec la valeur j
	}
}

void initalea(int *tab, int length, int max)
{
	int i=0;

	//Initialisation du générateur aléatoire:
	srand ( time(NULL) );

	//On remplit effectivement le tableau	
	for(i=0; i<length; i++) {
		tab[i] = rand() % max;
		//INVARIANT : tous les éléments de 0 à i du tableau sont initialisés avec des valeurs aléatoires entre 0 et max-1 (compris)
	}
}

void print(int *tab, int length)
{
	int i=0;

	for(i=0; i<length; i++) {
		//Afficage sans retour à la ligne : les valeurs sont séparées par des virgules :
		printf("%d, ",tab[i]);
	}
	//Retour à la ligne final
	printf("\n");
}

int somme(int *tab, int length)
{
	int i=0, somme=0;

	for(i=0; i<length; i++) {
		somme+=tab[i];
		//INVARIANT: à la j-ieme boucle somme contient la somme des j premières cases du tableau
	}
	//ICI somme contient la somme des 'length' premières cases du tableau
	return somme;
}

