/**
* @file fonctionsMatrice.h
* @author T. Guyet, Agrocampus-Ouest
* @date 03/2011
*/

#ifndef FONCTIONS_MATRICE
#define FONCTIONS_MATRICE

/**
* @brief Fonction pour la déclaration dynamique d'une matrice
* @param mat : matrice à afficher
* @param width, height : taille de la matrice
*
* La fonction se charge d'allouer correctement l'espace mémoire pour un tableau en deux dimensions de sorte à
* l'utiliser avec des notations crochetées.
*/
double **create_matrice(int width, int height);


/**
* @brief Fonction de destruction d'une matrice déclarée dynamiquement
* @param mat : matrice à détruire
* @param width, height : taille de la matrice
*/
void destroy_matrice(double **mat, int width, int height);


/**
* @brief Fonction d'affichage d'une matrice
* @param mat : matrice à afficher
* @param width, height : taille de la matrice
*/
void print(double **mat, int width, int height);


/**
* @brief Fonction d'initialisation d'une matrice
* @param mat : matrice à afficher
* @param width, height : taille de la matrice
* 
* La case mat[i][j] est initialisé avec la valeur i+j.
*/
void init(double **mat, int width, int height);

#endif

