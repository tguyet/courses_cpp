#include <stdio.h>

int a[]={0,1,2,3,4};


int main()
{
	int i, *p;
	
	for( i=0; i<=4;  i++) {
		printf("a[%d] = %d, ", i, a[i]);
	}
	printf("\n");
	
	for( p=&a[0]; p<=&a[4]; p++) {
		printf("*p = %d, ", *p);
	}
	printf("\n");
	
	for( p=&a[0], i=1; i<=5; i++) {
		printf("p[%d] = %d, ", i, p[i]);
	}
	printf("\n");
	
	for( p=a, i=0; p+i<=a+4; p++, i++) {
		printf("*(p+%d) = %d, ", i, *(p+i));
	}
	printf("\n");
}

