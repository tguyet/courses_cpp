/**
* @file fonctionsMatrice.c
* @author T. Guyet, Agrocampus-Ouest
* @date 03/2011
*/

#include <stdio.h>
#include <stdlib.h>	//Pour les fonctions rand et srand
#include <time.h> 	//Pour la fonction time
#include "fonctionsMatrice.h"

double **create_matrice(int width, int height) {
	int i;
	double **tab=(double **)calloc( width, sizeof(double *) );
	for(i=0; i< width; i++) {
		tab[i]=(double *)calloc( height, sizeof(double) );
	}
	return tab;
}


void destroy_matrice(double **mat, int width, int height) {
	int i;
	for(i=0; i< width; i++) {
		free(mat[i]);
	}
	free(mat);
}


void init(double **mat, int width, int height)
{
	int i=0, j=0;
	for(i=0; i<width; i++) {
		for(j=0; j<height; j++) {
			mat[i][j] = i+j;
		}
	}
}

void print(double **mat, int width, int height)
{
	int i=0, j=0;

	for(i=0; i<width; i++) {
		for(j=0; j<height; j++) {
			//Afficage sans retour à la ligne : les valeurs sont séparées par des virgules :
			// Le %8.2lf spécifie un affichage sur 8 caractères avec 2 chiffres après la virgule !
			printf("%8.2lf, ",mat[i][j]);
		}
		//Retour à la ligne
		printf("\n");
	}
}




