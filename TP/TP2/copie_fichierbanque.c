#include <stdio.h>
#include <string.h>

int main()
{
	FILE *fin, *fout; //fin et fout represente des fichiers
	char strnom [80], strprenom [80];
	int valeur;

	fin = fopen ("banque.txt","r"); //Ouverture d'un fichier en lecture
	fout = fopen ("copiebanque.txt","w+"); //Ouverture d'un fichier en ecriture

	// feof : teste la presence d'un caractere de fin de fichier (EOF)
	// ferror : texte la presence d'une erreur
	while( !feof(fin) && !ferror(fin) )
	{
		//Lecture des informations structurees par ligne
		fscanf(fin,"%s %s %d", strprenom, strnom, &valeur);
		if( !strcmp( strnom, "Guyet") && !strcmp( strprenom, "Thomas") ) {
			valeur += 10;
		}

		//Ecriture dans le fichier
		fprintf (fout, "%s %s %d\n", strprenom, strnom, valeur);
	}

	//Fermeture des fichiers
	fclose(fout);
	fclose(fin);
	return 0;
}

