#include <stdio.h>
#include <string.h>


#define TAB_SIZE 2000 //< definition d'une constante

typedef struct {
	char nom[80];
	char symbole[3];
	int numero;
	double masse_molaire;
} Element;

int main()
{
	int i, nbelem=0;
	FILE *fin;
	Element elements[TAB_SIZE];

	/******************************************/
	// Chargement du fichier
	/******************************************/
	printf("J'apprends le tableau des éléments ...\n");

	//Ouverture d'un fichier en lecture
	fin = fopen ("elem1545.dat","r");
	
	i=0;
	// feof : teste la presence d'un caractere de fin de fichier (EOF)
	// ferror : texte la presence d'une erreur
	while( !feof(fin) && !ferror(fin) )
	{
		Element e;
		//Lecture des informations structurees par ligne
		fscanf(fin,"%s %s %d %lf", e.nom, e.symbole, &e.numero, &e.masse_molaire);

		elements[i]=e;
		i++;
	}
	//Ici, i donne le nombre d'éléments réels dans le tableau
	nbelem=i;

	if( ferror(fin) ) {
		fclose(fin);
		printf("Une erreur est survenue lors du chargement du fichier\n");
		return 1;
	}

	//Fermeture des fichiers
	fclose(fin);
	printf("Je connais mon tableau des elements par coeur, interrogez moi !\n");

	/******************************************/
	// Maintenant, on interroge le tableau
	/******************************************/

	int j, val=0;
	while( 1 ) {
		printf("Donnez moi le numero de l'element recherche (tapez une lettre pour quitter)\n");
		int ret = scanf("%d", &val);
		if( ret == 0 )
			break;

		//ICI, on ne sait pas si le tableau des éléments est ordonné, il faut donc le parcourrir pour trouver l'élément. On parcourt le tableau jusqu'à arriver la fin (on n'a rien trouvé) ou jusqu'à trouver cet élément
		j=0;
		while( j< nbelem && elements[j].numero!=val) { //Attention : l'ordre des tests est important !
			j++;
		}
		//ICI soit j == nbelem soit elements[j].e==val (exclusif)

		if( j != nbelem ) { //donc elements[j].e==val
			// On fait l'affichage :
			Element e=elements[j];
			printf("L'element recherche est le %s, de symbole %s, et de masse molaire %lf\n",e.nom, e.symbole, e.masse_molaire);
		} else {
			printf("L'element recherche n'a pas ete trouve! Ce numero n'est certainement pas valable\n");
		}
	}

	return 0;
}

