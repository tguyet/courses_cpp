/*
* Jeu du pendu
* T. Guyet, AGROCAMPUS-OUEST, 2013
*/
#include <stdio.h>	//Pour les fonctions printf et scanf
#include <stdlib.h> //Pour l'appel system
#include <string.h> //Pour la comparaison de chaines de caracteres strcmp

int main()
{
    char mot[100];
    char mottente[100];
    int masque[100];
    int cpt=0;
    char saisie;

    printf("saisir un mot a deviner\n");
    scanf("%s", mot);
    getchar();

    int i;
    for(i=0; i<100 && mot[i]!='\0'; i++) {
        if( (int)(mot[i])<97 || (int)(mot[i])>122 ) {
            printf("Le mot saisi n'est pas valide\n");
            exit(0);
        }
        masque[i]=0;
    }
    system("clear");

    printf("Le mot est de longueur %d\n", i);

    //Boucle de jeu : question 3
    while(1) {
        printf("Voulez vous proposez un mot (o/n) ou tester une nouvelle lettre ? (points : %d)\n", cpt);
        scanf("%c", &saisie);
        getchar();
        if( saisie=='o' ) {
            //test d'un mot
            printf("saisir un mot\n");
            scanf("%s", mottente);
            getchar();
            if( !strcmp(mot, mottente) ) {
                printf("!!! gagne !!!!\n");
                printf("Nombre de points : %d\n", cpt);
                break;
            } else {
                cpt+=1;
                printf("!!! ce n'est pas le mot recherche !!!\n");
            }
        } else {
            //test d'une lettre
            printf("saisir une lettre\n");
            scanf("%c", &saisie);
            getchar();

            //tester la lettre et afficher les positions correspondante :
            printf("positions de la lettre :\n");
            for(i=0; i<100 && mot[i]!='\0'; i++) {
                if( mot[i]==saisie ) {
                    masque[i]=1;
                }

                if( masque[i] ) {
                    printf("%c", mot[i]);
                } else {
                    printf("*");
                }
            }
            printf("\n");
            cpt+=2;
        }
    }

    return 0;
}



