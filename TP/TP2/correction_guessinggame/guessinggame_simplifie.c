/*
* Jeu du guessing game
* Version simplifiée
* T. Guyet, AGROCAMPUS-OUEST, 2013
*/
#include <stdio.h>	// Pour les fonctions printf et scanf
#include <stdlib.h> // Pour rand
#include <time.h>   // Pour time
#include <string.h>	// Pour strcpy

//Definition de constantes
#define MAX_TRIES 8


int main()
{
	int toguess=0;
	int val=-1, nb=0, i=0;

	//tirage aléatoire d'un nombre
	srand( time(NULL) );
	toguess = rand() % 100;

	while( nb<MAX_TRIES ) {
        //Saisie d'une valeur avec gestion des erreurs de saisie
		printf("Saisir une valeur entre 0 et 100 (plus que  %d essais)\n", MAX_TRIES-nb);
		scanf("%d", &val);
		nb++;// incrementation du nombre de saisie faite

		//Vérification du gain (le calcul de r sert pour la suite)
		int r = val-toguess;

		if( r<0 ) {
		    printf("trop petit\n");
		}else if(r>0) {
		    printf("trop grand\n");
		} else {
		    printf("Vous avez gagné en %d coups!\n", nb);
			break;
		}
	}

    if( val!=toguess ) {
        printf("Perdu !!\n");
        printf("\tla valeur a trouver était la valeur %d! Réessayez !!\n", toguess);
    }

	return 0;
}


