/*
* Jeu du guessing game
* Version complete (avec option de quitter)
* T. Guyet, AGROCAMPUS-OUEST, 2013
*/
#include <stdio.h>	// Pour les fonctions printf et scanf
#include <stdlib.h> // Pour rand
#include <time.h>   // Pour time
#include <string.h>	// Pour strcpy

//Definition de constantes
#define MAX_TRIES 8
#define MAX_SAISIE 3


int main()
{
	int toguess=0;
	int val=-1, nb=0, i=0, nb_tent=0;
	int limits[]={5,10,20,50};
	char messages[5][100]; // 5 messages textuels (tableaux de chaînes de caractères)
	char saisie[100];
	strcpy(messages[0], "tres chaud");
	strcpy(messages[1], "chaud");
	strcpy(messages[2], "tiede");
	strcpy(messages[3], "froid");
	strcpy(messages[4], "très froid");

	//On prepare les distances aux carrees
	for(i=0; i<4; i++ ) {
		limits[i]=limits[i]*limits[i];
	}

	//tirage aléatoire d'un nombre
	srand( time(NULL) );
	toguess = rand() % 100;

	while( nb<MAX_TRIES ) {
        //Saisie d'une valeur avec gestion des erreurs de saisie
		printf("Saisir une valeur entre 0 et 100 (plus que  %d essais), 'q' pour quitter\n", MAX_TRIES-nb);
		nb_tent=0;

        // On recupère une saisie en format chaine de caractère
		scanf("%s", saisie);
		if( !strcmp(saisie, "q") ) {
		    printf("au revoir ...\n");
		    exit(0);
		}

		while( (sscanf(saisie, "%d", &val)!=1 || val<0 || val>100) && nb_tent<MAX_SAISIE ) {
            printf("Un nombre entier entre 0 et 100 on t'a dis ! triple buse !\nrecommence ...\n");
            scanf("%s", saisie);
            if( !strcmp(saisie, "q") ) {
                printf("au revoir ...\n");
                exit(0);
            }

            nb_tent++;
		}
		if( nb_tent>=MAX_SAISIE ) {
            printf("On n'est pas fait pour se comprendre !!\n");
            return -1; //sortie avec erreur
		}
		nb++;// incrementation du nombre de saisie faite

		//Vérification du gain (le calcul de r sert pour la suite)
		int r = val-toguess;
		if( !r ) { //peu se traduire par : if( r!=0 )
			printf("Vous avez gagné en %d coups!\n", nb);
			break;
		}

		//On determine le message a faire afficher
		int dist = r*r; // calcule de la distance au carré
		int pos=0;
		while( dist > limits[pos] && pos<4 ) {
			pos++;
		}

		//Affichage du message
		printf("Vous êtes %s\n",messages[pos]);
	}

    if( val!=toguess ) {
        printf("Perdu !!\n");
        printf("\tla valeur a trouver était la valeur %d! Réessayez !!\n", toguess);
    }

	return 0;
}


