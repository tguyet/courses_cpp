#include <stdio.h>

int main(void) {
    FILE * fp = fopen("machin.chose", "rb"); // Ouverture du fichier
    
    if (fp == NULL) {
	/* L'ouverture du fichier a echoue => le fichier n'existe pas ou il est bloqué à la lecture (ou autre ?). */
        fprintf(stderr, "La fonction fopen a echoue.\n");
    } else {
        int c; //Contient la valeur courante pendant la lecture
        long taille = 0; //Longueur du fichier
        
        while ((c = fgetc(fp)) != EOF)  // Sur cette ligne, il se passe deux choses : on récupère 
					// la valeur du caractère lu dans c et on teste ensuite son égalité avec EOF
        {
		//ICI, on peut utiliser c comme on veut/doit !
            
		//Incrémentation de la taille
        	taille++; 
        }
        
	// La lecture s'est arrêtée
        if ( feof(fp) ) {
        	printf("Fin de fichier atteinte !\n");
        	printf("La taille du fichier est : %ld octets.\n", taille);
        } else {
        	if( ferror(fp) )
        		printf("Arret du programme en raison d'une erreur de lecture !\n");
        }
    }
    
    return 0;
}

