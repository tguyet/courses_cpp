/*
* Programme d'estimation de la précision de la représentation des nombres flottants et de la disparité 
* de cette précision sur les réels (moins de précision pour comparer des grands nombres)
* T. Guyet, AGROCAMPUS-OUEST, 2010
*/

#include <stdio.h>

//On définit ci-dessous le type de nombre qu'on utilise pour représenter les variables flottantes
// Changer le type entre float, long float, double et long double
#define TYPE_FLOTTANT long double

int main()
{
	long int i=0;
	TYPE_FLOTTANT f=1;			
	TYPE_FLOTTANT g_init=10000000.0; //Changer cette valeur entre 0, 10000.0 et 10000000.0
	TYPE_FLOTTANT g=g_init+f; //g vaut toujours g_init + f, où f est un infinitésimal.
	while( g != g_init ) { //Test si g est toujours bien distingué de g_init
		// Si oui, on essaye avec un infinitésimal plus petit
		f/=10;
		g=g_init+f;
		i++;
	}
	//ICI on sait que la précision de comparaison est de 10^(-i)
	
	//On affiche le résultat :
	printf("%ld\n",i);
	return 0;
}


