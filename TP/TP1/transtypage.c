#include <stdio.h>

int main() {
	int i;
	unsigned int ui;
	long l;
	char c;

	printf("Rentrer un nombre\n");
	scanf("%d",&i); // On attend la saisie clavier validee avec Return (en supposant que se sera un nombre)

	c=(char)i; //Transtypage vers un caractere
	printf("Votre nombre correspond au caractere : %c\n", c);
	
	ui=(unsigned int)i; //Transtypage vers un non signe
	printf("Votre nombre correspond a l'entier (signe) : %u\n", ui);

	l=(long)i; //Transtypage vers un long
	printf("Votre nombre correspond au long : %ld\n", l);

	return 0;
}
