#include <stdio.h>

typedef enum {Base, Avance, Expert} type_livre;

typedef struct {
	char nom[200];
	int nb_auteur;
	char auteurs[5][20];
	int annee;
	type_livre type;
	long ISBN;
} Livre;


int main() {
	Livre l;
	
	printf("Donner le nom du livre\n");
	scanf("%s", l.nom);
	fflush(stdin); //Cette instruction est nécessaire pour éviter des pb de saisie (demander des explications!)
	
	printf("Donner le nombre d'auteurs\n");
	scanf("%d", &(l.nb_auteur));
	fflush(stdin);

	int i=0;
	while( i<l.nb_auteur ) {
		printf("Donner le nom de l'auteur %d\n", i);
		scanf("%s", l.auteurs[i]);
		fflush(stdin);
		i++;
	}

	l.annee = 0;
	printf("Saisir l'année d'édition (YYYY)\n");
	scanf("%d", &(l.annee));
	fflush(stdin);
	while( l.annee<1000 || l.annee>2011 ) {
		printf("Saisir l'année d'édition (YYYY)\n");
		scanf("%d", &(l.annee));
	}

	printf("Merci!\nRécapitulatif :\n");
	printf("\tNom : %s\n", l.nom);
	//TODO : terminer l'affichage récapitulatif ...

	return 0;
}

