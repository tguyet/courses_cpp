#include <stdio.h>

int main(int argc, char** argv) {
	unsigned short int a = 45000; 	//Ici, un "unsigned short int" est un entier positif codé sur 16 bits
	short int b;					//un "short int" est entier codé sur 16 bits

	//Quelques exemples de transtypage
	long c=(long)a;			// opération de transtypage vers un type entier relatif sur 32 bits
	b = (short int)a;		// opération de transtypage vers un type entier relatif sur 16 bits
	double d = (double)a;	// Transformation entre un entier vers un flottant ...

	//Vérification des valeurs "comprises" par le programme
	printf("%d %d %ld %lf\n", a, b, c, d);

	//Dans l'autre sens
	b=-21345;
	a=(unsigned short int)b;
	printf("%d %d\n",a,b);
}

