/*
* Programme de test des options d'optimisation du code C
*/
#include <stdio.h>

int main()
{
	long i=0;
	long somme=0;

	//Exécute 1 million de division avec reste
	for(i=0;i<1000000000L;i++)
		somme=(somme+i)%34;

	//Affichage du résultat
	printf("%ld\n",somme);
	return 0;
}


