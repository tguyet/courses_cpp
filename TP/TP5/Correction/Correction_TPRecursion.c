
#include <stdio.h>

//Exercice 1

long fact(int n) {
	int i=0;
	long f=1;
	for(i=2; i<=n; i++) {
		f*=i;
	}
	return f;
}

//Exercice 2

long factr(int i) {
	if( i==0) {
		return 1L; //cette notation indique une constante valant 1 codée en long.
	} else {
		return i * factr(i-1);
	}
}

long factr_print(int i) {
	long f=1L;
	printf(" > factr(%d)\n", i);
	if( i!=0){
		//Cas de récursion
		f= i * factr(i-1);
	} else {
		//Cas terminal : notez que tout le "else" pourrait être supprimé !!
		f = 1L;
	}
	printf(" < factr(%d)=%ld\n", i, f);
	return f;
}


long combi(int n, int k) {
	return factr(n)/(factr(k)*factr(n-k));
}


//Exercice 3

long combir(int n, int k) {
	if( k<0 || k>n ) {
		//Cas terminal
		return 0;
	} else if( k==1 ) {
		//Cas terminal
		return n;
	} else {
		return combir(n-1, k) + combir(n-1, k-1);
	}
}


int main()
{
	int n, k;
	printf("saisir deux nombres entiers séparés d'un espace\n");
	printf("ATTENTION à ne pas donner des nombres trop grands ...\n");
	if( 2!=scanf("%d %d", &n, &k) ) {
		printf("erreur de saisie\n");
		return 1;
	}

	long val;
	
	val = fact(n);
	printf("fact(%d)=%ld\n", n, val);
	val = factr(n);
	printf("factr(%d)=%ld\n", n, val);
	val = combi(n, k);
	printf("combi(%d, %d)=%ld\n", n, k, val);
	val = combir(n, k);
	printf("combir(%d, %d)=%ld\n", n, k, val);
	return 0;
}


