set autoscale
set xtic auto
set ytic auto
set title "Pi Convergence"
set nokey
#set terminal postscript portrait enhanced mono dashed lw 1 "Helvetica" 14 
#set output "my-plot.ps"
set terminal x11

#set multiplot;
#set size 0.5,0.5;
#set origin 0.0,0.0; plot "gregory.txt" with lines;
#set origin 0.5,0.0; plot "montecarlo.txt" with lines;
#set origin 0.0,0.5; plot "jpborwein.txt" with lines
#unset multiplot

plot "gregory.txt" title 'Gregory' with lines, "montecarlo.txt" title 'Monte-Carlo' with lines,  "jpborwein.txt" title 'JP Borwein' with lines
