/**
* picalcul.c
* Fonctions pour le calcul de pi
* T. Guyet, AGROCAMPUS-OUEST, 2011
*/

// Définition d'une valeur constante pour Pi
#define PI (double)3.1415926535897932384626433832795028841971693993751

#include <stdio.h>
#include <math.h>
#include "lois.h"

#define GENERATE_FILE 1

double gregory(int n)
{
#if GENERATE_FILE
	FILE *fout;
	fout = fopen ("gregory.txt","w+");
#endif
	double pi=0.0;
	int k=1;
	while( k<=n ) {
		int sign = 2*(k%2)-1; //sign vaut -1 si k est pair et 1 sinon, Attention aux parenthèses : '*' est prioritaire sur '%' !!
		pi += (double)sign/(double)(2*k-1); //Attention, il ne faut pas oublier les cast !
		
#if GENERATE_FILE
		fprintf (fout, "%d %lf\n", k, 4*pi);
#endif
		k++;
	} 

#if GENERATE_FILE
	fclose(fout);
#endif
	return 4*pi;
}


int testgregory(int p)
{
	double pi=0.0;
	int k=1;
	
	//On calcule la precision a atteindre avant la boucle !
	double prec=pow(10, (double)-p);
	
	//Attention, pi tend vers PI/4 !!!!
	while( fabs(PI - 4*pi)>prec ) {
		int sign = 2*(k%2)-1;
		pi += (double)sign/(double)(2*k-1);
		k++;
	} 

	return k;
}

double PiMonteCarlo(int n) {
#if GENERATE_FILE
	FILE *fout;
	fout = fopen ("montecarlo.txt","w+");
#endif
	int k=0;
	int nbin=0; //nb de points dans le quart de cercle
	while( k<n ) {
		double x=unif();
		double y=unif();
		if( x*x+y*y <1 ) nbin++;
#if GENERATE_FILE
		fprintf (fout, "%d %lf\n", k, 4.0*(double)nbin/(double)k);
#endif
		k++;
	}

#if GENERATE_FILE
	fclose(fout);
#endif

	return 4.0*(double)nbin/(double)n;
}

int testPiMonteCarlo(int p) {
	double pi=0.0;
	int k=1;
	int nbin=0; //nb de points dans le quart de cercle
	
	//On calcule la precision a atteindre avant la boucle !
	double prec=pow(10, (double)-p);
	
	while( fabs(PI - pi)>prec ) {
		double x=unif();
		double y=unif();
		
		if( x*x+y*y <1 ) nbin++;
		k++;
		
		pi=4.0*(double)nbin/(double)k; //< Pourrait être amélioré !!
		// printf("%lf\n", pi); //décommenter pour voir progresser la convergence ...
	}

	return k;
}




/**
* Ce code n'est pas optimisé : l'utilisation de la fonction pow est très lente !
*/
double f(double y)
{
	return pow(1-y*y*y*y, 0.25);
}

double JPBorwein(int n) {
#if GENERATE_FILE
	FILE *fout;
	fout = fopen ("jpborwein.txt","w+");
#endif
	int k=0;
	double z,b;
	double y=sqrt(2)-1;
	double a=6-4*sqrt(2);
	
	while( k<n ) {
		z=(1.0-f(y))/(1.0+f(y));
		
		double r=(1+z);
		b=a * r*r*r*r - pow(2, 2*k+3) *z*(1+z+z*z);
		
		//On passe au k suivant
		y=z;
		a=b;
#if GENERATE_FILE
		fprintf (fout, "%d %lf\n", k, 1.0/a);
#endif

		k++;
	}

#if GENERATE_FILE
	fclose(fout);
#endif
	return 1.0/a;
}


int main() {
	int n=100;
	double pi= gregory(n);
	printf("gregory(%d)=%.20lf\n",n,pi);
	
	rand_init();
	pi= PiMonteCarlo(n);
	printf("PiMonteCarlo(%d)=%.20lf\n",n,pi);
	
	pi=JPBorwein(3);
	printf("JPBorwein(%d)=%.20lf\n",n, pi);
	
	int prec=4;
	n=testgregory(prec);
	printf("Nb de tour pour une précision de 10^%d, Gregory : %d\n", prec, n);
	n=testPiMonteCarlo(prec);
	printf("Nb de tour pour une précision de 10^%d, MonteCarlo : %d\n", prec, n);
	
	return 0;
}

