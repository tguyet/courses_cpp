#include <stdio.h>
#include <string.h>

typedef struct {
	int val;
	char texte[20];
} toorder;

void print(toorder t) {
	printf("> %d, %s\n", t.val, t.texte);
}

void pinvert(toorder **u, toorder **v) {
	toorder *t;
	t=*u;
	*u=*v;
	*v=t;
}

void main() {
	toorder t1, t2;
	t1.val=4;
	strcpy(t1.texte, "coucou"); //< copie le texte dans l'emplacement t1.texte
	t2.val=2;
	strcpy(t2.texte, "hello");

	toorder *pt1 =&t1, *pt2 =&t2;

	//Les dessins représentent la bande rendu à ce point

	printf("***** initialement *****\n");
	printf("t1 : ");
	print(t1);
	printf("t2 : ");
	print(t2);
	printf("------------ :\n");
	printf("pt1 : ");
	print(*pt1);
	printf("pt2 : ");
	print(*pt2);

	pinvert(&pt1, &pt2);

	printf("***** apres pinvert *****\n");
	printf("t1 : ");
	print(t1);
	printf("t2 : ");
	print(t2);
	printf("------------ :\n");
	printf("pt1 : ");
	print(*pt1);
	printf("pt2 : ");
	print(*pt2);
}
