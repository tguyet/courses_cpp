# 0) vérifier les noms de fichier !!
# 1) lancer gnuplot en tapant "gnuplot" dans un terminal ... 
# 2) dans gnuplot : taper "load("convergence.pl")"

set autoscale
set xtic auto
set ytic auto
set label
set nokey
set terminal x11

#set multiplot;
#set size 0.5,0.5;
#set title "Gregory"
#set origin 0.0,0.5; plot "gregory.txt" title 'Gregory' with lines;
#set title "Monte-Carlo"
#set origin 0.5,0.5; plot "montecarlo.txt" with lines;
#set title "JP Borwein"
#set origin 0.5,0.0; plot "jpborwein.txt" with lines
#unset multiplot

plot "gregory.txt" title 'Gregory' with lines, "montecarlo.txt" title 'Monte-Carlo' with lines,  "jpborwein.txt" title 'JP Borwein' with lines
