#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "lois.h"

void rand_init() {
	srand(time(NULL));
}


//Loi uniforme
double unif()
{
	return (double)((float)rand() / ((float)RAND_MAX + 1.0)) ;
}

//Loi normale
double normale(double m, double s)
{
      double x1,x2,y;
      x1 = unif();
      x2 = unif();
      // methode de Boc-Muller
      // <y> suit une loi normale reduite (m=0,s=1)
      y = pow(-2*log(x1),0.5)*cos(2.* 3.*x2);
      return m + s*y;
}

//Loi de poisson
/*
double poisson(double lambda)
{
    int k = rand();
    return (exp(logn(lambda)*k-lambda)/factorielle(k));
}
*/
