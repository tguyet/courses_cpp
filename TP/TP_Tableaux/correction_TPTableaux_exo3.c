
#include <stdio.h>

#define LENGTH_MAX 1000

int nbvoy(char *s) {
	int i=0;
	int nb=0;
	for(i=0;s[i]!=0;i++) {
		if( s[i]=='a' ||s[i]=='e' ||s[i]=='i' ||s[i]=='o' ||s[i]=='u' ) {
			nb++;
		}
	}
	return nb;
}


int find(char *s, char c) {
	int i=0;
	int nb=0;
	while( s[i]!=0 ) {
		if( s[i]== c) {
			nb++;
		}
		i++;
	}
	return nb;
}

/**
* \param s chaine de caracteres
* \param ss sous-chaine de caractères
* \return la position de la premiere occurrence de la sous-chaine ss, ou -1 si on n'a rien trouvé!
* Cette question peut être traitée de beaucoup de manière différente. Ici, il ne s'agit pas d'un algorithme optimal.
*/
int findss(char *s, char *ss) {
	int i=0, j=0, k;	
	while( s[i]!=0 ) {
		//On test si ss se trouve à partir de la position i
		j=i;
		k=0;
		while( ss[k]!=0 && ss[k]==s[j]) {
			k++;
			j++;
		}
		//ICI ss[k]==0 (et on a donc trouvé ss) ou ss[k]!=s[j]
		if( ss[k]==0) {
			return i;
		}
		i++;
	}
	return -1;
}

int main() {
	char string[LENGTH_MAX];
	char c;
	char ss[LENGTH_MAX];

	printf("Entrez une ligne texte (sans espaces ...)\n");
	scanf("%s", string);

	int nb=nbvoy(string);
	printf("nb voy : %d\n", nb);


	printf("Entrez un caractères à compter\n");
	scanf("%c", &c);

	nb=find(string, c);
	printf("nb de %c : %d\n", c, nb);

	printf("Entrez une sous-chaîne à chercher\n");
	scanf("%s", ss);

	nb=findss(string, ss);
	if( nb>=0) {
		printf("première position de la sous-chaine '%s' : %d\n", ss, nb);
	} else {
		printf("la sous chaine n'a pas été trouvée\n");
	}

	return 0;
}


