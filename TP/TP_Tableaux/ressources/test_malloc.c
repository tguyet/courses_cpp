#include <stdlib.h>
#include <stdio.h>

/**
* @brief TODO
*/
int* cp( int* tab, int length) {
  int i;
  int *p=tab, *pret;
  int *tret = (int *) malloc( sizeof(int)*length );
  
  pret=tret;
  for( i=0; i<length; i++, p++, pret++) {
    *pret=*p;
    *p=i;
  }
  
  return tret;
}

int main() {
  int i;
  int T[]={6,5,4,3,2,1};
  int len=6;

  int *TT=NULL;
  TT=cp(T, len);
//  T=cp(T, len);
  
  //utilisation de TT
  for( i=0; i<len; i++) {
    printf("%d, ", TT[i]);
  }
  printf("\n");
  
  free( TT );
  
  return 0;
}
