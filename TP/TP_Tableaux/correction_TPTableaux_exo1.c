#define TAB_SIZE 15 //< definition d'une constante TAB_SIZE dont la valeur est 100
#include <stdio.h>	//Pour la fonction printf
#include <stdlib.h>	//Pour les fonctions rand et srand
#include <time.h> 	//Pour la fonction time


//Question a
/**
* Fonction d'initialisation d'un tableau de taille n avec des entiers
* entre 0 et 100
*/
void init(int *tab, int n) {
	srand(time(NULL));
	int i=0;
	for(i=0; i<n; i++) {
		tab[i]= rand() % 100;
	}
}

//Question b
/**
* Affichage "joli" d'un tableau : obligé de traité à part les bords du tableau !
* La fonction termine l'affichage par un retour à la ligne.
*/
void print(int *tab, int n) {
	if( n==0 ) return; //je traite ce cas à part !

	int i=0;
	printf("[");
	for(i=0; i<n-1; i++) {
		printf("%d,", tab[i]);
	}
	printf("%d]\n", tab[i]);
}


//Question d
/**
* Calcul la somme des n premiers éléments du tableau tab
*/
long somme(int *tab, int n) {
	long s=0;
	int i;
	for(i=0; i<n; i++) {
		s+=tab[i];
	}
	return s;
}

/**
* Calcul la moyenne des n premiers éléments du tableau tab
*/
float moyenne(int *tab, int n) {
	float s=somme(tab, n); //pas de pb pour mettre un entier dans un float !
	s = s / ((float)n); //je force le cast à un float
	return s;
}


/**
* Retourne le maximum des n premiers éléments du tableau tab
*/
int max(int *tab, int n) {
	int m=tab[0]; //toujours bien penser à cela !!
	int *p=tab;
	int i=0;
	while(i<n) {
		//ICI *p désigne la i-eme case du tableau tab
		if( m<*p ) {
			m=*p;
		}
		i++;
		p++;
	}
	return m;
}



int main() {
	int tab[TAB_SIZE], tab2[TAB_SIZE];

	init(tab, TAB_SIZE);
	print(tab, TAB_SIZE);

	long s=somme(tab, TAB_SIZE);
	float moy = moyenne(tab, TAB_SIZE);
	int m = max(tab, TAB_SIZE);

	printf("somme=%ld moy=%.2f max=%d\n", s, moy, m);

	return 0;
}
