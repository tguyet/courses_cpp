/*
* Exemple d'utilisation du générateur pseudo-aléatoire
* T. Guyet, AGROCAMPUS-OUEST, 2010
*/
#include <stdio.h>	//Pour la fonction printf
#include <stdlib.h>	//Pour les fonctions rand et srand
#include <time.h> 	//Pour la fonction time

int main ()
{
	// initialisation du random seed (la graine) : de la graine dépend toute la séquence de nombres aléatoires générés par les appels de rand()
	// ATTENTION : si les graines sont identiques, les séquences de nombres sont les mêmes (on parle de générateur pseudo-aléatoire)
	srand ( time(NULL) );
	
	int i=0;
	
	//Génération de 10 nombres aléatoires affichés à l'écran.
	for(i=0;i<10; i++) {
		int nombrealea = rand() % 120; 	//< Génération d'un nombre entier entre 0 et 120 (rand() renvoit un entier, et %120 renvoit le reste de la division avec 120
		printf("%d\n", nombrealea		//< Affichage du nombre
	}
	return 0;
}


