#include <stdio.h>
#include <string.h>


/**
	Structure de données pour une réponse du questionnaire (un ligne du fichier)
*/
typedef struct {
	int sexe; 	//0 si garçon, 1 sinon
	int q[6];	//tableau des réponses aux questions (contient 6 questions)
} Reponse;


/**
	Programme pour la lecture du fichier structuré
*/
int main()
{
	FILE *fin;		//< le fichier à charger
	Reponse tab[100]; 	//< le tableau pour contenir les réponses
	char s[30];		//< variable pour la lecture de chaîne de caractères
	int nb=0;			//< variable pour la lecture du numero
	int i=0;			//< compteur


	/****************************************
		Chargement du fichier
	****************************************/
	fin = fopen ("Pref_jus_orange.csv","r"); //Ouverture du fichier en lecture

	if( fin==0 ) { //ICI le fichier n'a pas été ouvert correctement
		printf("le fichier n'existe pas \n");
		return 1;
	}

	while( !feof(fin) && !ferror(fin) )
	{
		//Lecture des informations structurees par ligne
		fscanf(fin,"%d %s %d %d %d %d %d %d", &nb, s, &(tab[i].q[0]), &(tab[i].q[1]), &(tab[i].q[2]), &(tab[i].q[3]), &(tab[i].q[4]), &(tab[i].q[5]) );

		if( !strcmp( s, "masculin")) {
			tab[i].sexe=0;
		} else {
			tab[i].sexe=1;
		}
		//Incrementation du compteur
		i++;
	}
	//Fermeture du fichier
	fclose(fin);
	
	i--;
	//ICI i contient le nombre de ligne lues

	/****************************************
		Affichage du tableau construit
	****************************************/
	int j;
	for(j=0; j<i; j++) {
		printf("Q%d (%d): %d %d %d %d %d %d\n", j, tab[j].sexe, tab[j].q[0], tab[j].q[1], tab[j].q[2], tab[j].q[3], tab[j].q[4], tab[j].q[5]);
	}

	return 0;
}

