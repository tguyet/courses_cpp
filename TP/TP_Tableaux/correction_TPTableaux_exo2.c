#define TAB_SIZE 15 //< definition d'une constante TAB_SIZE dont la valeur est 100
#include <stdio.h>	//Pour la fonction printf
#include <stdlib.h>	//Pour les fonctions rand et srand
#include <time.h> 	//Pour la fonction time


/**
* Fonction d'initialisation d'un tableau de taille n avec des entiers
* entre 0 et 10
*/
void init(int *tab, int n) {
	srand(time(NULL));
	int i=0;
	for(i=0; i<n; i++) {
		tab[i]= rand() % 10;
	}
}

/**
* Affichage "joli" d'un tableau : obligé de traité à part les bords du tableau !
* La fonction termine l'affichage par un retour à la ligne.
*/
void print(int *tab, int n) {
	if( n==0 ) return; //je traite ce cas à part !

	int i=0;
	printf("[");
	for(i=0; i<n-1; i++) {
		printf("%d,", tab[i]);
	}
	printf("%d]\n", tab[i]);
}


//Question a
/**
* Affiche toutes les positions d'un élément dans un tableau
*/
void findall(int *tab, int n, int elem) {
	int i;
	for(i=0; i<n; i++) {
		if( *tab == elem ) {
			printf("find %d at pos %d\n", elem, i);
		}
		tab++;
	}
}

//Question b
/**
* Fonction permettant de trouver la première position d'un élément dans un tableau
* \return Retourne un pointeur sur la position lorsqu'il a trouvé qqc et NULL sinon
*/
int* find(int *tab, int n, int elem) {
	int i;
	for(i=0; i<n; i++) {
		if( *tab == elem ) {
			return tab;
		}
		tab++;
	}
	return 0;
}


int main() {
	int tab[TAB_SIZE], tab2[TAB_SIZE];
	int val, pos;
	int *pi;	//pointeur sur un entier

	init(tab, TAB_SIZE);
	print(tab, TAB_SIZE);

	//on demande un valeur à trouver pour simplifier les tests :
	printf("Donner une valeur à rechercher\n");
	scanf("%d", &val);

	//Appel de la fonction findall : elle affiche directement les résultats !
	findall(tab, TAB_SIZE, val);
	
	//Appel de la fonction find
	pi = find(tab, TAB_SIZE, val);
	if( pi!=0 ) {
		//ICI, la fonction find a bien trouvé un élément du tableau correspondant à la recherche
		pos = (int)(pi-tab); //la position dans le tableau peut être trouvée 
							 // en calculant le **décalage** entre le pointeur de la position et 
							// le pointeur sur le début du tableau !!
		printf("First occurrence of %d at pos %d\n", *pi, pos ); 
	}

	
	printf("Question c (**)\n");
	//Question c : difficile
	int rl=TAB_SIZE; //Taille du tableau restant à parcourir
	pi=tab;
	while( rl>0) {
		//ICI pi est un pointeur sur un élément du tableau tel qu'il reste rl cases à traiter en
		// fin de tableau à partir de pi
		pi = find(pi, rl, val);
		if( pi==0 ) {
			//ICI, on n'a rien trouvé de nouveau jusqu'à la fin du tableau, on sort de la boucle
			break;
		}
		//ICI, pi est un pointeur sur une case de tab qui contient elem, on affiche :
		pos = (int)(pi-tab);
		printf("find %d at pos %d\n", *pi, (int)(pi-tab) );

		//On prépare maintenant pour la boucle suivante en repartant de la case SUIVANT celle
		// pointée par pi, donc :
		pi++;
		rl = TAB_SIZE - pos-1; //<- très important : il faut également ajuster la taille du tableau qu'il reste à parcourir !
	}

	return 0;
}
