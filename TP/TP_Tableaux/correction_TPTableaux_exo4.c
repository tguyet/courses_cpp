
#define TAB_SIZE 15 //< definition d'une constante TAB_SIZE dont la valeur est 100
#include <stdio.h>	//Pour la fonction printf
#include <stdlib.h>	//Pour les fonctions rand et srand
#include <time.h> 	//Pour la fonction time


/**
* Fonction d'initialisation d'un tableau de taille n avec des entiers
* entre 0 et 100
*/
void init(int *tab, int n) {
	srand(time(NULL));
	int i=0;
	for(i=0; i<n; i++) {
		tab[i]= rand() % 100;
	}
}

/**
* Affichage "joli" d'un tableau : obligé de traité à part les bords du tableau !
* La fonction termine l'affichage par un retour à la ligne.
*/
void print(int *tab, int n) {
	if( n==0 ) return; //je traite ce cas à part !

	int i=0;
	printf("[");
	for(i=0; i<n-1; i++) {
		printf("%d,", tab[i]);
	}
	printf("%d]\n", tab[i]);
}


//Programme bonus !!
/**
* décalage vers la gauche, en place, d'un tableau avec completion circulaire (on récupère la dernière valeur)
*/
void decal(int *T, int size) {
	int i=0;
	int val=T[0];
	for(i=1; i<size; i++) {
		T[i-1]=T[i];
	}
	T[size-1]=val;
}

//Question a)
/**
* Décalage en place vers la droite avec completion par un 0
* Il faut partir de la fin du tableau !
*/
void decalg(int *T, int size) {
	int i=0;
	for(i=size-2; i>=0; i--) {
		T[i+1]=T[i];
	}
	T[0]=0;
}


//question b)
void invert(int *T, int size) {
	int i=0;
	int val=0;
	for(i=0; i<(size/2); i++) {
		//Inversion de deux cases T[i] et T[size-1-i]
		val = T[size-1-i];
		T[size-1-i] = T[i];
		T[i]=val;
	}
}


int main() {
	int tab[TAB_SIZE], tab2[TAB_SIZE];

	init(tab, TAB_SIZE);
	print(tab, TAB_SIZE);


	decal(tab, TAB_SIZE);
	print(tab, TAB_SIZE);

	decalg(tab, TAB_SIZE);
	print(tab, TAB_SIZE);

	invert(tab, TAB_SIZE);
	print(tab, TAB_SIZE);

	return 0;
}
