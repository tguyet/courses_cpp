
#include <stdio.h>

long fact(int d) {
	long l=1L;
	int i;
	for(i=1; i<=d; i++) {
		l*=i;
	}
	return l;
};


double e(int p)
{
	double e=0.0;
	int i=1;
	while(i<p) {
		e+=(double)i/(double)(fact(i));
		i++;
	}
	return e;
}

void main() {
	printf("%lf\n", e(10));
}
