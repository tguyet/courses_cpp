#include <stdio.h>

void mf(int *v, int *u) {
	*v=*v+*u;
}

void main() {
	int a[]={0,1,2,3};
	
	int *p[4];
	p[0]=a;
	p[1]=a+1;
	p[2]=a+2;
	p[3]=a+3;
	
	int **pp=p;
	
	printf("init :\n");
	
	printf("a=%d *a=%d, a[1]=%d\n", a, *a, a[1]);
	printf("p=%d *p=%d, **p=%d\n", p, *p, **p);
	printf("pp=%d *pp=%d, **pp=%d\n", pp, *pp, **pp);
	
	printf("Appel fonction :\n");
	
	mf(p[1], p[3]);
	printf("a=%d *a=%d, a[1]=%d\n", a, *a, a[1]);
	printf("p=%d *p=%d, **p=%d\n", p, *p, **p);
	
	printf("fin :\n");
	pp++;
	printf("pp-p=%d *pp-a=%d, **pp=%d\n", pp-p, *pp-a, **pp);
}
