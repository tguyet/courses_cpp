#include <stdio.h>

typedef struct {
	double r;
	double i;
} complexe;


void print(complexe c) {
	printf("%.1lf+%.1lfi",c.r, c.i);
};


complexe conj(complexe c1) {
	complexe c;
	c.r=c1.r;
	c.i=-c1.i;
	return c;
};

complexe add(complexe c1, complexe c2) {
	complexe c;
	c.r=c1.r+c2.r;
	c.i=c1.i+c2.i;
	return c;
};


complexe mult(complexe c1, complexe c2) {
	complexe c;
	c.r = c1.r*c2.r - c1.i*c2.i;
	c.i = c1.r*c2.i + c1.i*c2.r;
	return c;
};


int main() {
	complexe a ={3,4};
	complexe b ={2,0};
	complexe c = add(a, conj(b));
	complexe r = mult(c, c);
	print(r);
	printf("\n");
}
