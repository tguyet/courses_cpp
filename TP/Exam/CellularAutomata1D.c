#include <stdio.h>
#include <stdlib.h>

#define SIZE 8

typedef enum {ZERO, UN} etat;
typedef etat raw[SIZE];

void init(raw r)
{
	srand( time(NULL) );
	int i=0;
	for(;i<SIZE;i++) {
		r[i]=rand()%2;
	}
}

void print(raw r)
{
	int i=0;
	printf("|");
	for(;i<SIZE;i++) {
		if( r[i]==ZERO ) {
			printf(" ");
		} else {
			printf("*");
		}
	}
	printf("|\n");
}

void transform(raw r)
{
	int i, sum;
	raw s;
	for(i=1;i<SIZE-1;i++) {
		sum = r[i]+r[i-1]+r[i+1];
		if( sum>= 2 ) {
			s[i]=0;
		} else if( sum<= 1 ) {
			s[i]=1;
		} else {
			s[i]=r[i];
		}
	}
	sum=r[SIZE-1]+r[0]+r[SIZE-2];
	if( sum>= 2 ) {
		s[0]=0;
	} else if( sum<= 1 ) {
		s[0]=1;
	} else {
		s[0]=r[0];
	}
	sum=r[SIZE-1]+r[0]+r[SIZE-2];
	if( sum>=2 ) {
		s[0]=0;
	} else if( sum<= 1 ) {
		s[0]=1;
	} else {
		s[0]=r[0];
	}
	for(i=0;i<SIZE;i++) {
		r[i]=s[i];
	}
}

int main() {
	raw R;
	init(R);
	int j=0;
	while(j<3) {
		print(R);
		transform(R);
		j++;
	}
	return 1;
}


