
#include <math.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <stdlib.h>  

#define SIZE 250
//#define RACINE2 1.4142136
//#define FACTEUR 0.28867513 /*definition de sqrt(3)/6*/
#define FACTEUR 0.8660254 /*definition de sqrt(3)/2*/
#define RECLIM 10

//Variable globale (accessible par effet de bord) sur toute la récursion !
int **mat;
int rec_level;


/**
* Fonction récursive de dessin d'une courbe de Koch
*/
void dessine(double x1, double y1, double x2, double y2)
{
	if(rec_level>=RECLIM) {
		return;
	}
	rec_level++;
	
	printf("(%lf, %lf) (%lf %lf)\n", x1, y1, x2, y2);
	/*
	if( x1>x2 || y1>y2) {
		printf("Erreur (%lf, %lf) (%lf %lf)\n", x1, y1, x2, y2);
		return;
	}//*/
	
	//Calcul des arrondis
	int ix1=round(x1);
	int ix2=round(x2);
	int iy1=round(y1);
	int iy2=round(y2);
	
	if( ix1== ix2 && iy1== iy2) {
		if( ix1>=SIZE || ix1<0 || iy1>=SIZE || iy1<0 ) {
			//printf("Depassement d'image (%d, %d)\n", ix1, ix2);
		} else {
			mat[ix1][iy1]=0;
		}
		rec_level--;
		return; /*Condition d'arrêt*/
	}
	
	//Premier tier droit
	dessine(x1, y1, (2*x1+x2)/3, (2*y1+y2)/3);
	
	//tier du milieu en chapeau :
	//Calcul de la norme du vecteur A1A2:
	double N=sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
	
	//On multiplie le facteur d'angle
	
	double x= (x1+x2)/2 + FACTEUR*(y1-y2)/N;
	double y= (y1+y2)/2 + FACTEUR*(x2-x1)/N;
	
	dessine( (2*x1+x2)/3, (2*y1+y2)/3, x, y);
	dessine( x, y, (x1+2*x2)/3, (y1+2*y2)/3);
	
	//Dernier tier droit
	dessine((x1+2*x2)/3, (y1+2*y2)/3, x2, y2);
	
	rec_level--;
}


void saveppm(int **m, int l, int h) {
	std::ofstream sortie;
	sortie.open("sortie.ppm");
	if (sortie.bad()) {
		exit(1);
	}
	sortie << "P3" << std::endl;
	sortie << l << " " << h << std::endl << "255" << std::endl;
	int *pi=*m;
	for(int i=0; i<l*h;i++, pi++) {
		sortie << *pi <<" " << *pi <<" " << *pi <<" ";
	}
	sortie.close();
}

int main(int argc,char ** argv)
{
	rec_level=0;
	mat=(int **)malloc( SIZE*sizeof(int*) );
	//Initialisation de la matrice à 0
	for(int i=0; i<SIZE;i++) {
		mat[i]=(int *)malloc( SIZE*sizeof(int));
		for(int j=0;j<SIZE;j++) {
			mat[i][j]=255;
		}
	}
	
	/*
	double x1=3.66;
	double y1=1;
	double x2=5;
	double y2=1.28;
	
	double N=sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
	double x= (x1+x2)/2 + FACTEUR*(y1-y2)/N;
	double y= (y1+y2)/2 + FACTEUR*(x2-x1)/N;
	printf("%lf %lf\n", x,y);
	
	/*/
	//On lance le calcul récursif à partir de tout en haut:
	dessine(1,1, SIZE-1, 1);
	
	saveppm(mat, SIZE, SIZE);
	//*/
	
	//libération de la mémoire
	for(int i=0; i<SIZE;i++) {
		free(mat[i]);
	}
	free(mat);
}

