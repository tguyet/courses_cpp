#include <stdio.h>
#include <graphique.h>
#include <math.h>
#include <stdlib.h>

int main() {
  double cx, cy, x, y, xDiff, yDiff;
  double norme;
  int nbPoints, i;

  //srandom(time(NULL));
  printf("Indiquez la partie entiere et la partie imaginaire du point de depart\n");
  scanf("%lf%lf", &x, &y);
  printf("Indiquez la partie entiere et la partie imaginaire de c\n");
  scanf("%lf%lf", &cx, &cy);
  printf("Indiquez le nombre de points \n");
  scanf("%d", &nbPoints);
  Initialisation_Graphique(-2, -2, 2, 2);
  for (i = 0; i < nbPoints; i++) {
    xDiff = x - cx;
    yDiff = y - cy;
    norme = sqrt(xDiff * xDiff + yDiff * yDiff);
    x = sqrt((xDiff + norme) / 2.0);
    y = sqrt((-xDiff + norme) / 2.0);
    if (yDiff < 0.0) y = -y; /* x et y sont de signes contraires */
    if (rand() > RAND_MAX / 2 ) {
      x *= -1;
      y *= -1;
    }
    Point(x, y, 12);
  }
  fflush(stdin);
  getchar();
  Fin_Graphique();
  return 0;
}

