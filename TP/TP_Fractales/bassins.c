/**
* Calcul des bassins d'attraction des solutions numériques
* obtenues par la méthode de Newton à l'équation non-linéaire
* f(z) = z^3-1 (en notation complexe)
*
* Guyet T., AGROCAMPUS-OUEST
* Compilation : gcc -o bassins -Wall -lm -O2 bassins.c
*/

#include <stdio.h>
#include <math.h>


#define SIZE 1000 		//< taille de la matrice
#define H_PROX 0.001	//< seuil de proximité entre les solutions
#define SQRT3_2 0.866025404 //< valeur de sqrt(3)/2

// Déclaration des profils de fonctions utilent dans le main
int convergence(double x0, double y0);
void saveimage(int mat[SIZE][SIZE]);


int main(void)
{
	int i,j, k=0;
	int mat[SIZE][SIZE]; //< Matrice construite
	
	//Définition de variables pour positionner l'espace exploré
	double xdecay = 0.5; //< 0.5 pour centrer sur (0,0)
	double ydecay = 0.5;
	double scale = 2.0; //< Facteur d'échelle par rapport à -0.5:0.5
	
	//Construction de la matrice des bassins d'attraction
	for(i=0; i<SIZE; i++) {
		//printf("%d/%d\n",k,SIZE*SIZE);
		for(j=0; j<SIZE; j++) {
			mat[i][j]=convergence(scale*( (double)i/(double)SIZE-xdecay ), scale*( (double)j/(double)SIZE-ydecay ));
			k++;
		}
	}
	
	//Enregistrement des bassins d'attraction dans une image
	saveimage(mat);
	return 0;
}


/**
* Fonction qui détermine si un couple (x,y) est proche d'une solution de f(x)=0.
* La solution utilise les constantes SQRT3_2 et H_PROX.
* @param Position (x,y) à tester
* @return 0 si la position n'est pas proche d'une solution, et 1, 2 ou 3 si elle est
* proche d'une solution (le nombre dépend de la solution)
*/
int is_prox(double x, double y) 
{
	if( (x-1)*(x-1) + y*y < H_PROX ) return 1;
	double diffr = (x+0.5)*(x+0.5);
	if( diffr + (y-SQRT3_2)*(y-SQRT3_2) < H_PROX ) return 2;
	if( diffr + (y+SQRT3_2)*(y+SQRT3_2) < H_PROX ) return 3;
	return 0;
}

/**
* Fonction de calcul de la solution de l'equation non-lineaire f(z)=0 par la méthode
* de Newton
* Utilise la fonction is_prox comme critère d'arret.
* @param x, y : etat initiale dans la méthode de Newton
* @return 1, 2 ou 3 en fonction de la solution vers laquelle la méthode à converger
*/
int convergence(double x, double y)
{
	int ret=0;
	if(x==0 && y==0) {return 0;}
	//printf("\t%lf %lf =>",x,y);
	while( (ret=is_prox(x,y))==0 ) {
		//On fait quelques calculs intermédiaires pour rendre le programme plus efficace
		double x2=x*x;
		double y2=y*y;
		double x2y22=(x2+y2)*(x2+y2);
		//Calcul du pas suivant :
		double xnew= ( 2.0*x + (x2-y2)/x2y22 ) /3.0;
		double ynew=2.0* ( y - x*y/x2y22 ) /3.0;
		//On switch
		x=xnew;
		y=ynew;
	}
	//printf(" %lf %lf\n",x,y);
	return ret;
}

/**
* Fonction de sauvegarde de la matrice dans une image au format PPM
* ATTENTION: ce profil de fonction n'est pas propre (utiliser plutôt des pointeurs). 
* La fonction est limitée dans la taille de la matrice passée en paramètre !
* @param mat : matrice à deux dimensions 
*/
void saveimage(int mat[SIZE][SIZE])
{
	int i,j;
	FILE *output=fopen("bassins.ppm", "w");
	
	//On construit l'entête du fichier d'image PPM
	fprintf (output, "P3\n%d %d 255\n", SIZE, SIZE);
	
	//On enregistre les valeurs par triplets de valeurs
	for(i=0; i<SIZE; i++) {
		for(j=0; j<SIZE; j++) {
			int val=80*mat[i][j];
			fprintf(output,"%d %d %d ", val, val, val);
		}
	}
	fclose(output);
}

