/***************************************
* Mandel.cc
* Generateur de Mandelbrot et Julia en PPM
* 
*	G. Joly		B. Pesenti
*	R. Cheron	C. Jacoutot
*
*	Projet ESSI2	Janvier 99
****************************************/

#include <fstream>
#include <iostream>
#include <stdlib.h>  

using namespace std;

void error(char **av)
{
	cerr	<< "Usage: " << av[0] << " <fichier.ppm> [options]\n\n"
		<< "\t\tOptions:\t\tDefauts:\tValeurs:\n\n"
		<< "\t-l:\t<largeur>\t\t(250)\t\t{>0}\n"
		<< "\t-h:\t<haureur>\t\t(250)\t\t{>0}\n"
		<< "\t-q:\t<iterations>\t\t(35)\t\t{>=1}\n"
		<< "\t-d:\t<rouge> <vert> <bleu>\t0 0 0\t\t{0..255}\n"
		<< "\t-f:\t<rouge> <vert> <bleu>\t255 255 255\t{0..255}\n"
		<< "Julia:\t-j:\t<x> <y>\t\t\tMandelbrot\t{-2..2}\n"
		<< "Centre:\t-t:\t<x> <y>\t\t\t(0,0)\t\t{-2..2}\n"
		<< "\t-z:\t<zoom>\t\t\t(1)\t\t{>0}\n"
		<< "\t-m:\t<mode>\t\t\t(1)\t\t{1..4}\n"
		<< "\t-c:\tcurseur\n\n";
	exit(0);
}
int main(int argc,char ** argv)
{
	register double
		x,y,
		xc,yc,tx=0,ty=0,
		tmp,zo=1,m;

	register unsigned int
		mode=1,choix=0,c=0,k,q=35,
		l=250,h=250;
	register unsigned char
		r,g,b,
		rd=0,gd=0,bd=0,
		rf=255,gf=255,bf=255;
		
	cerr << "\nMANDEL (c)1999 R.Cheron, C.Jacoutot, G.Joly, B.Pesenti\n\n";	
		
	ofstream sortie;
		
	if(argc<2) error(argv);
	else if(!(argv[1][0]=='-' && argv[1][1]=='\0')) {
		sortie.close();
		sortie.open(argv[1]);
		if (sortie.bad()) error(argv);
	}
		
	for (int z=2; z<argc; z++)
	{
		if (argv[z][0]!='-') error(argv);
		switch (argv[z][1])
		{
			case 'j':	z++; xc=atof(argv[z]); z++; yc=atof(argv[z]); if (xc<-2 || xc>2 || yc<-2 || yc>2 || z>=argc) error(argv); choix=1; break;
			case 't':	z++; tx=atof(argv[z]); z++; ty=atof(argv[z]); if (tx<-2 || tx>2 || ty<-2 || ty>2 || z>=argc) error(argv); break;
			case 'z':	z++; zo=atof(argv[z]); if (zo<=0) error(argv); break;
			case 'l':	z++; l=atoi(argv[z]); if (l<=0) error(argv); break;
			case 'h':	z++; h=atoi(argv[z]); if (h<=0) error(argv); break;
			case 'c':	c=1; break;
			case 'q':	z++; q=atoi(argv[z]); if (q==0) error(argv);break;
			case 'm':	z++; mode=atoi(argv[z]); if (mode<1 || mode>4 ) error(argv); break;
			case 'd':	z++; rd=(char)atoi(argv[z]); z++; gd=(char)atoi(argv[z]); z++; bd=(char)atoi(argv[z]); break;
			case 'f':	z++; rf=(char)atoi(argv[z]); z++; gf=(char)atoi(argv[z]); z++; bf=(char)atoi(argv[z]); break;
			default:	error(argv);
		}
	}

	sortie 	<< "P6" << endl;
	sortie	<< l << " " << h << endl << "255" << endl;
	cerr << "calcul en cours...\n";
	for (double j=0; j<h; j++)
	{
		cerr << (int)((j+1)/h*100) << "%\15";
		for (double i=0; i<l; i++)
		{
			if (choix) { x=(2*i/l-1)/zo+tx;y=(2*j-h)/l/zo+ty; }
			else { x=0;y=0;xc=(2*i/l-1)/zo+tx;yc=(2*j-h)/l/zo+ty; }

			m=0;
			for (k=0;m<4 && k<q;k++)
			{
				tmp=x;
				x=x*x-y*y+xc;
				y=2*tmp*y+yc;
				m=x*x+y*y;
			}

			if (mode==1) { r=(float)k/q*(rf-rd)+rd; g=(float)k/q*(gf-gd)+gd; b=(float)k/q*(bf-bd)+bd; }
			if (mode==2) { r=(int)(i*rf/l+rd)*(m<4); g=(int)(j*gf/h+gd)*(m<4); b=(int)(j*bf/h+bd)*(m>=4); }
			if (mode==3) { if (x==0) x=0.01; r=(int)(y/x*rf+rd); g=(int)(y/x*gf+gd); b=(int)(y/x*bf+bd); }
			if (mode==4) { r=(int)(i*rf/l+rd)*(k%2); g=(int)(j*gf/h+gd)*(1-k%2); b=(int)(i*bf/l+bd)*(1-k%2); }

			if ((j==(int)(h/2) || i==(int)(l/2)) && c==1) {r=!r; g=!g; b=!b;}

			sortie << r << g << b;
		}
	}
	sortie.close();
	return 0;
}
