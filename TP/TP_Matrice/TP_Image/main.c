#include <stdio.h>
#include <stdlib.h>

//Importation des fonctions
#include "images.h"

int main()
{
    int h,w;
    int **image;
    image_load("Fraise.ppm", &image, &h, &w);
    image_save("Fraise_copy.ppm", image, h, w);
    return 0;
}
