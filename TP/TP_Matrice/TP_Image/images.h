
/**
* \brief Fonction de chargement d'une image à partir d'un fichier.
*
* La fonction lit le fichier file_name (au format d'image ppm) et
* créés la matrice correspondant à l'image. L'utilisateur de la
* fonction doit passer en paramètre un pointeur sur une matrice à
* remplir.
* La fonction alloue l'espace nécessaire à l'image. L'utilisateur
* doit passer un pointeur sur des variables qui recevront la
* taille de l'image (hauteur, largeur)
*  \param  file_name : nom du fichier à ouvrir
*  \param p_matrix : pointeur sur une matrice 2D construite par la fonction
*  \param p_length, p_width : pointeur remplit par la taille de l'image
*/
void image_load(char *file_name, int*** matrix, int *height, int *width);


/**
* \brief Fonction d'enregistrement d'une image dans un fichier
*  \param file_name : le nom du fichier dans lequel enregistrer l'image,
*  \param matrix : matrice de l'image,
*  \param height, width : la taille de l'image.
*/
void image_save(char* filename, int **matrix, int height, int width);


/**
* Fonction d'allocation d'une matrice 2D de taille vsize x hsize.
* Pour faire une image, il faut 3 matrices.
*/
int** new_matrix(int vsize,int hsize);
