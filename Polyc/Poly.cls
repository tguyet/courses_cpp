%% -------------------------------------------------------------
%%
%% This is the original source file `FormatBook.cls', 
%% No Copyright 2010
%%
%% author : Thomas Guyet, thomas.guyet@agrocampus-ouest.fr
%% --------------------------------------------------------------
%% 
%% It may be distributed and/or modified under the
%% any conditions.
%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{Poly}
\usepackage[utf8]{inputenc}
\RequirePackage[frenchb]{babel}
\RequirePackage{}
\RequirePackage{fancyhdr}
\RequirePackage{ae}
\RequirePackage{amsmath, amsfonts, amssymb, textcomp}
\RequirePackage{eso-pic,graphicx}
\RequirePackage{color}
\RequirePackage{verbatim}
\RequirePackage{ifthen}
\RequirePackage{lipsum,framed}
\RequirePackage{answers} % Package pour la gestion des exercices avec correction différée!

\RequirePackage[T1]{fontenc}
%\RequirePackage{arev}
%\RequirePackage{iwona}
%\RequirePackage{kurier}
\RequirePackage{fourier}

\RequirePackage{appendix}


%pour faire apparaître une image de fond
\newcommand{\bgimg}[1]{
\AddToShipoutPicture
   {
      \put(\LenToUnit{0 cm},\LenToUnit{0 cm})
      {
            \includegraphics[width=\paperwidth,height=\paperheight]{#1}
      }
   }
}


%Sur la base d'un book : on passe les options
%\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
%\ProcessOptions
\LoadClassWithOptions{book}


% -------- Commandes de Writer2Latex
%\newcommand\textsubscript[1]{\ensuremath{{}_{\text{#1}}}}


%-------Réglage de la taille de la page ------
\setlength{\textwidth}{16 cm} %Largeur du texte
\setlength{\textheight}{23 cm}%Hauteur de texte
\setlength{\topmargin}{-1 cm}
%\setlength{\oddsidemargin}{0 cm} %Marge a gauche <0 pour réduire
\setlength{\evensidemargin}{0 cm} %Marge a gauche <0 pour réduire


%% Redéfinition de la page de garde!
%%	Utilisation logo
%%	Bas de page avec date de la dernière version!

%Quelques variables globales supplémentaires
\global\let\@subtitle\@empty
\global\let\@annees\@empty
\global\let\@institution\@empty
\global\let\@pagewebcours\@empty
\global\let\@version\@empty
\newcommand{\subtitle}[1]{\def\@subtitle{#1}}
\newcommand{\annees}[1]{\def\@annees{#1}}
\newcommand{\institution}[1]{\def\@institution{#1}}
\newcommand{\pagewebcours}[1]{\def\@pagewebcours{\href{#1}{#1}}}
\newcommand{\version}[1]{\def\@version{#1}}

\version{1.0}


\renewcommand\maketitle{\begin{titlepage}%
	\newpage
	\thispagestyle{empty}
	\null
	\vskip -2cm
  \begin{figure}[h]
  \includegraphics{Figures/logo-agrocampusouest}
  \end{figure}
  
  \vskip 15em
  
  \begin{center}
  \let \footnote \thanks
    {\Huge \@title \par}
    {\LARGE \@subtitle \par}
    \vskip 6em
    {\large
      \lineskip .5em%
      \begin{tabular}[t]{c}
        \@author
      \end{tabular}\par}
    \vskip 1em
    {\large \@institution}
  \end{center}
  
  \vfill
  
  {\small
  Version \@version, Dernière modification : \@date\\
  \@pagewebcours
  }
  \end{titlepage}%
  \newpage
  \thispagestyle{empty}
  \pagestyle{fancy}
}
\renewcommand\contentsname{Table des matières}

%-------Définition de la page par défaut -----
\pagestyle{fancy}
%\fancyhead[LO]{\it \@title}
%\fancyhead[RO]{\it \title}
%\fancyhead[C]{}
%\fancyhead[RO]{\it \@title}
\fancyhead[LE]{\it \@title - \@author}
\fancyhead[RE]{}
%\fancyfoot[L]{}
\fancyfoot[CO]{}
\fancyfoot[CE]{}
\fancyfoot[LE]{\thepage}
\fancyfoot[RO]{\thepage}

%% Utilisation du package HyperRef pour faire de liens
\RequirePackage[pdftex]{hyperref}
% \makeatletter
% \AtBeginDocument{
%   \hypersetup{
%     pdftitle = {\@title},
%     pdfauthor = {\@author}
%   }
% }
% \makeatother

%% Utilisation de la couleuyr pour le package listing
\RequirePackage{color}
\definecolor{Brown}{cmyk}{0,0.81,1,0.60}
\definecolor{Green}{cmyk}{0.64,0,0.95,0.40}
\definecolor{Blue}{cmyk}{0.62,0.57,0.23,0}
\definecolor{grisclair}{gray}{0.90}
\definecolor{sable}{rgb}{0.9,0.9,0.7}
\definecolor{bleuclair}{rgb}{0.1,0.5,0.74}
\definecolor{orangeAO}{RGB}{241,90,34}
\definecolor{vertAO}{RGB}{102,204,102}
\definecolor{bleuAO}{RGB}{25,127,191}
\definecolor{orangeAO_l}{RGB}{255,130,74}
\definecolor{vertAO_l}{RGB}{152,254,152}
\definecolor{bleuAO_l}{RGB}{65,167,231}

%% Utilisation du package listing pour les bouts de code
\RequirePackage{listings}
% \lstset{language=C,
% columns=flexible,
% %,caption=\lstname,
% caption=,
% basicstyle=\small,
% tabsize=2,
% stringstyle=\ttfamily,
% showstringspaces=false,
% numbers=left,
% numberfirstline=true,
% numberstyle=\footnotesize,      % the size of the fonts that are used for the line-numbers
% stepnumber=1,                   % the step between two line-numbers. If it's 1 each line will be numbered
% breaklines=true,                % sets automatic line breaking
% keywordstyle=\ttfamily\color{Green},
% identifierstyle=\ttfamily\color{Blue}\bfseries,
% commentstyle=\color{Brown},
% }

\lstset{inputencoding=latin1}
\lstset{
%configuration de listings
  language=C,
  float=hbp,%
  %basicstyle=\sffamily\fontsize{8}{9}\selectfont\small, %
  basicstyle=\sffamily\footnotesize, %
  identifierstyle=\textbf, %
  keywordstyle=\color{bleuAO}\textbf, %
  stringstyle=\ttfamily, %
  showstringspaces=true,
  commentstyle=\textit, %
  emphstyle=\color{orangeAO}\textbf,
  columns=flexible, %
  tabsize=2, %
  frame=single, %
  frameround=tttt, %
  extendedchars=true, %
  %showspaces=false, %
  showstringspaces=false, %
  numbers=left, %
  numberstyle=\tiny, %
  breaklines=true, %
  breakautoindent=true, %
  captionpos=b,%
  %xrightmargin=0cm, %
  %xleftmargin=0cm
}

% Pour faire des insertions :
%\lstinputlisting[label=samplecode,caption=A sample]{sourceCode/HelloWorld.c}

%-------Commandes spéciales------------------

\newcommand{\hard}{$\star$}

\newcounter{NoEx}
\stepcounter{NoEx}
\newcounter{NoQ}
\stepcounter{NoQ}

\newcounter{NoExemple}
\stepcounter{NoExemple}
\newcounter{NoDef}
\stepcounter{NoDef}
\newcounter{NoRq}
\stepcounter{NoRq}

\newcommand{\NB}{{\underline{NB~:} }}
\newcounter{iscorrection}
\newcommand{\viewcorrection}{\setcounter{iscorrection}{1}}
\newcommand{\hidecorrection}{\setcounter{iscorrection}{0}}

\hidecorrection

\newcommand{\correction}[1]
{
  \ifthenelse{ \equal{\value{iscorrection}}{1} }
  {
    \vspace{5pt}
    \sffamily
    \setlength\leftmargin{5cm}
    \underline{\textbf{Correction :}}\vspace{0,1cm}\\ \null \hfill \parbox{.9\linewidth}{#1}\par
    \vspace{0,1cm}\rule{3cm}{0.5pt}
    \vspace{5pt}
  }
}

\newtheorem{Exc}{Exercice} %Utilisation d'un théorème pour le package ``answers''

\newenvironment{exercice}[1][]
{
  \lstset{xleftmargin=15pt}
  \def\FrameCommand{\vrule width 3pt \hspace{10pt} }%\fcolorbox{sable}{sable}
  \MakeFramed{\setlength{\hsize}{\textwidth} \advance\hsize-\width \FrameRestore}%
  %\vspace{-0,3cm}
  \noindent
  \begin{Exc}[#1]
  \setcounter{NoQ}{1}
}{
  \end{Exc}
  \endMakeFramed
}


\newcommand{\question}[1]
{
  \noindent
  \begin{center}\rule{\textwidth}{0pt}\end{center}\vspace{-0,7cm}
  \underline{Question \alph{NoQ})} \emph{#1} %\vspace{0,1cm}
  \stepcounter{NoQ}
}


\newenvironment{definition}[1][]
{
  \def\FrameCommand{\fcolorbox{grisclair}{grisclair}}%
  \MakeFramed{\setlength{\hsize}{0.9\textwidth} \advance\hsize-\width \FrameRestore}%
  \vspace{-0,3cm}
  \noindent
  \begin{center}\rule{\textwidth}{1pt}\end{center}\vspace{-0,3cm}
  {\bf Definition \arabic{NoDef} \ifthenelse{ \equal{#1}{} }{}{- #1}}\vspace{-0,5cm}
  \begin{center}\rule{\textwidth}{1pt}\end{center}
  \stepcounter{NoDef}
}
{
  \endMakeFramed
}
%\renewcommand{\definition}[1]{\begin{definition}#1\end{definition}}

\newenvironment{exemple}[1][]
{
  \lstset{xleftmargin=15pt}
  %\fboxsep=30pt
  \def\FrameCommand{\fcolorbox{sable}{sable}}%\fcolorbox{vertAO}{vertAO_l}}%
  \MakeFramed{\setlength{\hsize}{\textwidth} \advance\hsize-\width \FrameRestore}%
  \vspace{-0,3cm}
  \noindent
  \begin{center}\rule{\textwidth}{1pt}\end{center}\vspace{-0,3cm}
  {\bf Exemple \arabic{NoExemple} \ifthenelse{ \equal{#1}{} }{}{- #1} } \vspace{-0,5cm}
  \begin{center}\rule{\textwidth}{1pt}\end{center}
  %\vspace{-0,2cm}
  \stepcounter{NoExemple}
  \it
}
{
  \endMakeFramed
}


\newenvironment{remarque}[1][]
{
    \refstepcounter{NoRq}
    \noindent
    \begin{center}\rule{\textwidth}{1pt}\end{center}\vspace{-0,3cm}
    {\bf Remarque \arabic{NoRq} \ifthenelse{ \equal{#1}{} }{}{- #1}}\vspace{-0,5cm}
    \begin{center}\rule{\textwidth}{1pt}\end{center}
}{
\vspace{-0,5cm}\begin{center}\rule{\textwidth}{1pt}\end{center}
}

%\def\FrameCommand{\vrule width 3pt \hspace{10pt}}

\renewenvironment{warning}[1][]
{
  \lstset{xleftmargin=15pt}
  \def\FrameCommand{\fcolorbox{orangeAO}{orangeAO_l}}%
  \MakeFramed{\setlength{\hsize}{0.9\textwidth} \advance \hsize-\width \FrameRestore}%
  \vspace{-0,3cm}
  \noindent
  \begin{center}\rule{\textwidth}{1pt}\end{center}\vspace{-0,3cm}
  {\bf ! Attention ! \ifthenelse{ \equal{#1}{} }{}{- #1}}\vspace{-0,5cm}
  \begin{center}\rule{\textwidth}{1pt}\end{center}
}
{
  \endMakeFramed
}

\newcommand{\espace}{\vspace{5pt}}
\newcommand{\ie}{\emph{i.e.} }
\newcommand{\eg}{\emph{e.g.} }
\newcommand{\cf}{\emph{cf.} }

%%%%%  Configuration Package ANSWERS.STY %%%%%%
\Newassociation{Solution}{Soln}{solutions}

%\newcommand{\prehint}{~[Hint]}
%\newcommand{\presolution}{~[Solution]}
%\newcommand{\preSolution}{~[Homework]}
%\newcommand{\Opentesthook}[2]% {\Writetofile{#1}{\protect\section{#1: #2}}}
% introduction de la solution
\renewcommand{\Solnlabel}[1]{\textbf{\underline{Solution à l'exercice #1}}\setcounter{NoQ}{1}}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%             Modification des styles de chapitres          %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\RequirePackage{pstricks,psboxit}
%\RequirePackage{xcolor}
%\RequirePackage{pstricks}
%\RequirePackage{psboxit}

\makeatletter
\def\thickhrulefill{\leavevmode \leaders \hrule height 1ex \hfill \kern \z@}
\def\@makechapterhead#1{%
  \reset@font
  \parindent \z@
  \vspace*{10\p@}%
  \hbox{%
    \vbox{%
      \hsize=1.5cm%
      \begin{tabular}{c}
        \scshape \strut \@chapapp{} \\
        \colorbox{black}{\vbox{\hbox{\vbox to 1mm{}}\hbox{\color{white} \LARGE \bfseries \hspace{1mm}\thechapter\hspace{1mm}}\hbox{\vbox to 2cm{}}}}%
      \end{tabular}%
      }%
    \vbox{%
      \advance\hsize by -2cm
      \hrule height 0.4pt depth 0pt width \hsize
      \par
      \vskip 6pt%
      \hspace{20pt}%
      \parbox{300pt}{%
        \Huge \bfseries #1}%
      }%
    }%
  \vskip 20\p@
}
\def\@makeschapterhead#1{%
  \reset@font
  \parindent \z@
  \vspace*{10\p@}%
  \hbox{%
    \vbox{%
      \hsize=1.5cm%
      \begin{tabular}{c}
        \scshape \strut \phantom{\@chapapp{}} \\
        \colorbox{black}{\vbox{\hbox{\vbox to 1mm{}}\hbox{\color{white} \LARGE \bfseries \hspace{1mm}\phantom{\thechapter}\hspace{1mm}}\hbox{\vbox to 2cm{}}}}%
      \end{tabular}%
      }%
    \vbox{%
      \advance\hsize by -2cm
      \hrule height 0.4pt depth 0pt width \hsize
      \par
      \vskip 6pt%
      \hspace{20pt}%
      \parbox{260pt}{%
        \Huge \bfseries #1}%
      }%
    }%
  \vskip 100\p@
}

\def\chapter{%\cleardoublepage
\clearpage
 \thispagestyle{plain} \global\@topnum\z@
 \@afterindentfalse \secdef\@chapter\@schapter}

\def\section{\@ifstar\unnumberedsection\numberedsection}
\def\numberedsection{\@ifnextchar[%]
  \numberedsectionwithtwoarguments\numberedsectionwithoneargument}
\def\unnumberedsection{\@ifnextchar[%]
  \unnumberedsectionwithtwoarguments\unnumberedsectionwithoneargument}
\def\numberedsectionwithoneargument#1{\numberedsectionwithtwoarguments[#1]{#1}}
\def\unnumberedsectionwithoneargument#1{\unnumberedsectionwithtwoarguments[#1]{#1}}
\def\numberedsectionwithtwoarguments[#1]#2{%
  \ifhmode\par\fi
  \removelastskip
  \vskip 2ex\goodbreak
  \refstepcounter{section}%
  \begingroup
  \noindent
  \leavevmode\Large\bfseries\raggedright
  \thesection\ #2\par\nobreak
  \endgroup
  \noindent\hrulefill\nobreak
  \vskip 1pt%0.1ex
  \nobreak
  \addcontentsline{toc}{section}{%
    \protect\numberline{\thesection}%
    #1}%
  }
\def\unnumberedsectionwithtwoarguments[#1]#2{%
  \ifhmode\par\fi
  \removelastskip
  \vskip 2ex\goodbreak
%  \refstepcounter{section}%
  \begingroup
  \noindent
  \leavevmode\Large\bfseries\raggedright
%  \thesection\ 
  #2\par\nobreak
  \endgroup
  \noindent\hrulefill\nobreak
  \vskip 1pt
  \nobreak
  \addcontentsline{toc}{section}{%
%    \protect\numberline{\thesection}%
    #1}%
  }

\makeatother

