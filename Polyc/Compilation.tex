\chapter{Complément de compilation en C} \label{chp:Compilation}
Dans cette partie, on apporte quelques précisions sur les étapes de compilation spécifiques au langage C. La Figure \ref{fig:compilationC_full} illustre le schéma général de la compilation d'un programme C utilisant plusieurs fichiers \texttt{.c} et \texttt{.h}.

On présente également l'utilisation approfondie du compilateur \texttt{gcc} en ligne de commande ainsi que l'usage des \texttt{Makefile}.

\begin{figure}[ht]
\centering
\includegraphics[width=\defaultfigurewidth]{\Figures/CompilationC_full}
\caption{Schéma général de la compilation d'un programme C}
\label{fig:compilationC_full}
\end{figure}

Un programme en C est un ensemble de fichiers texte contenant les programmes. On distinguera par la suite deux types de fichiers :
\begin{itemize}
\item les fichiers \texttt{.c} : ils contiennent les implémentations des fonctions,
\item les fichiers \texttt{.h} : ils contiennent uniquement les déclarations de fonctions et de variables qui peuvent être utilisé par plusieurs fichiers \texttt{.c}. \textbf{Ces fichiers ne doivent pas être compilés}.
\end{itemize}


\section{Introduction à la compilation}
Lors de la compilation, il y a quatre tâches distinctes :

\begin{itemize}
\item La \textbf{vérification syntaxique} regarde si le programme est syntaxiquement correct,
\item La \textbf{compilation} construit des bouts de code assembleurs à partir de vos programmes,
\item L'\textbf{assemblage} construit du code machine à partir du code assembleur (je néglige cette étape par la suite),
\item L'\textbf{édition de liens} lie vos bouts de code ensemble, avec éventuellement d'autres bouts de codes extérieurs que vous auriez importé (sous la forme de librairies).
\end{itemize}

Des erreurs spécifiques peuvent apparaître à chacune de ces étapes.

\subsection{Notions d'analyse syntaxique}
Lorsque vous écrivez un programme, il y a tout un tas de \textit{règles syntaxiques} auxquelles le programmeur doit se contraindre. Ces règles permettent aux compilateurs de comprendre sans ambigu\"ité ce que le programmeur souhaitait que le programme fasse. Ces conventions sont donc indispensables à l'inter-compréhension de la machine et du programmeur. Avant de pouvoir passer à l'étape de compilation proprement dite, le compilateur vérifie donc que la syntaxe élémentaire est correcte.

En langage C, il vérifie, par exemple :
\begin{itemize}
\item que toutes les instructions finissent par un point virgule,
\item que toutes les fonctions auxquels il est fait référence ont été déclarées (existent),
\item que les accolades, les parenthèses, les guillemets ouverts sont bien fermées,
\item que les structures de contrôle sont bien utilisées,
\item ...
\end{itemize}

Pour comprendre le programme, le compilateur découpe votre fichier en lexème, c'est-à-dire en unité syntaxique et représente votre programme sous la forme d'un arbre (\cf Figure \ref{seq:refFigure2}). Pour cela, il s'appuie sur les séparateurs : l'espace, les parenthèses, les accolades, les guillemets, les virgules, les points virgules.

Ensuite, il vérifie que chacun des lexèmes correspond à un mot qu'il connaît soit parce que c'est un mot clé du langage C, soit parce que c'est un mot que vous avez défini (\textit{table des symboles}), soit parce que c'est un mot qui fait partie d'une librairie que vous avez explicitement importé (avec la directive de préprocessing \lstinline!include!, \cf section \ref{sec:preprocesseur}).

Plus finement, il vérifie que ces lexèmes vont bien ensemble (on parle un peu à tort d'\textit{analyse sémantique}, il s'agit plutôt d'une analyse grammaticale). Par exemple, il faut vérifier que après le mot clé \lstinline!if! il y a bien une parenthèse pour contenir le test.
%que si une fonction avait pour paramètre un nombre, la variable qui est utilisée est bien un nombre et pas une chaîne de caractères.

Finalement, les dernières vérification, mais pas moins les moins importantes, sont les vérifications de type. Le langage C est un langage ``fortement typé'', c'est à dire que toutes les variables, toutes les fonctions, tous les paramètres ont un type et que le compilateur fait la vérification de la bonne correspondance des types. Par exemple, une fois qu'on sait qu'il y a un élément entre les parenthèse du \lstinline!if!, il faut faire la vérification qu'il s'agit bien d'un résultat booléen et pas du texte ou autre chose.

\begin{figure}[ht]
 \centering
 \includegraphics{\Figures/Compilation_ArbreLexeme}
\begin{lstlisting}
if( nom != NULL ) {
  // Commentaire
  printf("Coucou %s\n", nom);
} else {
  printf("Bye bye\n");
}
\end{lstlisting}
\caption{Exemple d'analyse syntaxique. En bas : le programme C, en haut : représentation du programme sous la forme d'un arbre par analyse syntaxique.}
\label{seq:refFigure2}
\end{figure}


Si une erreur de syntaxe est trouvée, alors la compilation est arrêtée et des messages d'erreur vous invite de manière assez \texttt{explicite} à corriger celles-ci. Le compilateur peut même vous suggérer des modifications.

\begin{remarque}[Lisez les messages d'erreur]
 Les messages d'erreurs sont explicites !! Au début, il peut être difficile de les lire, mais à force de les voir vous devriez apprendre à les reconnaître et à corriger vous même vos erreurs !
\end{remarque}

\begin{remarque}[Utiliser le compilateur régulièrement pour la syntaxe !]
 Le compilateur peut également vous aider à écrire votre programme au fur et à mesure. Dans la mesure où il vous informe de la correction de votre syntaxe, n'hésitez pas à faire appel à lui (plutôt qu'à l'enseignant) pour vous donner un coup de main ...
\end{remarque}


Même en cas d'erreur, l'analyse syntaxique peut continuer afin d'identifier toutes les erreurs syntaxiques en une seule lecture du fichier. Vous pouvez ainsi vous retrouver avec des centaines d'erreur assez rapidement ! Il ne faut pas paniquer ... En règle général, il vaut mieux commencer par traiter les premières erreurs qui ont été identifiées, il y a des chances que si le début a été mal lu, le compilateur n'a plus rien compris à la suite et qu'il a trouvé plein de fautes ``imaginaires''.

D'autres messages d'information peuvent indiquer des ``\textit{Warning}''. Ceci ne bloque pas la compilation, mais il est conseillé de les corriger également. Même si ils ne gênent pas la compilation, les \textit{warnings} peuvent avoir des conséquences sur le bon déroulement de l'exécution du programme. Rien ne vaut un programme nickel d'un point de vue du
compilateur.

\begin{warning}
Ce n'est pas parce qu'un programme est syntaxiquement correct qu'il est ``bon'' ! En effet, généralement, un programme doit faire quelque chose, une syntaxe correcte ne garantis en rien que ce que fait le programme est conforme aux spécifications.
\end{warning}

Je ne préfère pas aborder ici les autres types d'erreur qui peuvent être rencontrées et qui sont plus complexes à généraliser :

\begin{itemize}
\item erreur d'édition de liens ;
\item erreurs d'exécution.
\end{itemize}

\subsection{Notion d'optimisation du code}
Un compilateur est un programme très complexe qui est capable de ``comprendre'' votre code et parfois de le remplacer en utilisant des astuces qui exploitent les spécificités de votre architecture matérielle. On parle alors d'optimisation.

Exemple d'optimisation qui pourra être faite de manière transparente pour le programmeur :

Voici une version non-optimisée qui nécessite de faire autant d'appel à la fonction \lstinline!getLimite()! que de tour de boucle :

\begin{lstlisting}
for(int i=0; i<getLimite(); i++) {
  //...
}
\end{lstlisting}
Et voici, une version optimisée, avec un seul appel à la fonction \lstinline!getLimite()! :

\begin{lstlisting}
int l=getLimite();
for(int i=0; i<l; i++) {
  //...
}
\end{lstlisting}

Il ne s'agit là que d'un exemple simple, mais c'est avec de petites modifications que le compilateurs est capable d'obtenir des améliorations impressionnantes.

Très souvent, il est même recommandé de laisser le compilateur faire les optimisations plutôt que de les faire soit même. Le compilateur est généralement plus efficace que l'humain (programmeur moyen) pour trouver des optimisations, et les programmes restent ainsi lisibles, plus facilement compréhensibles.

\section{Le pré-processeur} \label{sec:preprocesseur}
La compilation d'un fichier \texttt{.c} se fait en deux étapes (\cf Figure \ref{seq:refFigure4}) :
\begin{itemize}
\item le pré-processeur apporte quelques modifications à votre fichier pour le préparer à la compilation,
\item la compilation construit un fichier binaire (objet ou programme).
\end{itemize}

\begin{figure}[ht]
\centering
\includegraphics{\Figures/CompilationC_fichier}
\caption{Compilation d'un fichier C}
\label{seq:refFigure4}
\end{figure}


Le pré-processeur traite toutes instructions qui commencent par le caractère \texttt{\#} dans un programme C. Les instructions qui peuvent être données au pré-processeur sont les suivantes :
\begin{itemize}
\item \lstinline!#include "fichier.h"!~: demande au préprocesseur de copier le contenu du fichier fichier.h à cet emplacement. Cette instruction au pré-processeur est très fréquemment utilisé pour importer des fonctions décrites dans un autre fichier.
\item \lstinline!#define TOTO!~: déclare la variable \texttt{TOTO} (les variables du préprocesseur sont très souvent en majuscule)
\item \lstinline!#define FNCT(x) ...!~: déclare une macro \texttt{FNCT} avec une variable \texttt{x}
\item \lstinline!#ifndef TOTO ... #endif! : si la variable \texttt{TOTO} n'est pas définie, alors le code contenu jusqu'au \lstinline!#endif! sera compilé (sinon, il sera totalement ignoré)
\item \lstinline!#define TOTO 10! : déclare la variable \texttt{TOTO} avec la chaîne de caractères 10. à chaque fois que le préprocesseur rencontrera cette variable, il la remplacera par 10 \textit{avant la compilation} (il effectue simplement un remplacement du texte). Ceci est un moyen efficace de définir des constantes utiles dans un programme (par exemple la valeur de \texttt{Pi}).
\end{itemize}

\subsection{Exemple de l'effet du pré-processeur lors de la compilation}

Le petit programme \ref{code:compilationpp} utilise des commandes spécifiques au pré-processeur. En utilisant la commande de compilation suivante (avec l'option \texttt{-E}, on ne fait fonctionner que le pré-processeur~:
 \begin{center}
  \texttt{\$ gcc -E -o} \textit{exemple\_compilation\_pp.i} \textit{exemple\_compilation\_pp.c}
 \end{center}

Le résultat est de cette commande est illustré par le programme \ref{code:compilationpp_res}, pour lequel certains remplacements ont été réalisés. On constate, d'une part, que la variable de préprocesseur \lstinline!LIMITE! a été remplacée par sa valeur 10 dans le \lstinline!for! et, d'autre part, que la macro \lstinline!VALEUR_ABSOLUE! a été remplacée ``intelligemment'' (\ie toutes les valeurs de \lstinline!x! ont été remplacée par la valeur du paramètre effectif).

\lstinputlisting[label=code:compilationpp]{\Sources/exemple_compilation_pp.c}
\lstinputlisting[label=code:compilationpp_res]{\Sources/exemple_compilation_pp.i}

En pratique, le programme contient beaucoup de nouvelles lignes au début de fichier que je ne fais pas apparaître, vous pourrez menez l'expérience par vous même pour les voir, mais elles ont peu d'intérêt.

\subsection{Utilisation des \texttt{define} dans les \texttt{.h}}
Voici maintenant un exemple d'illustration du fonctionnement du préprocesseur qui est très utilisé dans l'écriture de fichiers \texttt{.h} propres. Je recommande donc de faire de même dans tout vos \texttt{.h}.

\begin{lstlisting}
/***
* Exemple de fichier .h
***/
#ifndef FICHIER_H
#define FICHIER_H
...
#endif
\end{lstlisting}


Dans cet exemple, on indique que si la variable \lstinline!FICHIER_H! n'est pas définit, on la définie et on ajoute (à la place des ...) les instructions en C spécifique à ce fichier. Cette façon de procéder permet de s'assurer que le préprocesseur n'insérera qu'une seule fois le contenu de ce fichier \texttt{.h} avant la compilation d'un \texttt{.c}. Lorsque le nombre de fichiers croît, il est facile de se retrouver avec des dépendances cycliques entre les fichiers \texttt{.c} et \texttt{.h} de sorte que le  préprocesseur insère deux fois un même \texttt{.h}. Cela pose des problèmes de compilation par la suite. Ici, si le fichier est appelé une seconde fois, par le préprocesseur, le fait que la variable \lstinline!FICHIER_H! soit déjà définie va bloquer l'insertion du contenu du fichier.

Pour chaque fichier \texttt{.h}, il faut un nom de variable différent.

Le préprocesseur supprime également tous les commentaires (\lstinline!//! et \lstinline!/*~~~*/!) qui sont utiles au programmeur, mais inutiles pour l'ordinateur.


\section{Compilation séparée}

Jusque là, nous nous sommes principalement intéressé à des programmes ne contenant qu'un unique fichier. Pour de gros programmes, il n'est pas raisonnable d'imaginer que tout ce passe dans un seul fichier. Si les programmes sont réparties dans plusieurs fichiers, il faut avoir recours à la compilation séparée.
Le problème de la décomposition d'un programme en plusieurs fichiers sera abordé dans la partie sur la ``décomposition'' des programmes. Je présente ici le problème de la compilation séparée.

\begin{remarque}
 Je commence par faire remarquer que la gestion de la compilation avec plusieurs fichiers est généralement transparente avec l'utilisation d'un IDE comme \texttt{Code::Blocks}.
\end{remarque}

Commençons par donner les deux grandes étapes nécessaires à la compilation séparée (\cf Figures \ref{fig:compilationC_full})~:
\begin{enumerate}
 \item tous les fichiers \texttt{.c} sont \textbf{compilés} indépendamment sous la forme de fichiers objets (\texttt{.o}),
 \item tous les fichiers objets (\texttt{.o}) sont assemblés par une étape d'\textbf{édition de liens}\index{édition de liens}. Dans cette étape, les liens sont également effectué avec des librairies de fonctions extérieures à votre code mais que vous utilisez.
\end{enumerate}



\subsection{Compilation d'un fichier \texttt{.c} en fichier objet avec \texttt{gcc}}
Pour compiler un fichier \texttt{.c} contenant un programme écrit en langage C sous la forme d'un fichier objet, il faut utiliser l'option \texttt{-c} de \texttt{gcc}. \texttt{gcc} est un compilateur qui s'exécute en ligne de commande sous Linux. Consulter la partie du cours sur l'utilisation de l'environnement de programmation Linux pour bien comprendre les explications suivantes.

Un fichier objet, extension \texttt{.o}, est un bout de programme compilé. Il ne peut pas être directement exécuté par le processeur, mais contient la traduction des programmes dans le langage machine. L'édition de liens se chargera \textit{simplement} de mettre ensemble tous les fichiers \texttt{.o} et à indiquer par où il faut commencer l'exécution du programme.

Commande permettant de compiler un fichier C à la suivante :

\begin{verbatim}
$ gcc -c -o fichier.o fichier.c 
\end{verbatim}
ou
\begin{verbatim}
$ gcc -c fichier.c 
\end{verbatim}

La ligne de commande se comprend ainsi :
\begin{itemize}
\item \texttt{-c} : option qui indique de ne faire que compiler le programme (pas d'édition de lien)
\item \texttt{-o fichier.o} : option pour imposer le nom du fichier binaire qui est construit (par défaut, il prend le même nom que le fichier \texttt{.c} et remplace juste l'extension en \texttt{.o})
\item \texttt{fichier.c} : nom du fichier à compiler (toujours à la fin de la ligne de commande)
\end{itemize}

Vous noterez qu'il n'est fait référence à aucun fichier \texttt{.h}. En effet, le compilateur ira chercher ces fichiers si le préprocesseur en a trouvé. Mais où va-t-il chercher les fichiers \texttt{.h} dans l'arborescence de fichier ? Par défaut, le compilateur les cherche dans le répertoire courant, et dans un certain nombre de répertoires usuels (définie lors de la configuration du système), par exemple \texttt{/usr/include}. Si vous souhaitez ajouter des répertoires dans lesquels le compilateur aura besoin d'aller identifier des \texttt{.h}, alors, il est nécessaire de lui indiquer explicitement à l'aide de l'option \texttt{-I}.

Par exemple :
\begin{verbatim}
$ gcc -c -o fichier.o -I ./monrepertoire/ fichier.c 
\end{verbatim}

Le résultat de la compilation est un fichier binaire qui est la traduction dans le langage machine du programme écrit en C. Pour visualiser le résultat, vous pouvez essayer d'ouvrir le fichier sous un éditeur de texte ... comme c'est du binaire, ça ne marche pas, ou bien avec une visualisation du contenu binaire :

\begin{verbatim}
$ od -a fichier.o 
$ od -x fichier.o
\end{verbatim}


\subsection{Édition de liens entre plusieurs fichiers \texttt{.o}}\index{édition de liens}
Une fois que tous vos fichiers \texttt{.c} ont été compilés en fichier \texttt{.o}, vous pouvez passer à l'édition de liens. La commande pour faire l'édition de liens entre trois fichiers \texttt{.o} est la suivante :

\begin{verbatim}
$ gcc -o monprogramme fichier1.o fichier2.o fichier3.o
\end{verbatim}

La ligne de commande se comprend ainsi :

\begin{itemize}
\item \texttt{-c} : contrairement à la compilation, \textbf{il n'y a pas d'option -c ici.}
\item \texttt{-o monprogramme} : le fichier binaire généré s'appellera monprogramme, en l'absence de cette fonction, le fichier généré s'appellera \texttt{a.out}.
\item \texttt{fichier1.o ...}~: à la fin de la ligne de commande, il faut préciser tous les fichiers \texttt{.o} que vous souhaitez lier entre eux.
\end{itemize}

De même que pour l'importation de fichier \texttt{.h} lors de la compilation, vous pourriez avoir besoin de préciser explicitement l'utilisation de librairies et où trouver ces libraires. Il faut alors utiliser les options \texttt{-L} et \texttt{-l}. Je ne détaille pas ce point ici, vous pouvez consulter la documentation de gcc pour plus d'informations.

Exemple d'utilisation de la librairie de fonctionnalités mathématiques :
\begin{verbatim}
$ gcc -o monprogramme -lm fichier1.o fichier2.o fichier3.o 
\end{verbatim}

\section{Compilation rapide d'un fichier unique}
Dans le cadre de ce cours, il est possible que vos programmes tiennent dans un seul et même fichier. Il n'est alors pas nécessaire de faire deux commandes pour compiler le programme. La commande suivante réalise d'un coup les deux étapes (compilation + édition de liens) :
\begin{verbatim}
$ gcc -o monprogramme fichier.c 
\end{verbatim}

\section{Utilisation d'un Makefile}\label{sec:Makefile}
Un \texttt{Makefile} est un fichier, nommé \texttt{Makefile} (sans extension), qui décrit la proédure de compilation qui doit être automatisée. Ce fichier est utilisé par la commande \texttt{make}.

\subsection{Un exemple simple}

Prenons l'exemple de trois fichiers :
\begin{itemize}
 \item \texttt{fonctions.h} : un fichier contenant les descriptions de quelques fonctions utilent au programme,
 \item \texttt{fonctions.c} : il contient les implémentations des fonctions de \texttt{fonctions.h}. Ce fichier fait appel à \texttt{fonctions.h},
 \item \texttt{main.c} : contient la fonction \lstinline!main()!, c'est le programme principal. Nous supposons qu'il fait appel à \texttt{fonctions.h}.
\end{itemize}

La Figure \ref{fig:Makefile1} illustre les dépendances entre les fichiers et les étapes de compilation à effectuer. 

\begin{figure}[ht]
 \centering
 \includegraphics[height=4cm]{./Figures/CompilationC_Makefile1}
 % CompilationC_Makefile1.eps: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=0 0 196 226
 \caption{Ordre de compilation et dépendances}
 \label{fig:Makefile1}
\end{figure}

Supposons que nous fassions une modification du fichier \texttt{main.c}, alors, il suffit de compiler de nouveau le fichier \texttt{main.c} et de refaire l'édition en lien. En revanche, si vous modifier le fichier \texttt{fonctions.h}, alors tous les fichiers doivent être compilés. En utilisant les dates des fichiers, la commande \texttt{make} est capable de déterminer les bons fichiers à compiler à partir des dépendances entre fichiers décrites dans le \texttt{Makefile}.

Pour cet exemple, le \texttt{Makefile} (assez simple) aura l'allure suivante :

\lstinputlisting[language=make,keywordstyle=\color{red}]{\Sources/exMakefile}

Ce fichier s'interprète à partir de motifs de règles qui ont l'allure suivante :
\begin{verbatim}
cible: dependance1 dependance2 ...
      commande
      commande
\end{verbatim}

Dans cette règle, \texttt{cible} indique le nom fichier qui sera produit par l'exécution de règle. Sur la même ligne, et séparé de ``:'' avec la cible, on donne toutes les dépendances nécessaire à la production de la cible. Sur les lignes en-dessous, décalées par une tabulation\footnote{Attention, il faut impérativement utiliser une tabulation. Les espaces ne vont pas fonctionner.}, on donne la (ou les) commande qu'il faut exécuter (typiquement un gcc).

Dans le \texttt{Makefile} exemple, la première règle \texttt{all} désigne ce qui doit être réalisé par défaut. On retrouve ensuite la règle de \texttt{main} qui réalise l'édition de liens et les deux règles pour les compilations de \texttt{main.c} et \texttt{fonctions.c}. Finalement on donne également une règle nommée \texttt{clean} pour faciliter la suppression de tous les fichiers compilés.
Pour la règle de \texttt{fonctions.o}, on a comme dépendance le fichier \texttt{fonctions.c} et également \texttt{fonctions.h} qui permettra de savoir qu'il faut réappliquer cette règle si l'un de ces fichiers a été modifié. La commande réalisé lors du déclenchement de cette règle est simplement un appel à \texttt{gcc}.
Pour la règle \texttt{clean}, il n'y a pas de dépendance et la commande exécuté sera la commande système \texttt{rm} (suppression de fichier).

\espace

Une fois ce fichier enregistré (dans le même répertoire que les fichiers sources), il s'utilise ainsi :
\begin{itemize}
 \item pour compiler ou recompiler votre programme \texttt{main}, tapez dans la console :
\begin{verbatim}
 ...$ make
\end{verbatim}
 \item pour déclencher la règle \texttt{clean}, faire :
\begin{verbatim}
 ...$ make clean
\end{verbatim}
\end{itemize}

Une fois le \texttt{Makefile} en place, on ne se pose plus de question sur la compilation !! C'est très pratique !

\subsection{Utilisation de variables dans le Makefile}
L'intérêt de \texttt{make} ne s'arrête pas là. Il est possible d'utiliser des variables dans le \texttt{Makefile} qui vont vous faciliter la vie. L'intérêt principal est de pouvoir facilement changer toutes les options de compilation sans avoir à modifier toutes les règles.

Le cas le plus usuel est le suivant. On retrouve exactement les mêmes règles que précédemment, mais, on a ajouté aux commandes de compilation une variable \texttt{\$(FLAGS)}. Cette variable sera remplacée par la valeur \texttt{-O2 -Wall}, définie en haut du fichier. On peut modifier facilement la valeur de cette variable (en \texttt{-g} par exemple). Dans ce cas, la modification s'appliquera aux deux commandes de compilation.

\lstinputlisting[language=make,keywordstyle=\color{red}]{\Sources/exMakefileVar}

Il restera alors à enregistrer le \texttt{Makefile}, puis à faire un \texttt{make clean} pour supprimer les anciennes versions compilées avec en \texttt{-O2} et à recompiler l'ensemble avec un \texttt{make}.
On peut ainsi facilement passer d'une compilation pour le débuggage (option \texttt{-g}) à une compilation finale (option \texttt{-O2}).

\espace

Beaucoup plus de fonctionnalité existent pour les \texttt{Makefile}, mais leur maîtrise n'est pas justifié dans le cadre de ce cours.

\section{Options avancées de compilation}
Lors de la compilation vous pouvez ajouter des options qui vont modifier le comportement du compilateur.

\texttt{-Wall} (\textit{Warning all}) : L'ajout de cette option indique au compilateur qu'on veut qu'il nous informe de tous les \textit{warnings} qu'il a pu trouver. Cette option assure que votre code est nickel pour la compilation.

Un autre grande classe d'option concerne le choix de l'optimisation à faire sur le code :

\begin{itemize}
\item options \texttt{-g}, \texttt{-g3}, \texttt{-pg} : correspond à la compilation en mode ``debug'' et de ``profiling''
\item option \texttt{-O1}, \texttt{-O2}, \texttt{-O3} : correspond au niveau global d'optimisation. Plus vous augmenterez le niveau d'optimisation, plus la compilation du code prendra du temps et utilisera de la mémoire. (l'option \texttt{-O3} utilise des fonctionnalités d'optimisation de code plus poussées, mais elles peuvent avec des conséquences importantes sur la traduction des programmes, et mieux vaut ne pas les utiliser sans en maîtriser les finesses.
\end{itemize}


%L'étape de débuggage consiste à trouver un problème (un bug) dans votre programme. Bien que celui-ci compile, il ne donne pas les résultats attendu, voire il plante totalement. Il peut être assez difficile de trouver l'origine du problème. Les outils de débuggage sont là pour vous aider (\texttt{gdb} et \texttt{valgrind}). L'option \texttt{-g} peut également être graduée entre 1 et 3 (par défaut \texttt{-g} signifie \texttt{-g2}).

Pour que les outils de débuggage (\texttt{gdb} et \texttt{valgrind}) fonctionnent, il faut impérativement que les programmes soient compilées avec l'option \texttt{-g}. En utilisant cette option, le compilateur va insérer tout un tas de code qui va permettre aux outils de bien comprendre ce qui se passe et ainsi vous aider à trouver les erreurs. Bien évidemment, toutes ces insertions nuisent aux performances de votre code, mais peu importe, vous l'utiliser dans une phase préliminaire.
L'option \texttt{-pg} permet d'ajouter de nouvelles informations dans le code qui vont permettre d'utiliser l'outil \texttt{gprof}.

Quand le code fonctionne bien, il est temps de compiler une vraie version (version ``Release''). Et même pourquoi pas une version optimisée en recompilant tout le code avec une option du type \texttt{-O}.% ou une option \texttt{-march}.
Le niveau \texttt{-O2} est le niveau généralement requis pour l'optimisation d'un code finalisé. 

%Option \texttt{-march} : cette option indique au compilateur quel code il doit produire pour l'architecture (ou arch) de votre processeur : elle indique qu'il doit fournir un code pour un certain type de processeur. Les différents processeurs ont des capacités différentes, supportent des jeux d'instructions différents et exécutent le code de façon différente. L'option \texttt{-march} va signaler au compilateur quel code créer spécifiquement pour votre processeur, avec toutes ses capacités.

% Quel type de processeur avez-vous ? Pour le savoir, tapez ceci : 
% 
% \begin{verbatim}
% $ cat /proc/cpuinfo
% \end{verbatim}
% 
% à présent, voyons l'option \texttt{-march} en action :
% \begin{itemize}
% \item Pour un vieux processeur Pentium III : \texttt{-march=pentium3}, 
% \item Pour un processeur AMD 64~bits : \texttt{-march=athlon64}
% \end{itemize}
% Si vous n'êtes pas s\^ur de votre type de processeur, utilisez juste \texttt{-march=native}. Quand cette option est utilisée, GCC détecte votre processeur et ajoute automatiquement les options appropriées.



% Il y a cinq paramètres \texttt{-O} : \texttt{-O0}, \texttt{-O1}, \texttt{-O2}, \texttt{-O3}, et \texttt{-Os}. Vous ne devrez utiliser qu'un seul d'entre eux. à l'exception de \texttt{-O0}, les paramètres \texttt{-O} activent chacun plusieurs options supplémentaires, donc assurez-vous de bien lire le chapitre sur les options d'optimisation\footnote{http://gcc.gnu.org/onlinedocs/gcc-4.3.3/gcc/Optimize-Options.html\#Optimize-Options}
% du manuel de \texttt{gcc} pour savoir quelles options sont activées à chaque niveau \texttt{-O}, ainsi que pour obtenir quelques explications sur ce qu'elles font. 
% 
% Examinons chaque niveau d'optimisation : 
% 
% \begin{itemize}
% \item \texttt{-O0}: Ce niveau (la lettre {\textquotedbl}O{\textquotedbl} suivie d'un zéro) désactive entièrement l'optimisation, c'est le niveau par défaut si aucun niveau -O n'est spécifié. Votre code ne sera pas optimisé : ce n'est généralement pas ce qui est voulu. 
% \item \texttt{-O1}: C'est le niveau le plus classique d'optimisation. Le compilateur va essayer de générer un code plus rapide et plus léger sans prendre plus de temps à compiler. C'est relativement classique, mais cela devrait fonctionner dans tous les cas.
% \item \texttt{-O2}: Un étape au-dessus du \texttt{-O1}. C'est le niveau recommandé d'optimisation sauf si vous avez des besoins spécifiques. Le niveau \texttt{-O2} va activer quelques options en plus de celles du \texttt{-O1}. Avec le
% niveau \texttt{-O2}, le compilateur va tenter d'augmenter les performances du code sans faire de compromis sur la taille et sans prendre trop de temps à compiler. 
% \item \texttt{-O3}: Il s'agit du plus haut niveau d'optimisation possible mais aussi du plus risqué. Le temps de compilation sera plus long avec cette option qui en fait ne devrait pas être utilisée de façon globale avec GCC 4.x. Le comportement de \texttt{gcc} a changé de façon significative depuis la version 3.x. Dans la version 3.x, \texttt{-O3} a
% montré que son utilisation conduisait à des temps d'exécution marginaux plus rapides qu'avec \texttt{-O2}, mais ce n'est plus le cas avec \texttt{gcc} 4.x. Compiler tous vos paquets avec \texttt{-O3} produira des plus gros binaires qui demanderont plus de mémoire et va augmenter de façon significative les étranges erreurs de compilation ou provoquer des
% comportements inattendus pour les programmes (y compris des erreurs). Les inconvénients sont plus nombreux que les avantages ;
% \textbf{L'utilisation du niveau \texttt{-O3} n'est pas recommandé pour GCC 4.x.} 
% \item \texttt{-Os}: Ce niveau va optimiser votre code en taille. Il active toutes les options du niveau \texttt{-O2} qui n'influent pas sur la taille du code généré. Il peut être utile pour les machines qui ont une taille très limitée d'espace libre sur le disque dur et/ou qui ont des processeurs avec une petite taille de cache. Toutefois, ce niveau
% peut tout à fait causer d'autres problèmes, c'est pourquoi l'utilisation de -Os n'est pas recommandé. 
% \end{itemize}




%Option \texttt{-pipe} : \texttt{-pipe} est une option assez répandue. Elle n'a, en fait, pas d'effet sur le code généré, mais va accélérer le processus de compilation. Elle dit au compilateur d'utiliser des tubes à la place des fichiers temporaires pendant les différentes étapes de la compilation. L'utilisation de tubes va utiliser plus de mémoire.
%Cette option est donc déconseillée sur les machines avec peu de mémoire, car \texttt{gcc} pourrait en manquer et se faire tuer par le système.