\chapter{Structures de données \label{chp:structuresdonnees}}
\Opensolutionfile{solutions}{}

Dans cette partie du cours, j'introduis la notion de structure de données (mots clés \lstinline!struct!, \lstinline!union!, \lstinline!enum! et \lstinline!typedef!) pour définir de nouveaux types à partir des types de base. 

Dans la vie réelle, les données qui sont traitées dans les programmes ne peuvent pas toujours se réduire à des nombres ou des caractères. Les données sont très souvent structurées et organisées.
Par exemple, si les numéros de facture d'une entreprise sont de la forme 021100576 avec comme les deux premiers chiffres qui indiquent le mois de l'années de la facture puis deux chiffres pour l'année, ensuite trois chiffres pour le numéro de la facture dans le mois et enfin deux chiffres pour coder le nom de l'employé qui a édité la facture. On peut choisir de représenter ce numéro de facture sous la forme d'un nombre entier. Dans ce cas, à chaque fois que l'ordinateur voudra connaître le mois d'édition de cette facture, il devra réaliser des opérations arithmétiques. Il semble plus raisonnable de structurer ces informations dans un seul bloc et avec les données de mois, années, numéro et employé clairement séparées, facilement accessibles sans calcul complexe.

L'objectif des structures de données est de faciliter la vie du programmeur en lui offrant la possibilité de définir ses propres structures de données composites.

\section{Définir de nouveaux types avec \texttt{typedef}}
L'instruction \lstinline!typedef! permet de définir de nouveau type de données. Sa syntaxe est la suivante~:

{\centering\itshape
\lstinline!typedef! description\_du\_type nom\_du\_type;
\par}

Vous pouvez ainsi définir le type \lstinline!bool! à partir des entiers ainsi~:
\begin{lstlisting}
typedef int bool;
bool b;
\end{lstlisting}

%typedef uint unsigned int;

\section{Types énumérés}

\subsection{Définition d'un type énumeré}
Un type énuméré est constitué par une famille finie de nombres entiers, chacun associé à un identificateur qui en est le nom. Mis à part ce qui touche à la syntaxe de leur déclaration, il n'y a pas grand-chose à dire à leur sujet.

Les valeurs d'un type énuméré se comportent comme des constantes entières ; elles font donc double emploi avec celles qu'on définit à l'aide de \lstinline!#define!. Leur unique avantage réside dans le fait que certains compilateurs détectent parfois, mais ce n'est pas exigé par la norme, les mélanges entre objets de types énumérés
distincts (par exemple, ne pas mettre une valeur de type \lstinline!mois_annee! dans une variable de type \lstinline!jour_semaine!) ; l'utilisation de ces types permet alors d'augmenter la sécurité des programmes.

La syntaxe de la déclaration des énumérations est décrite dans les exemples suivants.

\begin{exemple}
La ligne ci-dessous introduit un type énuméré, appelé \lstinline!enum jour_semaine!, constitué par les constantes
\lstinline!lundi! valant 0, \lstinline!mardi! valant 1, \lstinline!mercredi! valant 2, etc.

\begin{lstlisting}[numbers=none]
enum jour_semaine {lundi, mardi, mercredi, jeudi, vendredi, samedi, dimanche}; 
\end{lstlisting}

Ainsi, les expressions \lstinline!mardi+2! et \lstinline!jeudi! représentent la même valeur. Ces valeurs peuvent être directement utilisées dans le code.

Pour définir une variable ayant ce type, il y a deux solutions~:

\begin{itemize}
\item sans \lstinline!typedef!
\begin{lstlisting}[numbers=none]
enum jour_semaine maVariableJour; 
\end{lstlisting}

\item avec typedef (on définit un nouveau type)
\begin{lstlisting}[numbers=none]
typedef enum jour_semaine {lundi, mardi, mercredi, jeudi, vendredi} type_jour_ouvre;
type_jour_ouvre maVariableJourOuvre;
\end{lstlisting}
\end{itemize}
\end{exemple}



\begin{exemple} [Type booléen]
\begin{lstlisting}[numbers=none]
typedef enum valeurs_booleenne {false, true} bool;
bool b=true;
\end{lstlisting}

Dans cet exemple, l'énumération permet de définir deux valeurs (conforme à l'usage usuel en C)~: \lstinline!false! (avec la valeur 0 ) et \lstinline!true! (avec la valeur 1), et de définir le type \lstinline!bool!.
\end{exemple}

\begin{remarque}
Lors de la déclaration des éléments énumérés on peut explicitement donner des valeurs entières à chacune des valeurs symboliques.

Par exemple~:
\begin{lstlisting}[numbers=none]
enum Booleen {Vrai =1, Faux = 0}; 
\end{lstlisting}

\end{remarque}

\subsection{Utilisation d'un type énumeré}

Les type énumérés peuvent servir avec des \lstinline!switch! à distinguer des comportements en fonction de la valeur d'une variable de ce type.

\begin{exemple}[Type énuméré et switch]
 
\begin{lstlisting}
typedef enum {MR, MME, MLLE, DR} civilite;

void main() {
  civilite c;

  //... initialisation de c

  
  printf("Bonjour ");
  
  //Suite de l'affichage en fonction de civilité :
  switch(c) {
    case MR:
      printf("Monsieur");
      break;
    case MLLE:
      printf("Madame");
      break;
    case MME:
      printf("Madame");
      break;
    case DR:
      printf("Docteur");
      break;
    default:
  }
  printf(",\n");
}
\end{lstlisting}
\end{exemple}


\section{Les structures de données}
\begin{definition}[Structures]
Les structures sont des variables \textbf{composées de champs} de types différents.
\end{definition}

\subsection{Définition d'une nouvelle structure}
Pour définir une structure, il faut utiliser le mot clé \lstinline!struct!. À la suite du mot-clé, on indique~:

\begin{itemize}
\item facultativement, le nom que l'on souhaite donner à la structure (\lstinline!Point3D! dans l'exemple). Ce nom de structure pourra être utilisé comme un nom de type.
\item entre accolades~: la liste des déclarations des champs de la structure séparés par des virgules. Chaque champs à un type et un nom (ça s'écrit comme la déclaration d'une variable). Chaque champ peut avoir un type quelconque (y compris un autre type structure, bien s\^ur).
\end{itemize}

La structure ci-dessous illustre un structure de point en 3 dimensions~:
\begin{lstlisting}
struct Point3D {
  double x,
  double y,
  double z
};
\end{lstlisting}

La définition d'une structure permet de définir de nouveau type en combinant le mot clé \lstinline!struct! avec le mot clé \lstinline!typedef!~:

\begin{lstlisting}
typedef struct {
  // declarations des champs
} nom_type;
\end{lstlisting}

\begin{warning}
Le nom du type définit par \textup{typedef} se situe à la fin des accolades.
\end{warning}

\begin{exemple} [Structure Point2D]
Nous allons définir d'un type de structure \lstinline!Point2D! et déclaration d'une variable \lstinline!p! de type \lstinline!Point2D!.
\begin{lstlisting}
typedef struct {
  double x;
  double y;
} Point2D ;

Point2D p;
\end{lstlisting}
\end{exemple}

\begin{remarque}[Alignement des données] Contrairement aux tableaux, l'organisation en mémoire peut être plus complexe que la simple contigu\"ité des champs. Pour des raisons d'efficacité, certains compilateurs ``décalent'' les emplacements mémoire pour les ``aligner'' sur des multiples de tailles élémentaires.
\end{remarque}

\subsection{Utilisation des variables-structure}
Pour cette partie, on définit le type de structure ci-dessous~:

\begin{lstlisting}
typedef struct {
  int numero,
  char nom[32],
  char prenom[32],
  double salaire
} Fiche;
\end{lstlisting}

\begin{itemize}
\item Accès aux champs d'une structure~: utilisation de la ``notation pointée''
\begin{lstlisting}
Fiche f1, f2;
//Mettre ici l'initialisation de f1 et f2
f1.numero = 1;
if( f1.salaire>f2.salaire ) {
  printf("%s gagne plus que %s", f1.nom, f2.nom);
} else {
  printf("%s gagne plus que %s", f2.nom, f1.nom);
}
\end{lstlisting}

\item Accès aux champs d'une structure définie par un pointeur\footnote{se référer au chapitre sur les pointeurs pour la compréhension de cette notation.}~: utilisation de la ``notation fléchée''
\begin{lstlisting}
Fiche *f1, *f2; // declaration de deux pointeurs sur des structures
//Mettre ici allocation des pointeurs f1 et f2 et initialisation du contenu des emplacements pointes
f1->numero = 1;
if( f1->salaire > f2->salaire ) {
  printf("%s gagne plus que %s",f1->nom, f2->nom);
} else {
  printf("%s gagne plus que %s", f2->nom, f1->nom);
}
\end{lstlisting}
\end{itemize}

Notez que pour l'allocation mémoire d'une structure, tout ce passe exactement pareil qu'avant. En particulier, la fonction \lstinline!sizeof! va se charger de calculer la place nécessaire pour une instance de votre structure. Ainsi,
dans ce cas, l'allocation ressemblerait à cela~:

\begin{lstlisting}
f1 = (Fiche *)malloc( sizeof(Fiche) ); 
\end{lstlisting}

\begin{itemize}
\item Initialisation d'une structure~: l'initialisation d'une structure peut se faire manuellement (champs par champs en utilisant la notation pointée ou fléchée), ou bien à la déclaration ainsi~:
\end{itemize}
\begin{lstlisting}
Fiche f1 ={1, "Guyet","Thomas", 3500.0};
\end{lstlisting}

\begin{exemple}[Exemple de structures complexes]

\begin{itemize}
\item une structure avec des champs diversifiés
\begin{lstlisting}
typedef struct {
  FILE *imagefile; // fichier image
  char *filename; // nom du fichier correspondant a l'image
  int height, width; // dimensions de l'image
  Point2D points[height*width]; // tableau de points de l'image (de taille < height*width)
} Image;
\end{lstlisting}

\item une structure d'un n{\oe}ud d'arbre binaire
\begin{lstlisting}
typedef struct snode {
  snode *fd; // noeud fils droite
  snode *fg; // noeud fils gauche
} node;
\end{lstlisting}

\item une structure d'un élément d'une liste doublement chaînée de \lstinline!double!
\begin{lstlisting}
typedef struct selem {
  selem *prec; // element precedent
  double value; // valeur contenu par l'element
  selem *succ; // element suivant
} elem;
\end{lstlisting}

\end{itemize}
\end{exemple}

\begin{exercice}
Nous voulons définir un type composé Joueur qui comprend 4 informations : le nom du joueur, le nombre de match joués, le nombre de match gagnés et le nombre de matchs restant à jouer.

\question{Définir la structure \lstinline!Joueur!}

\question{Écrire une fonction \lstinline!SaisieJoueur! pour saisir au clavier les informations d'un joueurs. Cette fonction retournera une variable de type \lstinline!Joueur!}

\question{Écrire une fonction \lstinline!Saisies! pour remplir un tableau de joueurs. Cette fonction prendra comme paramètre d'entrée \lstinline!TabJ! : un tableau de joueur. La fonction ne retournera pas de résultat. Le nombre de joueur sera contenu dans une variable de pré-processing \lstinline!NB_JOUEUR!}
 
\question{(\hard) Écrire \lstinline!Calcul! pour calculer le nombre total de match restant à jouer en fonction du nombre de match que chaque joueur doit jouer (paramètre de la fonction)}.

\begin{Solution}
\question{Structure}

\begin{lstlisting}
typedef struct {
    char nom[20];
    int nbmj;
    int nbmg;
    int nbmrj;
}  Joueur;
\end{lstlisting}

\question{SaisieJoueur}

\begin{lstlisting}
Joueur SaisieJoueur(){
    Joueur j;
    printf("Nom?\n");
    scanf("%s",&j.nom);
    printf("Nombre de matchs joués?\n");
    scanf("%d",&j.nbmj);
    printf("Nombre de matchs gagnés?\n");
    scanf("%d",&j.nbmg);
    printf("Nombre de matchs restants?\n");
    scanf("%d",&j.nbmrj);
}
\end{lstlisting}

\question{Saisies}

\begin{lstlisting}
#define NB_JOUEUR 3
void saisies(Joueur J[NB_JOUEUR]){
    int i;
    for(i=0; i<NB_JOUEUR; i++) {
      printf("JOUEUR %d\n", i+1);
      Joueur j = SaisieJoueur();
      J[i]=j;
    }
}
\end{lstlisting}

\question{Calcul}

\begin{lstlisting}
int Calcul(Joueur J[NB_JOUEUR], int nb) {
    int i, deuxtotaljoue=0;

    int nbmatchs = NB_JOUEUR * nb;

    for(i=0; i<NB_JOUEUR; i++) {
      deuxtotaljoue+=J[i].nbmrj;
    }

    return nbmatchs-deuxtotaljoue/2;
}
\end{lstlisting}

\end{Solution}
\end{exercice}

% \begin{exercice}(Allo !!! (\hard\hard))
%  À l'aide d'un tableau de personnes (nom, prénom, numéro dans la rue, rue, code postal, ville, numéro de téléphone), faire un programme de recherche automatique de toutes les informations sur les personnes répondant à une valeur d'une rubrique donnée (tous les PATRICK , tous ceux de Cesson, travaillant à AGROCAMPUS, etc...). On suppose que le tableau est déjà initialisé.
% 
% \begin{Solution}
% Voici le code complet de la solution à l'exercice :
% \lstinputlisting{\Sources/exo_Allo.c}
% \end{Solution}
%\end{exercice}




\section{Correction des exercices}
\Closesolutionfile{solutions}
\Readsolutionfile{solutions}
