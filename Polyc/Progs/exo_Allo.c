#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define DIM 100

enum champs {nom,prenom,num,rue,cp,ville,tel};
char *nomchamp[7]={"Nom", "Prénom", "Numéro", "Rue", "Code Postal", "Ville", "Tel"}; //Déclaration d'une variable statique

typedef struct 
 {
  char    nom[15];
  char    prenom[20];
  int     num;
  char    rue[60];
  long    codepostal;
  char    ville[20];
  char    tel[15];
 } fiche;

fiche *rech_nom(fiche *,char *);
fiche *rech_prenom(fiche *,char *);
fiche *rech_num(fiche *,char *);
fiche *rech_rue(fiche *,char *);
fiche *rech_cp(fiche *,char *);
fiche *rech_ville(fiche *,char *);
fiche *rech_tel(fiche *,char *);
fiche *rech_nom(fiche *,char *);

typedef fiche *ptrfiche;
typedef ptrfiche (*ptrfonction)(ptrfiche,char*);
ptrfonction tabfonction[7]={rech_nom, rech_prenom, rech_num, rech_rue, rech_cp, rech_ville, rech_tel};

void affiche(fiche *f)
{
    if(f->nom[0])
    printf("%s %s\n%d, %s\n%ld %s\nTel : %s\n", f->nom, f->prenom, 
           f->num, f->rue, f->codepostal, f->ville, f->tel);
  else
    printf("fiche inconnue\n");
}
int idem(char *s1,char *s2)
/* compare deux chaines, dit si elles sont égales (1), ou non (0). On 
   pourrait supposer égalité quand la chaine s2 est incluse dans s1 */
{
  return(strcmp(s1,s2)?0:1);
}

fiche *rech_nom(fiche *pf,char *n)
 {while ((pf->nom[0])&&(!idem(pf->nom,n)))pf++;
  return(pf);}
fiche *rech_prenom(fiche *pf,char *n)
 {while ((pf->nom[0])&&(!idem(pf->prenom,n)))pf++;
  return(pf);}
fiche *rech_num(fiche *pf,char *n)
 {while ((pf->nom[0])&&(pf->num!=atoi(n)))pf++;
  return(pf);}
fiche *rech_rue(fiche *pf,char *n)
 {while ((pf->nom[0])&&(!idem(pf->rue,n)))pf++;
  return(pf);}
fiche *rech_cp(fiche *pf,char *n)
 {while ((pf->nom[0])&&(pf->codepostal!=atoi(n)))pf++;
  return(pf);}
fiche *rech_ville(fiche *pf,char *n)
 {while ((pf->nom[0])&&(!idem(pf->ville,n)))pf++;
  return(pf);}
fiche *rech_tel(fiche *pf,char *n)
 {while ((pf->nom[0])&&(!idem(pf->tel,n)))pf++;
  return(pf);}

int choix(void)
 {
  char lig[40];
  enum champs i,rep;
  for (i=nom;i<=tel;i++) printf("%d:%s ",i,nomchamp[i]);
  printf("\nou -1 pour quitter. Type de recherche désirée ? ");
  gets(lig);
  sscanf(lig,"%d",&rep);
  return(rep);
 }

void lecture(fiche *tab)
 {
  char lig[40];
  do
   {
    printf("nom (rien pour finir) ?");
    gets(tab->nom);
    if(tab->nom[0])
     {
      printf("         prénom ? ");
      gets(tab->prenom);
      printf("             N° ? ");
      gets(lig);
      sscanf(lig,"%d",&(tab->num));
      printf("            rue ? ");
      gets(tab->rue);
      printf("    code postal ? ");
      gets(lig);
      sscanf(lig,"%ld",&(tab->codepostal));
      printf("          ville ? ");
      gets(tab->ville);
      printf("n° de téléphone ? ");
      gets(tab->tel);
     }
   }
  while ((tab++)->nom[0]);
 }

void main(void)
 {
  enum champs c;
  char clef[40];
  fiche tab[DIM];
  lecture(tab);
  do
   {
    if (((c=choix())<0)||(c>6)) break;
    printf("quel(le) %s recherche-t'on ? ",nomchamp[c]);
    gets(clef);
    affiche(tabfonction[c](tab,clef));
   }
  while (c>=0);
 }
