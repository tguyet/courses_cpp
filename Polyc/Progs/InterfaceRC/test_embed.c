#include <Rinternals.h> 
#include <Rembedded.h> 

SEXP hello() { 
  return mkString("Hello, world!\n"); 
} 

int main(int argc, char **argv) { 
  SEXP x;

  Rf_initEmbeddedR(argc, argv); 
  x = hello();
  Rprintf("%s\n",CHAR(STRING_ELT(x,0)));
  Rf_endEmbeddedR(0);

  return 0;
}
