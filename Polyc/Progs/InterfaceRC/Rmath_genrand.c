#include <stdio.h>
#include <time.h>
#define MATHLIB_STANDALONE
#include "Rmath.h"

void main(){
  int i,n;
  double sum;

  //Initialisation du generateur aleatoire
  set_seed(time(NULL),77911);  

  printf("Nombre de tirage ?");
  scanf("%d",&n);

  sum = 0.;
  for(i=0;i<n;i++){
    sum += rnorm(0,1);
  }

  printf("La moyenne est %lf\n",sum/(double)n);
}
