#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#define MAX_VAL 100

/**
* @param a tableau de donnees
* @param l position gauche du sous-tableau a trier
* @param r position droite du sous-tableau a trier
* @return la position du pivot qui separe les deux zone du sous-tableau  entre les valeurs inferieures au pivot et les valeurs superieures au pivot.
*
* La fonction range le tableau a (entre l et r) par rapport a la valeur a[l] : a gauche du tableau, toutes les valeurs inferieures ou egales a a[l], a droite toutes les valeurs superieures a a[l]. Au final, a[l] est place a la limite des deux zones, a la position retournee par la fonction.
*/
int partition( int *a, int l, int r) {
	int pivot, i, j, t;
	pivot = a[l];
	i = l;
	j = r+1;
	
	//Rangement du tableau
	while( 1)
	{
		do ++i; while( a[i] <= pivot && i <= r );
		//ICI, toutes les positions entre l et i sont inferieures ou egales au pivot (a[l])
		do --j; while( a[j] > pivot );
		//ICI, toutes les positions entre j et r sont superieures au pivot
		if( i >= j ) break;
		// ICI, il existe des valeurs de a qui ne sont pas bien classees par rapport au pivot en particulier a[i] > pivot et a[j] < pivot, alors que i<j

		//Donc, on inverse a[i] et a[j]
		t = a[i];
		a[i] = a[j];
		a[j] = t;
	}
	
	//On replace pivot au centre du tableau 
	t = a[l];
	a[l] = a[j];
	a[j] = t;
	return j;
}

/**
* Algorithme de tri rapide (recursif)
* @param a tableau de donnees global a trier
* @param l index de la limite gauche a trier
* @param r index de la limite droite a trier
*/
void quicksort( int *a, int l, int r)
{
	int j;
	if( l < r ) {
		//On separe le tableau en deux selon la valeur de a[l]
		j = partition( a, l, r);
		// a[l] est replace en j, et travaille maintenant sur les sous-tableaux :
		quicksort( a, l, j-1);
		quicksort( a, j+1, r);
	}
}

/**
* Programme de test de la fonction quicksort
*/
int main()
{
	int i,it;
	clock_t start, finish;
	int nIter=10, size=20000;

	srand(time(NULL));

	for(it=0; it<nIter;it++) {
		//Allocation memoire pour le tableau de taille dynamique
		int *a = (int *)malloc(sizeof(int)* size*(it+1));

		// Generation d'un tableau aleatoire
		for(i=0; i<size*(it+1); i++) {
			a[i]=rand()%MAX_VAL;
		}
		//Application de l'algorithme QuickSort pour la classification
		start=clock();
		quicksort( a, 0, size*(it+1)-1);
		finish = clock();
		double duration = (double)(finish - start) / CLOCKS_PER_SEC;
		printf( "%2.3fs\n", duration );
		free(a);
	}
	
	return 0;
}

