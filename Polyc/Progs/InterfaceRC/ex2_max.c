#include <R.h>

void max(double *d, int *n, double *output)
{
	if(!output) return; //Verification de la nullite de output
	*output=-1;
	if(!d || !n) return; //Verification de la nullite de d et n
	int i=0;
	*output=d[0];
	for(i=0; i<*n; i++) {
		if(*output< d[i])
			*output=d[i];
	}
}
