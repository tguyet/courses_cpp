#include <R.h>
#include <Rdefines.h>

SEXP maxmin(SEXP v)
{
	SEXP output;
	int i;
	double *p, *q;

	PROTECT( v=AS_NUMERIC(v) );

	int size = GET_LENGTH(v);
	p = NUMERIC_POINTER(v);

	PROTECT( output = allocVector(REALSXP, 2) );
	q = NUMERIC_POINTER(output);

	//Calcul du max
	q[0] = p[0];
	q[1] = p[0];
	for( i=0; i< size; i++) {
		REAL(output)[0] = (p[i]<q[0]?q[0]:p[i]);
		REAL(output)[1] = (p[i]>q[1]?q[1]:p[i]);
	}

	UNPROTECT(2); //liberation de output et v
	return output;
}
