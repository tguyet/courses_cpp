partition <- function( a, l, r) {
	pivot <- a[l]
	i <- l
	j <- r+1
	
	while( 1 )
	{
		i<-i+1
		while( a[i] <= pivot && i <= r ) {
			i<-i+1
		}
		j<-j-1
		while( a[j] > pivot ) {
			j<-j-1
		}
		if( i >= j ) break

		t <- a[i]
		a[i] <- a[j]
		a[j] <- t
	}
	t <- a[l]
	a[l] <- a[j]
	a[j] <- t
	ret <- list( "pivot" = j, "tableau" = a)
	return(ret)
}

quicksort <- function( a, l, r)
{
	if( l < r ) {
		ret = partition( a, l, r)
		a <- ret$tableau
		j <- ret$pivot
		a <- quicksort( a, l, j-1)
		a <- quicksort( a, j+1, r)
	}
	return(a)
}


nbtest <- 10
nbsize <- 20000
for( i in 1:nbtest) {
	mytime <- proc.time()
	vect <- rnorm( nbsize*i )
	vect <- quicksort(vect, 1, length(vect))
	print(proc.time() - mytime)
}



