#include <R.h>
#include <Rinternals.h>

SEXP max(SEXP d1, SEXP d2)
{
	SEXP output;
	double dd1 = REAL(d1)[0];
	double dd2 = REAL(d2)[0];
	PROTECT( output = allocVector(REALSXP, 1) );
	REAL(output)[0] = (dd1>dd2?dd1:dd2);
	UNPROTECT(1);
	return output;
}
