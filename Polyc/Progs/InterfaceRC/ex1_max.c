#include <R.h>

void max(double *d1, double *d2, double *output)
{
  *output = (*d1>*d2?*d1:*d2);
}
