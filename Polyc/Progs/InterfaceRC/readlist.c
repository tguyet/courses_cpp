#include <R.h>
#include <Rdefines.h>

static SEXP getListElement(SEXP list, const char *str)
{
    SEXP elmt = R_NilValue;
	SEXP names;
    const char *tempChar;
    int i;

	//PROTECT( names=getAttrib(list, R_NamesSymbol) );
	names=getAttrib(list, R_NamesSymbol);

    for (i = 0; i < length(list); i++) {
		tempChar = CHAR(STRING_ELT(names, i));
		if( strcmp(tempChar,str) == 0) {
			elmt = VECTOR_ELT(list, i);
			break;
		}
    }
	//UNPROTECT(1);
    return elmt;
}

SEXP viewValue(SEXP arglist)
{
  int val=0;
  SEXP sval;

  if( !IS_LIST(arglist) )
    error("la fonction attend une liste comme argument");

  sval = getListElement(arglist, "value");
  if( sval==R_NilValue )
    error("Impossible de trouver l'élément 'value' dans la liste");

  val=INTEGER(sval)[0];

  Rprintf("L'élément est %d\n", val);
  return R_NilValue;
}

