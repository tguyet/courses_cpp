#include <R.h>
#include <Rdefines.h>

SEXP getInt(SEXP myint) {
  int *Pmyint;   // Pmyint sera un pointeur sur notre vecteurs d'entiers

  PROTECT(myint = AS_INTEGER(myint)); //Recuperation du parametre comme un vecteur d'entiers !
  Pmyint = INTEGER_POINTER(myint);    //Recuperation de l'adresse memoire du tableau d'entiers correspondant

  int n = GET_LENGTH(myint); //Recuperation de la taille du tableau
  int i=0;
  for(;i<n; i++) printf("[%d] %d\n",i, Pmyint[i]);
  UNPROTECT(1);

  return(R_NilValue);
}
