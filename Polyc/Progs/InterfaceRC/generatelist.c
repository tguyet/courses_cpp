/**
* Illustration de l'utilisation des listes dans l'interfacage R/C
* Thomas Guyet, AGROCAMPUS-OUEST, 2010
*/

#include <R.h>
#include <Rinternals.h>
#include <Rmath.h>


SEXP getListElement(SEXP list, const char *str)
{
    SEXP elmt = R_NilValue;
    const char *tempChar;
    int i;

	SEXP names=getAttrib (list, R_NamesSymbol);

    for (i = 0; i < length(list); i++) {
		tempChar = CHAR(STRING_ELT(names, i));
		if( strcmp(tempChar,str) == 0) {
			elmt = VECTOR_ELT(list, i);
			break;
		}
    }
    return elmt;
}


SEXP generate(SEXP arglist)
{
	int i;
	int size=20;
	double mu=0, sigma=1.0;
	SEXP list, dim1, dim2, names;

	size = INTEGER(getListElement(arglist, "size"))[0];
	mu = REAL(getListElement(arglist, "mu"))[0];
	sigma = REAL(getListElement(arglist, "sigma"))[0];

	
	PROTECT(list = allocVector(VECSXP, 2) );
	PROTECT(names = allocVector(STRSXP, 2) );

	SET_STRING_ELT(names, 0, mkChar("data") );
	SET_STRING_ELT(names, 1, mkChar("size") );

	setAttrib(list, R_NamesSymbol, names);
	
	GetRNGstate();
	PROTECT( dim1 = allocVector(REALSXP, size) );
	for(i=0; i<size; i++) { 
		REAL(dim1)[i]=rnorm(mu,sigma);
	}
	PutRNGstate();

	PROTECT( dim2 = allocVector(INTSXP, 1) );
	INTEGER(dim2)[0]=size;

	SET_VECTOR_ELT(list, 0, dim1);
	SET_VECTOR_ELT(list, 1, dim2);

	UNPROTECT(4);
	return list;
}


