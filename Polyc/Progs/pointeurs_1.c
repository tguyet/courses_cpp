#include <stdio.h>

int i_global;
int c_global;

int main()
{
   int i;
   char c;
   
   i_global=270;
   c_global='C';
   i=270;
   c='C';
   
   printf("Variables globales\n\t");
   
   /*valeur et adresse associees a la variable i_global */
   printf("la valeur de i_global %d\n\t", i_global);
   printf("l'adresse de i_global %u (%x)\n\t", &i_global,&i_global);
   
   /*valeur et adresse associees a la variable c_global */
   printf("la valeur de c_global %d\n\t", c_global);
   printf("l'adresse de c_global %d (%x)\n", &c_global,&c_global);
   
   printf("Variables locales\n\t");
   
   /*valeur et adresse associees a la variable i */
   printf("la valeur de i %d\n\t", i);
   printf("l'adresse de i %d (%x)\n\t", &i,&i);
   
   /*valeur et adresse associees a la variable c */
   printf("la valeur de c %d\n\t", c);
   printf("l'adresse de c %d (%x)\n", &c,&c);
}
