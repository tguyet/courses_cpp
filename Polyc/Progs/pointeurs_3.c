#include <stdio.h>

main() {
   int a[3]={0,2,5} ;
   int dist ;
   int *ptr_int;
   
   ptr_int=&a[0];
   
   printf("valeur de a[0] = %d\n", a[0]) ;
   printf("adresse de a[0] = %u (%x)\n", &a[0],&a[0]) ;
   printf("valeur de ptr_int= %u (%x)\n", ptr_int, ptr_int) ;
   printf("valeur pointee par ptr_int = %d\n",*ptr_int);
   
   *ptr_int=*ptr_int+2; //incrementation du contenu pointe par ptr_int
   printf("\n apres incrementation de *ptr\n\n");
   printf("valeur de a[0] = %d\n", *ptr_int) ;
   
   ptr_int++; //incrementation de l'adresse memoire contenu dans ptr_int
   
   printf("adresse de a[1] = %u (%x) \n", &a[1],&a[1]) ;
   printf("valeur de ptr_int apres (ptr_int++) %u (%x)\n",ptr_int, ptr_int);
   
   dist=ptr_int-&a[0];
   
   printf("nombre d'entiers entre ptr_int (%x) et a[0] (%x) = %d\n", ptr_int,&a[0],dist);
   dist=(char*)ptr_int-(char*)&a[0];
   printf("nombre de caracteres entre ptr_int (%x) et a[0] (%x) = %d\n", ptr_int,&a[0],dist);
}
