#include <stdio.h>
#define LIMITE 10
#define VALEUR_ABSOLUE(x) ( ((x) < 0)?-(x):(x) )

int main() {
	int i=0;
	for(i=-LIMITE; i< LIMITE; i++) {
		int val = VALEUR_ABSOLUE(i*i*i);
		printf("%d\n", val);
	}
	return 0;
}

