#include <stdio.h>

int main()
{
   int i=3;
   int *ptr_int;
   
   ptr_int=&i;
   
   printf("la valeur de i %d\n", i);
   printf("l'adresse de i %u (%x)\n", &i, &i);
   printf("la valeur de ptr_int %u (%x)\n",ptr_int, ptr_int);
   printf("la valeur pointee par ptr_int %d\n",*ptr_int);
   
   /*incrementation du contenu de l'emplacement memoire pointe par ptr_int*/
   *ptr_int=*ptr_int+2;
   
   printf("\nApres incrementation du contenu *ptr_int\n");
   printf("la valeur de i %d\n", i);
   printf("l'adresse de i %u (%x)\n", &i, &i);
   printf("la valeur de ptr_int %u (%x) \n",ptr_int, ptr_int);
   printf("la valeur pointee par ptr_int %d\n",*ptr_int);
   
   return 0;
}
