void  main () {
   int  *ptr;
   int  a, b, c;
   int  tab[3 ];
   int  i;

   a = 1 ;
   b = 2 ;
   c = 3 ;
   tab[0 ] = 5 ;
   tab[1 ] = 8 ;
   tab[2 ] = 12 ;

   /* Faire en sorte que ptr reference une variable existante */
   ptr = &a;
   printf ("Valeur actuelle de la donnee referencee par ptr = %d\n" ,*ptr);

   ptr = &b;
   printf ("Valeur actuelle de la donnee referencee par ptr = %d\n" ,*ptr);

   ptr = &tab[1 ];
   printf ("Valeur actuelle de la donnee referencee par ptr = %d\n" ,*ptr);

   /* Modifier la valeur de ce qui est reference par ptr */
   ptr = &c;
   printf ("Valeur actuelle de la donnee referencee par ptr = %d\n" ,*ptr);

   c = 4 ;
   printf ("Valeur actuelle de la donnee referencee par ptr = %d\n" ,*ptr);

   /* Reserver une zone memoire referencee par ptr */
   ptr = (int*) malloc(sizeof(int));
   *ptr = 10 ;
   printf ("Valeur actuelle de la donnee referencee par ptr = %d\n" ,*ptr);
   free(ptr);

   /* Reserver une zone memoire reference par ptr II : tableau dynamique */
   ptr = (int*) malloc(5 *sizeof(int));
   *ptr = 1 ;
   *(ptr+1 ) = 5 ;
   *(ptr+2 ) = 10 ;
   *(ptr+3 ) = 15 ;
   *(ptr+4 ) = 20 ;
   for (i=0 ; i < 5 ; i++) {
      printf ("Valeur actuelle de l'element %d du tableau dynamique défini par ptr = %d\n" ,i, *(ptr+i));
   }
   free(ptr);
}
